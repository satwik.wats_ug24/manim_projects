from manim import *


class SineWave(Scene):
    """Animates a sine wave using FunctionGraph"""

    def sine_wave(self, xx, wavelength, frequency, time, phase, x_origin):
        return np.sin(
            2 * PI * ((xx - x_origin) / wavelength - frequency * time) + phase
        )

    def sine_wave_graph(
        self,
        wavelength,
        frequency,
        time,
        phase=0.0,
        x_origin=0.0,
        x_range=None,
        color=RED,
    ):
        def sine_wave_caller(xx):
            sine = self.sine_wave(xx, wavelength, frequency, time, phase)
            return sine

        func = sine_wave_caller
        return FunctionGraph(func, x_range=x_range, color=color)

    def construct(self):
        """Main scene constructor"""
        ### Parameters
        time = ValueTracker(0.0)
        wavelength = ValueTracker(1.0)  # m
        frequency = ValueTracker(0.5)  # Hz

        ### TeX
        time_tex = Tex(r"$t = $", color=WHITE)
        wavelength_tex = Tex(r"$\lambda = $", color=WHITE)
        frequency_tex = Tex(r"$\nu = $", color=WHITE)

        ### DecimalNumbers
        time_decimal = DecimalNumber(
            time.get_value(),
            num_decimal_places=1,
            include_sign=False,
        ).next_to(time_tex, RIGHT, buff=0.2)
        wavelength_decimal = DecimalNumber(
            wavelength.get_value(),
            num_decimal_places=1,
            include_sign=False,
        ).next_to(wavelength_tex, RIGHT, buff=0.2)
        frequency_decimal = DecimalNumber(
            frequency.get_value(),
            num_decimal_places=1,
            include_sign=False,
        ).next_to(frequency_tex, RIGHT, buff=0.2)

        ### Groups of Tex and DecimalNumbers
        time_group = VGroup(time_tex, time_decimal).move_to(3.5 * UP)
        wavelength_group = VGroup(wavelength_tex, wavelength_decimal).next_to(
            time_group, DOWN
        )
        frequency_group = VGroup(frequency_tex, frequency_decimal).next_to(
            wavelength_group, DOWN
        )

        ### Create the sine wave Mobject
        sine = self.sine_wave_graph(
            wavelength.get_value(), frequency.get_value(), time.get_value()
        )

        ### Updaters
        def sine_updater(mob):
            """Takes in sine wave graph mobject, returns an updated one"""
            temp_mob = self.sine_wave_graph(
                wavelength.get_value(), frequency.get_value(), time.get_value()
            )
            mob.become(temp_mob)
            return

        ### Additions
        # must add the time ValueTracker to the scene
        self.add(time)
        self.add(wavelength)
        self.add(frequency)

        # Add updaters to the mobjects they update
        sine.add_updater(sine_updater)

        time_decimal.add_updater(lambda mob: mob.set_value(time.get_value()))
        wavelength_decimal.add_updater(
            lambda mob: mob.set_value(wavelength.get_value())
        )
        frequency_decimal.add_updater(lambda mob: mob.set_value(frequency.get_value()))

        ### Scene play
        # add the sine wave
        self.play(Create(sine), run_time=3)
        self.wait(0.2)

        self.play(Create(time_group))
        self.play(Create(wavelength_group))
        self.play(Create(frequency_group))
        self.wait(0.2)

        self.play(
            time.animate.set_value(5), run_time=5, rate_func=rate_functions.linear
        )
        self.wait(0.2)

        self.play(
            wavelength.animate.set_value(3),
            run_time=5,
            rate_func=rate_functions.ease_in_out_sine,
        )
        self.wait(0.2)
