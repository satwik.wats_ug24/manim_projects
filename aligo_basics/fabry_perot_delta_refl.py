from manim import *

def get_cav_refl(phi, r1, r2):
    """Get the Fabry-Perot cavity amplitude reflection.
    phi is single-pass phase of the light.
    """
    return (-r1 + r2 * np.exp(2j * phi))/(1 - r1 * r2 * np.exp(2j * phi))

def get_cav_buildup_toward_end_mirror(phi, r1, r2):
    """Get the cavity buildup going toward the end mirror.
    phi is single-pass phase of the light.
    """
    t1 = np.sqrt(1 - r1**2)
    return t1 / (1 - r1 * r2 * np.exp(2j * phi))

def get_cav_buildup_toward_input_mirror(phi, r1, r2):
    """Get the cavity buildup going toward the end mirror.
    phi is single-pass phase of the light.
    """
    t1 = np.sqrt(1 - r1**2)
    return t1 * r2 * np.exp(1j * phi) / (1 - r1 * r2 * np.exp(2j * phi))

def get_cav_trans(phi, r1, r2):
    """Get the cavity buildup going toward the end mirror.
    phi is single-pass phase of the light.
    """
    t1 = np.sqrt(1 - r1**2)
    t2 = np.sqrt(1 - r2**2)
    return t1 * t2 * np.exp(1j * phi) / (1 - r1 * r2 * np.exp(2j * phi))

class FabryPerotDeltaRefl(Scene):
    """Make a two-mirror cavity, with five propogating E-fields
    1) Input beam
    2) Refl beam
    3) Cavity beam heading to end mirror
    4) Cavity beam heading back to input mirror
    5) Transmitted beam
    """
    def get_phase(self, k=1, x=0, freq=1, time=0, phase=0):
        return k * x - 2 * PI * freq * time + phase
    def get_sine_wave(self, amp=1, k=1, x=0, freq=1, time=0, phase=0):
        return  amp * np.sin(k * x - 2 * PI * freq * time + phase)

    def rotate(self, x1, y1, theta, origin=np.array([0,0,0])):
        """Return x2 and y2 being rotated about the origin by theta.
        """
        x0 = origin[0]
        y0 = origin[1]
        x2 = (x1 - x0) * np.cos(theta) - (y1 - y0) * np.sin(theta)
        y2 = (x1 - x0) * np.sin(theta) + (y1 - y0) * np.cos(theta)
        return x2, y2

    def show_axis(self):
        x_start = np.array([-5,0,0])
        x_end = np.array([5,0,0])

        y_start = np.array([0,-3,0])
        y_end = np.array([0,3,0])

        x_axis = Line(x_start, x_end)
        y_axis = Line(y_start, y_end)

        # self.add(x_axis, y_axis)

        self.origin_point = np.array([0,0,0])

        return

    def make_electric_field(
        self, 
        amp=ValueTracker(1), 
        k=ValueTracker(1), 
        freq=ValueTracker(1), 
        time=0, 
        phase=ValueTracker(0), 
        start=np.array([-5,0,0]), 
        end=np.array([5,0,0]), 
        number_of_lines=50, 
        color=RED,
        amp_scaler=ValueTracker(1),
        k_scaler=1,
        freq_scaler=1,
        phase_offset=ValueTracker(0.0),
        x_start=None,
        x_end=None,
    ):
        """Makes an electric field VGroup of Lines.
        Electric field is defined by amp, k, freq, time, and phase ValueTrackers.
        start and end are 3D vectors defining the beginning and end of the wave.
        number_of_lines defines how many Line segments the wave is broken up into.
        x_start and x_end should either be None if we don't want to move the sine wave
        starting points, or a ValueTracker if we do.
        Returns the VGroup.
        """
        # Define the VGroup of Lines which make up the E-field, and store some ValueTrackers
        beam = VGroup()
        beam.amp = amp
        beam.k = k
        beam.freq = freq
        beam.time = time
        beam.phase = phase

        beam.x_start = x_start
        beam.x_end = x_end

        beam.amp_scaler = amp_scaler
        beam.k_scaler = k_scaler
        beam.freq_scaler = freq_scaler
        beam.phase_offset = phase_offset

        amp_float = amp_scaler.get_value() * amp.get_value()
        k_float = k_scaler * k.get_value()
        freq_float = freq_scaler * freq.get_value()
        phase_float = phase_offset.get_value() + phase.get_value()

        # Check if ValueTrackers exist
        if x_start:
            start[0] = x_start.get_value()
        if x_end:
            end[0] = x_end.get_value()

        # Get the start and end vector info.
        # This function will first calculate the wave along the x axis,
        # then rotate it appropriately.
        span = end - start
        origin = (start + end) / 2
        total_length = np.sqrt(np.sum(span**2))
        norm = span / total_length
        theta = np.arctan2(norm[1], norm[0])

        x_diff = total_length / number_of_lines

        phase_output = self.get_phase(k=k_float, x=total_length, freq=freq_float, time=time, phase=0)

        # Store some additional info
        beam.phase_output = phase_output
        beam.start = start
        beam.end = end
        beam.origin = origin
        beam.theta = theta
        beam.number_of_lines = number_of_lines
        beam.color = color

        for ii in range(number_of_lines):
            # Calculate wave in 1D
            temp_x_start = ii * x_diff
            temp_x_end = (ii + 1) * x_diff

            temp_y_start = self.get_sine_wave(amp_float, k_float, temp_x_start, freq_float, time, phase_float)
            temp_y_end = self.get_sine_wave(amp_float, k_float, temp_x_end, freq_float, time, phase_float)

            # Translate to origin = (0, 0)
            temp_x_start -= total_length/2
            temp_x_end -= total_length/2
            # temp_y_start += origin[1]
            # temp_y_end += origin[1]

            # Rotate about origin
            temp_x_start, temp_y_start = self.rotate(temp_x_start, temp_y_start, theta)
            temp_x_end, temp_y_end = self.rotate(temp_x_end, temp_y_end, theta)

            # Translate to sine wave position beam.origin
            temp_x_start += origin[0]
            temp_x_end += origin[0]
            temp_y_start += origin[1]
            temp_y_end += origin[1]

            # Define the Line
            temp_start = np.array([temp_x_start, temp_y_start, 0])
            temp_end = np.array([temp_x_end, temp_y_end, 0])

            temp_line = Line(temp_start, temp_end, color=color)
            beam.add(temp_line)

        return beam

    def sum_electric_fields(self, field1, field2, color=YELLOW, reverse=True):
        """Add together two VGroups of electric fields
        """
        if field1.number_of_lines != field2.number_of_lines:
            print()
            print("Cannot sum two fields of different lengths")
            return
        
        beam = VGroup()
        beam.color = color
        for ii in range(field1.number_of_lines):
            if reverse:
                jj = (field1.number_of_lines - 1) - ii
            else:
                jj = ii
        
            line1 = field1[ii]
            line2 = field2[jj]

            if reverse:
                new_line_start = line1.start
                new_line_end = line1.end
                new_line_start[1] = line1.start[1] + line2.end[1]
                new_line_end[1] = line1.end[1] + line2.start[1]
            else:
                new_line_start = line1.start 
                new_line_end = line1.end 
                new_line_start[1] = line1.start[1] + line2.start[1]
                new_line_end[1] = line1.end[1] + line2.end[1]

            new_line = Line(new_line_start, new_line_end, color=color)
            beam.add(new_line)

        return beam

    def construct(self):
        # Set up a timer variable
        self.timer = 0.0
        self.rate = 0.25
        number_of_lines = 100

        x_start_input = ValueTracker(-7.0)
        x_input_mirror = ValueTracker(-3.0)
        x_end_mirror = ValueTracker(3.0)
        x_end_input = ValueTracker(7.0)

        # Mirror
        self.mirror_width = 0.5
        self.input_mirror_refl = ValueTracker(0.5)
        self.end_mirror_refl = ValueTracker(0.5)

        self.input_mirror = always_redraw(
            lambda: Rectangle(height=3.2, width=self.mirror_width, fill_color=BLUE, fill_opacity=self.input_mirror_refl.get_value(), color=BLUE)
            .shift((x_input_mirror.get_value() - self.mirror_width/2) * RIGHT)
        )

        self.end_mirror = always_redraw(
            lambda: Rectangle(height=3.2, width=self.mirror_width, fill_color=BLUE, fill_opacity=self.end_mirror_refl.get_value(), color=BLUE)
            .shift((x_end_mirror.get_value() + self.mirror_width/2) * RIGHT)
        )

        # Input Beam
        amp_input = ValueTracker(0.75)
        k_input = ValueTracker(PI / 2)
        freq_input = ValueTracker(1)
        time_input = 0.0
        phase_input = ValueTracker(0.0)
        
        start_input = np.array([-7.0, 0.0, 0.0])
        end_input = np.array([-3.0, 0.0, 0.0])

        # Calculate the cavity params
        length_input = x_end_mirror.get_value() - x_input_mirror.get_value()
        phi_input = k_input.get_value() * (end_input[0] - start_input[0])

        phi = k_input.get_value() * length_input
        r1 = self.input_mirror_refl.get_value()
        r2 = self.end_mirror_refl.get_value()

        phase_input.set_value( -1 * phi_input )

        self.input_beam = self.make_electric_field(
            amp_input, 
            k_input, 
            freq_input, 
            time_input, 
            phase_input, 
            start_input, 
            end_input, 
            number_of_lines,
            x_start=x_start_input,
            x_end=x_input_mirror,
        )

        # Refl Beam
        amp_refl = amp_input
        k_refl = k_input
        freq_refl = freq_input
        time_refl = 0.0
        phase_refl = phase_input
        x_start_refl = x_input_mirror
        x_end_refl = x_start_input

        start_refl = np.array([-3.0, 0.0, 0.0])
        end_refl = np.array([-7.0, 0.0, 0.0])

        cav_refl = get_cav_refl(phi, r1, r2)

        amp_refl_scaler = ValueTracker(np.abs(cav_refl))
        k_refl_scaler = 1
        freq_refl_scaler = 1
        phase_refl_offset = ValueTracker(np.angle(cav_refl))

        self.refl_beam = self.make_electric_field(
            amp_refl, 
            k_refl, 
            freq_refl, 
            time_refl, 
            phase_refl, 
            start_refl, 
            end_refl, 
            number_of_lines,
            color=ORANGE,
            amp_scaler=amp_refl_scaler,
            k_scaler=k_refl_scaler,
            freq_scaler=freq_refl_scaler,
            phase_offset=phase_refl_offset,
            x_start=x_start_refl,
            x_end=x_end_refl
        )

        # Cavity Toward End Mirror Beam
        amp_cav_right = amp_input
        k_cav_right = k_input
        freq_cav_right = freq_input
        time_cav_right = 0.0
        phase_cav_right = phase_input

        x_start_cav_right = x_input_mirror
        x_end_cav_right = x_end_mirror

        start_cav_right = np.array([-3.0, 0.0, 0.0])
        end_cav_right = np.array([3.0, 0.0, 0.0])

        cav_right = get_cav_buildup_toward_end_mirror(phi, r1, r2)

        amp_cav_right_scaler = ValueTracker(np.abs(cav_right))
        k_cav_right_scaler = 1
        freq_cav_right_scaler = 1
        phase_cav_right_offset = ValueTracker(np.angle(cav_right))

        self.cav_right_beam = self.make_electric_field(
            amp_cav_right, 
            k_cav_right, 
            freq_cav_right, 
            time_cav_right, 
            phase_cav_right, 
            start_cav_right, 
            end_cav_right, 
            number_of_lines,
            color=PURPLE,
            amp_scaler=amp_cav_right_scaler,
            k_scaler=k_cav_right_scaler,
            freq_scaler=freq_cav_right_scaler,
            phase_offset=phase_cav_right_offset,
            x_start=x_start_cav_right,
            x_end=x_end_cav_right
        )

        # Cavity Toward Input Mirror Beam
        amp_cav_left = amp_input
        k_cav_left = k_input
        freq_cav_left = freq_input
        time_cav_left = 0.0
        phase_cav_left = phase_input

        x_start_cav_left = x_end_mirror
        x_end_cav_left = x_input_mirror

        start_cav_left = np.array([3.0, 0.0, 0.0])
        end_cav_left = np.array([-3.0, 0.0, 0.0])

        cav_left = get_cav_buildup_toward_input_mirror(phi, r1, r2)

        amp_cav_left_scaler = ValueTracker(np.abs(cav_left))
        k_cav_left_scaler = 1
        freq_cav_left_scaler = 1
        phase_cav_left_offset = ValueTracker(np.angle(cav_left))

        self.cav_left_beam = self.make_electric_field(
            amp_cav_left, 
            k_cav_left, 
            freq_cav_left, 
            time_cav_left, 
            phase_cav_left, 
            start_cav_left, 
            end_cav_left, 
            number_of_lines,
            color=PINK,
            amp_scaler=amp_cav_left_scaler,
            k_scaler=k_cav_left_scaler,
            freq_scaler=freq_cav_left_scaler,
            phase_offset=phase_cav_left_offset,
            x_start=x_start_cav_left,
            x_end=x_end_cav_left
        )

        # Transmitted Beam
        amp_trans = amp_input
        k_trans = k_input
        freq_trans = freq_input
        time_trans = 0.0
        phase_trans = phase_input
        x_start_trans = x_end_mirror
        x_end_trans = x_end_input

        start_trans = np.array([3.0, 0.0, 0.0])
        end_trans = np.array([7.0, 0.0, 0.0])

        cav_trans = get_cav_trans(phi, r1, r2)

        amp_trans_scaler = ValueTracker(np.abs(cav_trans))
        k_trans_scaler = 1
        freq_trans_scaler = 1
        phase_trans_offset = ValueTracker(np.angle(cav_trans))

        self.trans_beam = self.make_electric_field(
            amp_trans, 
            k_trans, 
            freq_trans, 
            time_trans, 
            phase_trans, 
            start_trans, 
            end_trans, 
            number_of_lines,
            color=GREEN_C,
            amp_scaler=amp_trans_scaler,
            k_scaler=k_trans_scaler,
            freq_scaler=freq_trans_scaler,
            phase_offset=phase_trans_offset,
            x_start=x_start_trans,
            x_end=x_end_trans
        )

        ## Standing Waves
        # Before cavity
        self.standing_beam_refl = self.sum_electric_fields(self.input_beam, self.refl_beam)

        self.standing_beam_refl.forward_beam = self.input_beam
        self.standing_beam_refl.backward_beam = self.refl_beam

        # Inside cavity
        self.standing_beam_intra = self.sum_electric_fields(self.cav_right_beam, self.cav_left_beam, color=RED_A)

        self.standing_beam_intra.forward_beam = self.cav_right_beam
        self.standing_beam_intra.backward_beam = self.cav_left_beam

        ## Tex
        # Electric field equations
        self.input_tex = Tex(r"$E_\mathrm{in}$", color=RED).shift(3*UP + 4.5*LEFT)
        self.equals_one = Tex(r"$=$").next_to(self.input_tex, LEFT)
        self.standing_refl_tex = Tex(r"$E_\mathrm{total}$", color=YELLOW).next_to(self.equals_one, LEFT)
        self.plus_one = Tex(r"$+$").next_to(self.equals_one, DOWN, buff=0.5)
        self.refl_tex = Tex(r"$E_\mathrm{refl}$", color=ORANGE).next_to(self.plus_one, RIGHT)


        self.equals_two = Tex(r"$=$").shift(3*UP)
        self.cav_right_tex = Tex(r"$E_\mathrm{cav\,right}$", color=PURPLE).next_to(self.equals_two, RIGHT)
        self.standing_intra_tex = Tex(r"$E_\mathrm{cav}$", color=RED_A).next_to(self.equals_two, LEFT)
        self.plus_two = Tex(r"$+$").next_to(self.equals_two, DOWN, buff=0.5)
        self.cav_left_tex = Tex(r"$E_\mathrm{cav\,left}$", color=PINK).next_to(self.plus_two, RIGHT)

        self.trans_tex = Tex(r"$E_\mathrm{trans}$", color=GREEN_C).shift(3*UP + 5*RIGHT)

        # Eq groups
        self.front_equation = VGroup(self.input_tex, self.plus_one, self.refl_tex, self.equals_one, self.standing_refl_tex)
        self.middle_equation = VGroup(self.cav_right_tex, self.plus_two, self.cav_left_tex, self.equals_two, self.standing_intra_tex)

        # Mirrors
        self.input_mirror_tex = Tex(r"Input", color=BLUE).next_to(self.input_mirror, DOWN, buff=0.3)
        self.input_mirror_refl_value_tex = Tex(r"$r_i = $", color=BLUE)
        self.input_mirror_refl_value = always_redraw(
            lambda: DecimalNumber(num_decimal_places=2, color=BLUE)
            .set_value(self.input_mirror_refl.get_value())
            .next_to(self.input_mirror_refl_value_tex, RIGHT, buff=0.1)
        )
        self.input_mirror_tex_group = VGroup(
            self.input_mirror_refl_value_tex, self.input_mirror_refl_value
        ).next_to(self.input_mirror_tex, DOWN, buff=0.3)
        

        self.end_mirror_tex = always_redraw(
            lambda: Tex(r"End", color=BLUE)
            .next_to(self.end_mirror, DOWN, buff=0.3)
        )
        self.end_mirror_refl_value_tex = Tex(r"$r_e = $", color=BLUE)
        self.end_mirror_refl_value = always_redraw(
            lambda: DecimalNumber(num_decimal_places=2, color=BLUE)
            .set_value(self.end_mirror_refl.get_value())
            .next_to(self.end_mirror_refl_value_tex, RIGHT, buff=0.1)
        )
        self.end_mirror_tex_group = always_redraw(
            lambda: VGroup(self.end_mirror_refl_value_tex, self.end_mirror_refl_value)
            .next_to(self.end_mirror_tex, DOWN, buff=0.3)     
        )   

        # Length as Decimal Number
        self.cavity_length = ValueTracker(x_end_mirror.get_value() - x_input_mirror.get_value())
        self.length_value_tex = Tex(r"$L = $")
        self.length_value = always_redraw(
            lambda: DecimalNumber(num_decimal_places=2)
            .set_value(self.cavity_length.get_value())
            .next_to(self.length_value_tex, RIGHT, buff=0.1)
        )
        self.length_value_group = always_redraw(
            lambda: VGroup(self.length_value_tex, self.length_value)
            .move_to( (self.input_mirror_tex.get_center() + self.end_mirror_tex.get_center())/2.0 )
        )

        # Cavity gain as Decimal Number
        self.cav_gain_value_tex_init = MathTex(
            r"g = { E_\mathrm{cav\,right} \over E_\mathrm{in} }", 
            substrings_to_isolate=(r"g", r"E_\mathrm{cav\,right}", r"E_\mathrm{in}"),
        ).set_color_by_tex(r"g", YELLOW).set_color_by_tex(r"E_\mathrm{cav\,right}", PURPLE).set_color_by_tex(r"E_\mathrm{in}", RED).shift(3 * UP)

        self.cav_gain_value_tex = MathTex(
            r"g = { t_i \over 1 - r_i r_e e^{ {i 2 \pi (2 L) \over \lambda} } } = ", 
            substrings_to_isolate=(r"g", r"r_i", r"r_e"),
        ).set_color_by_tex(r"g", YELLOW).set_color_by_tex(r"r_i", BLUE).set_color_by_tex(r"r_e", BLUE).shift(3 * UP)

        self.cav_gain_value = always_redraw(
            lambda: DecimalNumber(num_decimal_places=2)
            .set_value(amp_cav_right_scaler.get_value())
            .next_to(self.cav_gain_value_tex, RIGHT, buff=0.2)
            .shift(0.15 * UP)
        )
        self.cav_gain_group = always_redraw(
            lambda: VGroup(self.cav_gain_value_tex, self.cav_gain_value)
        )

        ## Updaters
        def time_updater(mob, dt):
            self.timer += dt * self.rate
            return

        def wave_updater(mob):
            temp_mob = self.make_electric_field(
                mob.amp, mob.k, mob.freq, self.timer, mob.phase, mob.start, mob.end, mob.number_of_lines, mob.color, 
                mob.amp_scaler, mob.k_scaler, mob.freq_scaler, mob.phase_offset, mob.x_start, mob.x_end
            )
            mob.become(temp_mob)
            return 

        def standing_updater(mob):
            fmob = mob.forward_beam
            bmob = mob.backward_beam
            temp_fmob = self.make_electric_field(
                fmob.amp, fmob.k, fmob.freq, self.timer, fmob.phase, fmob.start, fmob.end, fmob.number_of_lines, fmob.color, 
                fmob.amp_scaler, fmob.k_scaler, fmob.freq_scaler, fmob.phase_offset, fmob.x_start, fmob.x_end
            )
            temp_bmob = self.make_electric_field(
                bmob.amp, bmob.k, bmob.freq, self.timer, bmob.phase, bmob.start, bmob.end, bmob.number_of_lines, bmob.color, 
                bmob.amp_scaler, bmob.k_scaler, bmob.freq_scaler, bmob.phase_offset, bmob.x_start, bmob.x_end
            )
            temp_mob = self.sum_electric_fields(temp_fmob, temp_bmob, color=mob.color)
            mob.become(temp_mob)
            return 

        # Add updaters to reflection ValueTrackers which need to be updated based on the cavity length
        # Remember to add these ValueTrackers to the Scene itself with self.add()
        self.add(self.cavity_length)

        self.cavity_length.add_updater(
            lambda z: z.set_value( x_end_mirror.get_value() - x_input_mirror.get_value() )
        )

        self.add(amp_refl_scaler)
        self.add(phase_refl_offset)

        amp_refl_scaler.add_updater(
            lambda z: z.set_value( 
                np.abs( 
                    get_cav_refl(
                        k_input.get_value() * self.cavity_length.get_value(), 
                        self.input_mirror_refl.get_value(),
                        self.end_mirror_refl.get_value()
                    ) 
                )
            )
        )

        phase_refl_offset.add_updater(
            lambda z: z.set_value( 
                np.angle( 
                    get_cav_refl(
                        k_input.get_value() * self.cavity_length.get_value(), 
                        self.input_mirror_refl.get_value(),
                        self.end_mirror_refl.get_value()
                    ) 
                )
            )
        )

        self.add(amp_cav_right_scaler)
        self.add(phase_cav_right_offset)

        amp_cav_right_scaler.add_updater(
            lambda z: z.set_value( 
                np.abs( 
                    get_cav_buildup_toward_end_mirror(
                        k_input.get_value() * self.cavity_length.get_value(), 
                        self.input_mirror_refl.get_value(),
                        self.end_mirror_refl.get_value()
                    ) 
                )
            )
        )

        phase_cav_right_offset.add_updater(
            lambda z: z.set_value( 
                np.angle( 
                    get_cav_buildup_toward_end_mirror(
                        k_input.get_value() * self.cavity_length.get_value(), 
                        self.input_mirror_refl.get_value(),
                        self.end_mirror_refl.get_value()
                    ) 
                )
            )
        )

        self.add(amp_cav_left_scaler)
        self.add(phase_cav_left_offset)

        amp_cav_left_scaler.add_updater(
            lambda z: z.set_value( 
                np.abs( 
                    get_cav_buildup_toward_input_mirror(
                        k_input.get_value() * self.cavity_length.get_value(), 
                        self.input_mirror_refl.get_value(),
                        self.end_mirror_refl.get_value()
                    ) 
                )
            )
        )

        phase_cav_left_offset.add_updater(
            lambda z: z.set_value( 
                np.angle( 
                    get_cav_buildup_toward_input_mirror(
                        k_input.get_value() * self.cavity_length.get_value(), 
                        self.input_mirror_refl.get_value(),
                        self.end_mirror_refl.get_value()
                    ) 
                )
            )
        )

        self.add(amp_trans_scaler)
        self.add(phase_trans_offset)

        amp_trans_scaler.add_updater(
            lambda z: z.set_value( 
                np.abs( 
                    get_cav_trans(
                        k_input.get_value() * self.cavity_length.get_value(), 
                        self.input_mirror_refl.get_value(),
                        self.end_mirror_refl.get_value()
                    ) 
                )
            )
        )

        phase_trans_offset.add_updater(
            lambda z: z.set_value( 
                np.angle( 
                    get_cav_trans(
                        k_input.get_value() * self.cavity_length.get_value(), 
                        self.input_mirror_refl.get_value(),
                        self.end_mirror_refl.get_value()
                    ) 
                )
            )
        )

        # Set the scene and play it
        # self.show_axis()

        self.play(GrowFromCenter(self.input_mirror))
        self.play(Create(self.input_mirror_tex))
        self.play(Create(self.input_mirror_tex_group))
        self.wait(0.5)

        self.play(GrowFromCenter(self.end_mirror))
        self.play(Create(self.end_mirror_tex))
        self.play(Create(self.end_mirror_tex_group))
        self.wait(0.5)


        self.play(Create(self.length_value_group))
        self.wait(0.5)

        self.play(Create(self.input_tex))
        self.play(Create(self.input_beam))
        self.wait(0.5)

        self.play(Create(self.refl_tex))
        self.play(Create(self.refl_beam))
        self.wait(0.5)

        self.play(Create(self.cav_right_tex))
        self.play(Create(self.cav_right_beam))
        self.wait(0.5)
        
        self.play(Create(self.cav_left_tex))
        self.play(Create(self.cav_left_beam))
        self.wait(0.5)

        self.play(Create(self.trans_tex))
        self.play(Create(self.trans_beam))
        self.wait(0.5)

        self.play(Create(self.standing_refl_tex))
        self.play(Create(self.standing_beam_refl))
        self.wait(0.5)

        self.play(Create(self.standing_intra_tex))
        self.play(Create(self.standing_beam_intra))
        self.wait(0.5)

        self.play(
            Create(self.equals_one),
            Create(self.plus_one),
            Create(self.equals_two),
            Create(self.plus_two)
        )
        self.wait(0.5)

        self.play(
            Uncreate(self.front_equation),
            Uncreate(self.middle_equation),
            Uncreate(self.trans_tex)
        )
        self.wait(1.0)

        self.play(Create(self.cav_gain_value_tex_init))
        self.wait(1.0)

        self.play(
            Transform(self.cav_gain_value_tex_init, self.cav_gain_value_tex),
            Create(self.cav_gain_value)
        )
        self.wait(1.0)

        # self.input_beam.add_updater(time_updater)
        self.input_beam.add_updater(wave_updater)
        self.refl_beam.add_updater(wave_updater)
        self.cav_right_beam.add_updater(wave_updater)
        self.cav_left_beam.add_updater(wave_updater)
        self.trans_beam.add_updater(wave_updater)

        self.standing_beam_refl.add_updater(standing_updater)
        self.standing_beam_intra.add_updater(standing_updater)
        
        self.play(
            self.input_mirror_refl.animate.set_value(0.9),
            self.end_mirror_refl.animate.set_value(0.9), 
            run_time=10
        )
        self.wait(2)

        # self.play(x_end_mirror.animate.set_value(3.0), run_time=2*PI)
        # self.wait(2)
