"""michelson_review.py
Overview the michelson results we just derived
"""

from manim import *

class MichelsonReview(Scene):
    def construct(self):

        self.color_map = {
            "E_in" : RED,
            "E_x1" : RED_A,
            "E_y1" : RED_E,
            "E_x2" : ORANGE,
            "E_y2" : PINK,
            "E_refl" : YELLOW,
            "E_refl_x" : YELLOW_A,
            "E_refl_y" : YELLOW_E,
            "E_trans" : GREEN,
            "E_trans_x" : PURPLE,
            "E_trans_y" : GREY,
            "mirror" : BLUE,
            "lc" : TEAL,
            "ld" : MAROON,
        }

        # ## Tex
        self.michelson_interferometer_review_tex = Tex(
            "Michelson interferometer review",
            color=RED
        ).move_to(3*UP + 6*LEFT, aligned_edge=LEFT)


        self.answer_1_a = Tex(
            "{{ Differential }} motion changes the amount of "
        ).scale(0.7).next_to(self.michelson_interferometer_review_tex, DOWN, aligned_edge=LEFT).shift(0.5*DOWN)
        self.answer_1_a[0].set_color(self.color_map["ld"])
        
        self.answer_1_b = Tex(
            "{{ laser power }} in each beamsplitter port"
        ).scale(0.7).next_to(self.answer_1_a, DOWN, aligned_edge=LEFT)
        self.answer_1_b[0].set_color(RED)


        self.answer_2_a = Tex(
            "{{ Common }} motion creates an overall phase shift, "
        ).scale(0.7).next_to(self.answer_1_b, DOWN, aligned_edge=LEFT).shift(0.3*DOWN)
        self.answer_2_a[0].set_color(self.color_map["lc"])
        
        self.answer_2_b = Tex(
            "but does not change {{ power }} levels"
        ).scale(0.7).next_to(self.answer_2_a, DOWN, aligned_edge=LEFT)
        self.answer_2_b[1].set_color(RED)


        self.answer_3_a = Tex(
            "{{ Differential }} motion can be sensed with the "
        ).scale(0.7).next_to(self.answer_2_b, DOWN, aligned_edge=LEFT).shift(0.3*DOWN)
        self.answer_3_a[0].set_color(self.color_map["ld"])

        self.answer_3_b = Tex(
            "{{ photodetector }} at the antisymmetric port, a.k.a. {{ transmission }} port"
        ).scale(0.7).next_to(self.answer_3_a, DOWN, aligned_edge=LEFT)
        self.answer_3_b[0].set_color(GREEN)
        self.answer_3_b[2].set_color(GREEN)


        self.answer_4_a = Tex(
            "{{ Gravitational waves }} naturally create {{ differential }} motion"
        ).scale(0.7).next_to(self.answer_3_b, DOWN, aligned_edge=LEFT).shift(0.3*DOWN)
        self.answer_4_a[0].set_color(ORANGE)
        self.answer_4_a[2].set_color(self.color_map["ld"])


        # Equations
        self.refl_eq = MathTex(
            r"{ {{ E_\mathrm{refl} }} \over {{ E_\mathrm{in} }} }{{ = }}e^{ i 2 k {{ \ell_c }} }{{ }}\cos{( 2 k {{ \ell_d }} )}"
        ).scale(0.8).move_to(2*UP + 4*RIGHT)
        self.refl_eq[1].set_color(self.color_map["E_refl"])
        self.refl_eq[3].set_color(self.color_map["E_in"])
        self.refl_eq[7].set_color(self.color_map["lc"])
        self.refl_eq[11].set_color(self.color_map["ld"])

        self.trans_eq = MathTex(
            r"{ {{ E_\mathrm{trans} }} \over {{ E_\mathrm{in} }} }{{ = }}-i e^{ i 2 k {{ \ell_c }} }{{ }}\sin{( 2 k{{ \ell_d }} )}"
        ).scale(0.8).next_to(self.refl_eq, DOWN)
        self.trans_eq[1].set_color(GREEN)
        self.trans_eq[3].set_color(RED)
        self.trans_eq[7].set_color(TEAL)
        self.trans_eq[11].set_color(MAROON)

        # Q1
        self.play(
            Write(self.michelson_interferometer_review_tex)
        )
        self.wait(2)
        
        # answer 1
        self.play(
            Write(self.answer_1_a)
        )
        self.play(
            Write(self.answer_1_b)
        )
        self.wait(1)

        # Write equations
        self.play(
            FadeIn(self.refl_eq),
            run_time=2
        )
        self.wait(1)
        self.play(
            FadeIn(self.trans_eq),
            run_time=2
        )
        self.wait(3)

        # answer 2
        self.play(
            Write(self.answer_2_a)
        )
        self.play(
            Write(self.answer_2_b)
        )
        self.wait(3)

        # answer 3
        self.play(
            Write(self.answer_3_a)
        )
        self.play(
            Write(self.answer_3_b)
        )
        self.wait(3)

        # answer 4
        self.play(
            Write(self.answer_4_a)
        )
        self.wait(3)
