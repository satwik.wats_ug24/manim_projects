"""fabry_perot_review.py
Overview the fabry_perot results we just derived
"""

from manim import *

class FabryPerotReview(Scene):
    def construct(self):

        self.color_map = {
            "E_in" : RED,
            "E_x1" : RED_A,
            "E_y1" : RED_E,
            "E_x2" : ORANGE,
            "E_y2" : PINK,
            "E_refl" : YELLOW,
            "E_refl_x" : YELLOW_A,
            "E_refl_y" : YELLOW_E,
            "E_trans" : GREEN,
            "E_trans_x" : PURPLE,
            "E_trans_y" : GREY,
            "mirror" : BLUE,
            "lc" : TEAL,
            "ld" : MAROON,
        }

        # ## Tex
        self.fabry_perot_interferometer_review_tex = Tex(
            r"Fabry P\'erot interferometer review",
            color=PURPLE
        ).move_to(3*UP + 6*LEFT, aligned_edge=LEFT)


        self.answer_1_a = Tex(
            "{{ Two-mirror }} cavity separated by distance $L$"
        ).scale(0.7).next_to(self.fabry_perot_interferometer_review_tex, DOWN, aligned_edge=LEFT).shift(0.2*DOWN)
        self.answer_1_a[0].set_color(self.color_map["mirror"])
        
        # self.answer_1_b = Tex(
        #     "{{ laser power }} in each beamsplitter port"
        # ).scale(0.7).next_to(self.answer_1_a, DOWN, aligned_edge=LEFT)
        # self.answer_1_b[0].set_color(RED)


        self.answer_2_a = Tex(
            r"When $L = {\lambda \over 2} n$, the beams inside the cavity"
        ).scale(0.7).next_to(self.answer_1_a, DOWN, aligned_edge=LEFT).shift(0.3*DOWN)
        # self.answer_2_a[0].set_color(self.color_map["lc"])
        
        self.answer_2_b = Tex(
            "constructively interfere, a.k.a. {{ resonate }}"
        ).scale(0.7).next_to(self.answer_2_a, DOWN, aligned_edge=LEFT)
        self.answer_2_b[1].set_color(YELLOW)


        self.answer_3_a = Tex(
            "For {{ highly reflective mirrors, }} the number of internal reflections increases,"
        ).scale(0.7).next_to(self.answer_2_b, DOWN, aligned_edge=LEFT).shift(0.3*DOWN)
        self.answer_3_a[1].set_color(self.color_map["mirror"])

        self.answer_3_b = Tex(
            r"vastly increasing the {{ resonant power }} of the {{ Fabry P\'erot }}"
        ).scale(0.7).next_to(self.answer_3_a, DOWN, aligned_edge=LEFT)
        self.answer_3_b[1].set_color(RED)
        self.answer_3_b[3].set_color(ORANGE)
        self.answer_3_b[3].set_color(PURPLE)

        self.answer_4_a = Tex(
            r"High {{ resonant power }} increases the {{ Fabry P\'erot }}"
        ).scale(0.7).next_to(self.answer_3_b, DOWN, aligned_edge=LEFT).shift(0.3*DOWN)
        self.answer_4_a[1].set_color(RED)
        self.answer_4_a[3].set_color(PURPLE)

        self.answer_4_b = Tex(
            "{{ reflected }} response to changes in the cavity length $L$"
        ).scale(0.7).next_to(self.answer_4_a, DOWN, aligned_edge=LEFT)
        self.answer_4_b[0].set_color(ORANGE)

        self.answer_5_a = Tex(
            r"{{ Gravitational waves }} naturally change the length $L$"
        ).scale(0.7).next_to(self.answer_4_b, DOWN, aligned_edge=LEFT).shift(0.3*DOWN)
        self.answer_5_a[0].set_color(ORANGE)
        

        self.answer_5_b = Tex(
            r"of a {{ Fabry P\'erot }} interferometer"
        ).scale(0.7).next_to(self.answer_5_a, DOWN, aligned_edge=LEFT)
        self.answer_5_b[1].set_color(PURPLE)


        # Equations
        self.cav_gain_eq = MathTex(
            r"{{ g }} = { {{ \tau_i }} \over 1 - {{ r_i }}{{ r_e }} e^{ i k (2 L) } }"
        ).scale(0.8).move_to(DOWN + 4.5*RIGHT)
        self.cav_gain_eq[0].set_color(YELLOW)
        self.cav_gain_eq[2].set_color(self.color_map["mirror"])
        self.cav_gain_eq[4].set_color(self.color_map["mirror"])
        self.cav_gain_eq[5].set_color(self.color_map["mirror"])

        self.electric_field_refl_deriv_3 = MathTex(
            r"{ d {{ r_\mathrm{cav} }} \over dL }{{ = }}{ i 2 k {{ \tau_i }}^2 {{ r_e }} \over ( 1 - {{ r_i }}{{ r_e }} )^2 }"
        ).scale(0.8).next_to(self.cav_gain_eq, DOWN).shift(0.5*RIGHT)
        self.electric_field_refl_deriv_3[1].set_color(ORANGE)
        self.electric_field_refl_deriv_3[5].set_color(self.color_map["mirror"])
        self.electric_field_refl_deriv_3[7].set_color(self.color_map["mirror"])
        self.electric_field_refl_deriv_3[9].set_color(self.color_map["mirror"])
        self.electric_field_refl_deriv_3[10].set_color(self.color_map["mirror"])

        # Small FP
        small_height = 2.2
        mirror_height = 1.2
        mirror_width = 0.3
        x_shift = -2
        self.input_mirror_small = Rectangle(
            height=mirror_height, width=mirror_width, fill_color=BLUE, fill_opacity=0.5, color=BLUE
        )
        self.input_mirror_small.next_to(self.answer_1_a, RIGHT).shift(1.5*RIGHT)

        self.end_mirror_small = Rectangle(
            height=mirror_height, width=mirror_width, fill_color=BLUE, fill_opacity=1.0, color=BLUE
        )
        self.end_mirror_small.move_to(self.input_mirror_small).shift(4*RIGHT)

        # Small arrows
        self.input_arrow_small = Arrow(
            start=self.input_mirror_small.get_center() + LEFT,
            end=self.input_mirror_small.get_center(),
            color=RED
        )
        self.cav_east_arrow_small = Arrow(
            start=self.input_mirror_small.get_center() + 0.3*UP,
            end=self.end_mirror_small.get_center() + 0.3*UP,
            color=PURPLE
        )
        self.cav_west_arrow_small = Arrow(
            start=self.end_mirror_small.get_center() - 0.3*UP,
            end=self.input_mirror_small.get_center() - 0.3*UP,
            color=PINK
        )

        # Q1
        self.play(
            Write(self.fabry_perot_interferometer_review_tex)
        )
        self.wait(2)
        
        # answer 1
        self.play(
            Write(self.answer_1_a)
        )
        self.wait(1)

        # Make FP
        self.play(
            FadeIn(self.input_mirror_small),
            FadeIn(self.end_mirror_small)
        )
        self.play(
            FadeIn(self.input_arrow_small)
        )
        self.play(
            FadeIn(self.cav_east_arrow_small)
        )
        self.play(
            FadeIn(self.cav_west_arrow_small)
        )
        self.wait(2)

        # answer 2
        self.play(
            Write(self.answer_2_a)
        )
        self.play(
            Write(self.answer_2_b)
        )
        self.wait(3)

        # answer 3
        self.play(
            Write(self.answer_3_a)
        )
        self.play(
            Write(self.answer_3_b)
        )
        self.wait(1)
        
        # Write equations
        self.play(
            FadeIn(self.cav_gain_eq),
            run_time=2
        )
        self.wait(3)

        # answer 4 
        self.play(
            Write(self.answer_4_a)
        )
        self.play(
            Write(self.answer_4_b)
        )
        self.wait(1)

        # Write equations
        self.play(
            FadeIn(self.electric_field_refl_deriv_3),
            run_time=2
        )
        self.wait(3)

        # answer 5
        self.play(
            Write(self.answer_5_a)
        )
        self.play(
            Write(self.answer_5_b)
        )
        self.wait(3)
