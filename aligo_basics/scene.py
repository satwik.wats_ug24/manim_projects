from manim import *


class SquareToCircle(Scene):
    def construct(self):
        circle = Circle()  # create a circle
        circle.set_fill(PINK, opacity=0.5)  # set color and transparency

        square = Square()  # create a square
        square.rotate(PI / 4)  # rotate a certain amount

        self.play(Create(square))  # animate the creation of the square
        self.play(Transform(square, circle))  # interpolate the square into the circle
        self.play(FadeOut(square))  # fade out animation

class VectorArrow(Scene):
    def construct(self):
        dot = Dot(ORIGIN)
        arrow = Arrow(ORIGIN, [2, 2, 0], buff=0)
        numberplane = NumberPlane()
        origin_text = Text('(0, 0)').next_to(dot, DOWN)
        tip_text = Text('(2, 2)').next_to(arrow.get_end(), RIGHT)
        self.add(numberplane, dot, arrow, origin_text, tip_text)

class SineCurveUnitCircle(Scene):
    # contributed by heejin_park, https://infograph.tistory.com/230
    def construct(self):
        self.show_axis()
        self.show_circle()
        self.move_dot_and_draw_curve()
        self.wait()

    def show_axis(self):
        x_start = np.array([-6,0,0])
        x_end = np.array([6,0,0])

        y_start = np.array([-4,-2,0])
        y_end = np.array([-4,2,0])

        x_axis = Line(x_start, x_end)
        y_axis = Line(y_start, y_end)

        # self.add(x_axis, y_axis)
        self.add_x_labels()

        self.origin_point = np.array([-4,0,0])
        self.curve_start = np.array([-3,0,0])

    def add_x_labels(self):
        x_labels = [
            MathTex("\pi"), MathTex("2 \pi"),
            MathTex("3 \pi"), MathTex("4 \pi"),
        ]

        for i in range(len(x_labels)):
            x_labels[i].next_to(np.array([-1 + 2*i, 0, 0]), DOWN)
            self.add(x_labels[i])

    def show_circle(self):
        circle = Circle(radius=1)
        circle.move_to(self.origin_point)
        self.add(circle)
        self.circle = circle

    def move_dot_and_draw_curve(self):
        orbit = self.circle
        origin_point = self.origin_point

        dot = Dot(radius=0.08, color=YELLOW)
        dot.move_to(orbit.point_from_proportion(0))
        self.t_offset = 0
        rate = 0.25

        def go_around_circle(mob, dt):
            self.t_offset += (dt * rate)
            # print(self.t_offset)
            mob.move_to(orbit.point_from_proportion(self.t_offset % 1))

        def get_line_to_circle():
            return Line(origin_point, dot.get_center(), color=BLUE)

        def get_line_to_curve():
            x = self.curve_start[0] + self.t_offset * 4
            y = dot.get_center()[1]
            return Line(dot.get_center(), np.array([x,y,0]), color=YELLOW_A, stroke_width=2 )


        self.curve = VGroup()
        self.curve.add(Line(self.curve_start,self.curve_start))
        def get_curve():
            last_line = self.curve[-1]
            x = self.curve_start[0] + self.t_offset * 4
            y = dot.get_center()[1]
            new_line = Line(last_line.get_end(),np.array([x,y,0]), color=YELLOW_D)
            self.curve.add(new_line)

            return self.curve

        dot.add_updater(go_around_circle)

        origin_to_circle_line = always_redraw(get_line_to_circle)
        dot_to_curve_line = always_redraw(get_line_to_curve)
        sine_curve_line = always_redraw(get_curve)

        self.add(dot)
        self.add(orbit, origin_to_circle_line, dot_to_curve_line, sine_curve_line)
        self.wait(8.5)

        dot.remove_updater(go_around_circle)

class InputBeamPlusMirrorVsTime(Scene):
    def get_phase(self, k=1, x=0, freq=1, time=0, phase=0):
        return k * x - 2 * PI * freq * time + phase
    def get_sine_wave(self, amp=1, k=1, x=0, freq=1, time=0, phase=0):
        return  amp * np.sin(k * x - 2 * PI * freq * time + phase)

    def rotate(self, x1, y1, theta, origin=np.array([0,0,0])):
        """Return x2 and y2 being rotated about the origin by theta.
        """
        x0 = origin[0]
        y0 = origin[1]
        x2 = (x1 - x0) * np.cos(theta) - (y1 - y0) * np.sin(theta)
        y2 = (x1 - x0) * np.sin(theta) + (y1 - y0) * np.cos(theta)
        return x2, y2

    def show_axis(self):
        x_start = np.array([-5,0,0])
        x_end = np.array([5,0,0])

        y_start = np.array([0,-3,0])
        y_end = np.array([0,3,0])

        x_axis = Line(x_start, x_end)
        y_axis = Line(y_start, y_end)

        # # self.add(x_axis, y_axis)

        self.origin_point = np.array([0,0,0])

        return

    def make_electric_field(
        self, 
        amp=ValueTracker(1), 
        k=ValueTracker(1), 
        freq=ValueTracker(1), 
        time=0, 
        phase=ValueTracker(0), 
        start=np.array([-5,0,0]), 
        end=np.array([5,0,0]), 
        number_of_lines=50, 
        color=RED,
        amp_scaler=ValueTracker(1),
        k_scaler=1,
        freq_scaler=1,
        phase_offset=0
    ):
        """Makes an electric field VGroup of Lines.
        Electric field is defined by amp, k, freq, time, and phase ValueTrackers.
        start and end are 3D vectors defining the beginning and end of the wave.
        number_of_lines defines how many Line segments the wave is broken up into.
        Returns the VGroup.
        """
        # Define the VGroup of Lines which make up the E-field, and store some ValueTrackers
        beam = VGroup()
        beam.amp = amp
        beam.k = k
        beam.freq = freq
        beam.time = time
        beam.phase = phase

        beam.amp_scaler = amp_scaler
        beam.k_scaler = k_scaler
        beam.freq_scaler = freq_scaler
        beam.phase_offset = phase_offset

        amp_float = amp_scaler.get_value() * amp.get_value()
        k_float = k_scaler * k.get_value()
        freq_float = freq_scaler * freq.get_value()
        phase_float = phase_offset + phase.get_value()

        # Get the start and end vector info.
        # This function will first calculate the wave along the x axis,
        # then rotate it appropriately.
        span = end - start
        origin = (start + end) / 2
        total_length = np.sqrt(np.sum(span**2))
        norm = span / total_length
        theta = np.arctan2(norm[1], norm[0])

        x_diff = total_length / number_of_lines

        phase_output = self.get_phase(k=k_float, x=total_length, freq=freq_float, time=time, phase=0)

        # Store some additional info
        beam.phase_output = phase_output
        beam.start = start
        beam.end = end
        beam.origin = origin
        beam.theta = theta
        beam.number_of_lines = number_of_lines
        beam.color = color

        for ii in range(number_of_lines):
            # Calculate wave in 1D
            temp_x_start = ii * x_diff
            temp_x_end = (ii + 1) * x_diff

            temp_y_start = self.get_sine_wave(amp_float, k_float, temp_x_start, freq_float, time, phase_float)
            temp_y_end = self.get_sine_wave(amp_float, k_float, temp_x_end, freq_float, time, phase_float)

            # Translate to origin = (0, 0)
            temp_x_start -= total_length/2
            temp_x_end -= total_length/2
            # temp_y_start += origin[1]
            # temp_y_end += origin[1]

            # Rotate about origin
            temp_x_start, temp_y_start = self.rotate(temp_x_start, temp_y_start, theta)
            temp_x_end, temp_y_end = self.rotate(temp_x_end, temp_y_end, theta)

            # Translate to sine wave position beam.origin
            temp_x_start += origin[0]
            temp_x_end += origin[0]
            temp_y_start += origin[1]
            temp_y_end += origin[1]

            # Define the Line
            temp_start = np.array([temp_x_start, temp_y_start, 0])
            temp_end = np.array([temp_x_end, temp_y_end, 0])

            temp_line = Line(temp_start, temp_end, color=color)
            beam.add(temp_line)

        return beam

    def sum_electric_fields(self, field1, field2, color=YELLOW, reverse=True):
        """Add together two VGroups of electric fields
        """
        if field1.number_of_lines != field2.number_of_lines:
            print()
            print("Cannot sum two fields of different lengths")
            return
        
        beam = VGroup()
        for ii in range(field1.number_of_lines):
            if reverse:
                jj = (field1.number_of_lines - 1) - ii
            else:
                jj = ii
        
            line1 = field1[ii]
            line2 = field2[jj]

            if reverse:
                new_line_start = line1.start
                new_line_end = line1.end
                new_line_start[1] = line1.start[1] + line2.end[1]
                new_line_end[1] = line1.end[1] + line2.start[1]
            else:
                new_line_start = line1.start 
                new_line_end = line1.end 
                new_line_start[1] = line1.start[1] + line2.start[1]
                new_line_end[1] = line1.end[1] + line2.end[1]

            new_line = Line(new_line_start, new_line_end, color=color)
            beam.add(new_line)

        return beam

    def get_trans_value(self, refl):
        """Get amplitude trans value from amplitude refl
        """
        trans = np.sqrt(1 - refl**2)
        return trans

    def construct(self):
        # Set up a timer variable
        self.timer = 0.0
        self.rate = 0.25
        number_of_lines = 100

        # Input Beam
        amp_input = ValueTracker(0.75)
        k_input = ValueTracker(2*PI/5)
        freq_input = ValueTracker(1)
        time_input = 0.0
        phase_input = ValueTracker(PI/2)

        start_input = np.array([-7.0, 0, 0])
        end_input = np.array([3.0, 0, 0])

        self.input_beam = self.make_electric_field(
            amp_input, 
            k_input, 
            freq_input, 
            time_input, 
            phase_input, 
            start_input, 
            end_input, 
            number_of_lines
        )

        # Mirror
        self.mirror_refl = ValueTracker(1.0)
        self.mirror_trans = ValueTracker(0.0)

        # from Note after last example here:
        # https://docs.manim.community/en/v0.8.0/reference/manim.mobject.value_tracker.ValueTracker.html
        # You have to add the ValueTracker you want to change to the scene to add and updater to it
        self.add(self.mirror_trans)

        self.mirror_trans.add_updater(
            lambda z: z.set_value( np.sqrt(1.0 - self.mirror_refl.get_value()**2) )
        )

        self.right_mirror = Rectangle(
            height=3.2, width=0.5, fill_color=BLUE, fill_opacity=0.9, color=BLUE
        ).shift(3.25*RIGHT)

        # Refl Beam
        amp_refl = amp_input
        k_refl = k_input
        freq_refl = freq_input
        time_refl = 0.0
        phase_refl = phase_input

        start_refl = np.array([3.0, 0, 0])
        end_refl = np.array([-7.0, 0, 0])

        amp_refl_scaler = self.mirror_refl
        k_refl_scaler = 1
        freq_refl_scaler = 1
        phase_refl_offset = self.input_beam.phase_output

        self.refl_beam = self.make_electric_field(
            amp_refl, 
            k_refl, 
            freq_refl, 
            time_refl, 
            phase_refl, 
            start_refl, 
            end_refl, 
            number_of_lines,
            color=ORANGE,
            amp_scaler=amp_refl_scaler,
            k_scaler=k_refl_scaler,
            freq_scaler=freq_refl_scaler,
            phase_offset=phase_refl_offset
        )

        # Standing Wave
        self.standing_beam = self.sum_electric_fields(self.input_beam, self.refl_beam)

        self.standing_beam.forward_beam = self.input_beam
        self.standing_beam.backward_beam = self.refl_beam

        # Trans Beam
        amp_trans = amp_input
        k_trans = k_input
        freq_trans = freq_input
        time_trans = 0.0
        phase_trans = phase_input

        start_trans = np.array([3.0, 0.0, 0.0])
        end_trans = np.array([7.0, 0.0, 0.0])

        amp_trans_scaler = self.mirror_trans
        k_trans_scaler = 1
        freq_trans_scaler = 1
        phase_trans_offset = self.input_beam.phase_output

        self.trans_beam = self.make_electric_field(
            amp_trans, 
            k_trans, 
            freq_trans, 
            time_trans, 
            phase_trans, 
            start_trans, 
            end_trans, 
            number_of_lines,
            color=GREEN_E,
            amp_scaler=amp_trans_scaler,
            k_scaler=k_trans_scaler,
            freq_scaler=freq_trans_scaler,
            phase_offset=phase_trans_offset
        )

        ## Tex
        # Names of waves
        self.input_tex = Tex(r"Input beam", color=RED).shift(3*UP + 4*LEFT)
        self.refl_tex = Tex(r"Reflected beam", color=ORANGE).next_to(self.input_tex, DOWN, buff=0.2)
        self.standing_tex = Tex(r"Standing wave", color=YELLOW).next_to(self.refl_tex, DOWN, buff=0.2)
        self.trans_tex = Tex(r"Transmitted beam", color=GREEN_E).next_to(self.trans_beam, UP, buff=2.0)

        self.right_mirror_tex = Tex(r"Mirror", color=BLUE).next_to(self.right_mirror, DOWN, buff=0.25)

        self.right_mirror_refl_tex = Tex(r"$R = $", color=ORANGE)
        self.right_mirror_refl_value = always_redraw(
            lambda: DecimalNumber(num_decimal_places=2, color=ORANGE)
            .set_value(self.mirror_refl.get_value()**2)
            .next_to(self.right_mirror_refl_tex, RIGHT, buff=0.1)
        )

        self.right_mirror_trans_tex = Tex(r"$T = $", color=GREEN_E)
        self.right_mirror_trans_value = always_redraw(
            lambda: DecimalNumber(num_decimal_places=2, color=GREEN_E)
            .set_value(self.mirror_trans.get_value()**2)
            .next_to(self.right_mirror_trans_tex, RIGHT, buff=0.1)
        )

        self.right_mirror_refl_tex_group = VGroup(
            self.right_mirror_refl_tex, self.right_mirror_refl_value
        ).next_to(self.right_mirror_tex, DOWN, buff=0.25)

        self.right_mirror_trans_tex_group = VGroup(
            self.right_mirror_trans_tex, self.right_mirror_trans_value
        ).next_to(self.right_mirror_refl_tex_group, DOWN, buff=0.25)


        # Time as Decimal Number
        self.timer_value_tex = Tex(r"$t = $").shift(3*UP + 4*RIGHT)
        self.timer_value = always_redraw(
            lambda: DecimalNumber(num_decimal_places=1)
            .set_value(self.timer)
            .next_to(self.timer_value_tex, RIGHT, buff=0.1)
        )

        # Updaters
        def time_updater(mob, dt):
            self.timer += dt * self.rate
            return

        def wave_updater(mob):
            temp_mob = self.make_electric_field(
                mob.amp, mob.k, mob.freq, self.timer, mob.phase, mob.start, mob.end, mob.number_of_lines, mob.color, 
                mob.amp_scaler, mob.k_scaler, mob.freq_scaler, mob.phase_offset
            )
            mob.become(temp_mob)
            return 

        def standing_updater(mob):
            fmob = mob.forward_beam
            bmob = mob.backward_beam
            temp_fmob = self.make_electric_field(
                fmob.amp, fmob.k, fmob.freq, self.timer, fmob.phase, fmob.start, fmob.end, fmob.number_of_lines, fmob.color, 
                fmob.amp_scaler, fmob.k_scaler, fmob.freq_scaler, fmob.phase_offset
            )
            temp_bmob = self.make_electric_field(
                bmob.amp, bmob.k, bmob.freq, self.timer, bmob.phase, bmob.start, bmob.end, bmob.number_of_lines, bmob.color, 
                bmob.amp_scaler, bmob.k_scaler, bmob.freq_scaler, bmob.phase_offset
            )
            temp_mob = self.sum_electric_fields(temp_fmob, temp_bmob)
            mob.become(temp_mob)
            return 

        # Set the scene and play it
        self.show_axis()

        self.play(Create(self.input_tex))
        self.play(Create(self.input_beam))
        self.wait(0.5)

        self.play(Create(self.right_mirror_tex))
        self.play(Create(self.right_mirror))
        self.play(Create(self.right_mirror_refl_tex_group))
        self.play(Create(self.right_mirror_trans_tex_group))
        self.wait(0.5)

        self.play(Create(self.refl_tex))
        self.play(Create(self.refl_beam))
        self.wait(0.5)

        self.play(Create(self.standing_tex))
        self.play(Create(self.standing_beam))
        self.wait(0.5)

        self.play(Create(self.trans_tex))
        self.play(Create(self.trans_beam))
        self.wait(0.5)

        self.play(Create(self.timer_value_tex))
        self.play(Create(self.timer_value))
        self.wait(0.5)

        
        self.input_beam.add_updater(wave_updater)
        self.refl_beam.add_updater(wave_updater)
        self.standing_beam.add_updater(standing_updater)
        self.trans_beam.add_updater(wave_updater)

        self.play(self.mirror_refl.animate.set_value(np.sqrt(0.5)), run_time=PI)
        self.wait(2)

        self.input_beam.add_updater(time_updater)
        seconds_to_oscillate = 1
        self.wait(seconds_to_oscillate/self.rate)
        # self.wait(20)

class InputBeamPlusMirrorVsPhase(Scene):
    def get_phase(self, k=1, x=0, freq=1, time=0, phase=0):
        return k * x - 2 * PI * freq * time + phase
    def get_sine_wave(self, amp=1, k=1, x=0, freq=1, time=0, phase=0):
        return  amp * np.sin(k * x - 2 * PI * freq * time + phase)

    def rotate(self, x1, y1, theta, origin=np.array([0,0,0])):
        """Return x2 and y2 being rotated about the origin by theta.
        """
        x0 = origin[0]
        y0 = origin[1]
        x2 = (x1 - x0) * np.cos(theta) - (y1 - y0) * np.sin(theta)
        y2 = (x1 - x0) * np.sin(theta) + (y1 - y0) * np.cos(theta)
        return x2, y2

    def show_axis(self):
        x_start = np.array([-5,0,0])
        x_end = np.array([5,0,0])

        y_start = np.array([0,-3,0])
        y_end = np.array([0,3,0])

        x_axis = Line(x_start, x_end)
        y_axis = Line(y_start, y_end)

        # self.add(x_axis, y_axis)

        self.origin_point = np.array([0,0,0])

        return

    def make_electric_field(
        self, 
        amp=ValueTracker(1), 
        k=ValueTracker(1), 
        freq=ValueTracker(1), 
        time=0, 
        phase=ValueTracker(0), 
        start=np.array([-5,0,0]), 
        end=np.array([5,0,0]), 
        number_of_lines=50, 
        color=RED,
        amp_scaler=1,
        k_scaler=1,
        freq_scaler=1,
        phase_offset=0
    ):
        """Makes an electric field VGroup of Lines.
        Electric field is defined by amp, k, freq, time, and phase ValueTrackers.
        start and end are 3D vectors defining the beginning and end of the wave.
        number_of_lines defines how many Line segments the wave is broken up into.
        Returns the VGroup.
        """
        # Define the VGroup of Lines which make up the E-field, and store some ValueTrackers
        beam = VGroup()
        beam.amp = amp
        beam.k = k
        beam.freq = freq
        beam.time = time
        beam.phase = phase

        beam.amp_scaler = amp_scaler
        beam.k_scaler = k_scaler
        beam.freq_scaler = freq_scaler
        beam.phase_offset = phase_offset

        amp_float = amp_scaler * amp.get_value()
        k_float = k_scaler * k.get_value()
        freq_float = freq_scaler * freq.get_value()
        phase_float = phase_offset + phase.get_value()

        # Get the start and end vector info.
        # This function will first calculate the wave along the x axis,
        # then rotate it appropriately.
        span = end - start
        origin = (start + end) / 2
        total_length = np.sqrt(np.sum(span**2))
        norm = span / total_length
        theta = np.arctan2(norm[1], norm[0])

        x_diff = total_length / number_of_lines

        phase_output = self.get_phase(k=k_float, x=total_length, freq=freq_float, time=time, phase=0)

        # Store some additional info
        beam.phase_output = phase_output
        beam.start = start
        beam.end = end
        beam.origin = origin
        beam.theta = theta
        beam.number_of_lines = number_of_lines
        beam.color = color

        for ii in range(number_of_lines):
            # Calculate wave in 1D
            temp_x_start = ii * x_diff
            temp_x_end = (ii + 1) * x_diff

            temp_y_start = self.get_sine_wave(amp_float, k_float, temp_x_start, freq_float, time, phase_float)
            temp_y_end = self.get_sine_wave(amp_float, k_float, temp_x_end, freq_float, time, phase_float)

            # Translate to origin = (0, 0)
            temp_x_start -= total_length/2
            temp_x_end -= total_length/2
            # temp_y_start += origin[1]
            # temp_y_end += origin[1]

            # Rotate about origin
            temp_x_start, temp_y_start = self.rotate(temp_x_start, temp_y_start, theta)
            temp_x_end, temp_y_end = self.rotate(temp_x_end, temp_y_end, theta)

            # Translate to sine wave position beam.origin
            temp_x_start += origin[0]
            temp_x_end += origin[0]
            temp_y_start += origin[1]
            temp_y_end += origin[1]

            # Define the Line
            temp_start = np.array([temp_x_start, temp_y_start, 0])
            temp_end = np.array([temp_x_end, temp_y_end, 0])

            temp_line = Line(temp_start, temp_end, color=color)
            beam.add(temp_line)

        return beam

    def sum_electric_fields(self, field1, field2, color=YELLOW, reverse=True):
        """Add together two VGroups of electric fields
        """
        if field1.number_of_lines != field2.number_of_lines:
            print()
            print("Cannot sum two fields of different lengths")
            return
        
        beam = VGroup()
        for ii in range(field1.number_of_lines):
            if reverse:
                jj = (field1.number_of_lines - 1) - ii
            else:
                jj = ii
        
            line1 = field1[ii]
            line2 = field2[jj]

            if reverse:
                new_line_start = line1.start
                new_line_end = line1.end
                new_line_start[1] = line1.start[1] + line2.end[1]
                new_line_end[1] = line1.end[1] + line2.start[1]
            else:
                new_line_start = line1.start 
                new_line_end = line1.end 
                new_line_start[1] = line1.start[1] + line2.start[1]
                new_line_end[1] = line1.end[1] + line2.end[1]

            new_line = Line(new_line_start, new_line_end, color=color)
            beam.add(new_line)

        return beam


    def construct(self):
        # Set up a timer variable
        self.timer = 0.0
        self.rate = 0.25
        number_of_lines = 100

        # Input Beam
        amp_input = ValueTracker(0.75)
        k_input = ValueTracker(2*PI/5)
        freq_input = ValueTracker(1)
        time_input = 0.0
        phase_input = ValueTracker(PI/2)

        start_input = np.array([-5, 0, 0])
        end_input = np.array([5, 0, 0])

        self.input_beam = self.make_electric_field(
            amp_input, 
            k_input, 
            freq_input, 
            time_input, 
            phase_input, 
            start_input, 
            end_input, 
            number_of_lines
        )

        # Refl Beam
        amp_refl = amp_input
        k_refl = k_input
        freq_refl = freq_input
        time_refl = 0.0
        phase_refl = phase_input

        start_refl = np.array([5, 0, 0])
        end_refl = np.array([-5, 0, 0])

        amp_refl_scaler = 1
        k_refl_scaler = 1
        freq_refl_scaler = 1
        phase_refl_offset = self.input_beam.phase_output

        self.refl_beam = self.make_electric_field(
            amp_refl, 
            k_refl, 
            freq_refl, 
            time_refl, 
            phase_refl, 
            start_refl, 
            end_refl, 
            number_of_lines,
            color=ORANGE,
            amp_scaler=amp_refl_scaler,
            k_scaler=k_refl_scaler,
            freq_scaler=freq_refl_scaler,
            phase_offset=phase_refl_offset
        )

        # Standing Wave
        self.standing_beam = self.sum_electric_fields(self.input_beam, self.refl_beam)

        self.standing_beam.forward_beam = self.input_beam
        self.standing_beam.backward_beam = self.refl_beam

        # Mirror
        self.right_mirror = Rectangle(height=3.2, width=0.5, fill_color=BLUE, fill_opacity=1, color=BLUE).shift(5.25*RIGHT)

        ## Tex
        # Names of waves
        self.input_tex = Tex(r"Input beam", color=RED).shift(3*UP + 4*LEFT)
        self.refl_tex = Tex(r"Reflected beam", color=ORANGE).next_to(self.input_tex, DOWN, buff=0.2)
        self.standing_tex = Tex(r"Standing wave", color=YELLOW).next_to(self.refl_tex, DOWN, buff=0.2)

        self.right_self.mirror_tex = Tex(r"Mirror", color=BLUE).next_to(self.right_mirror, DOWN, buff=0.25)

        # Time as Decimal Number
        self.phase_value_tex = Tex(r"$\varphi = $").shift(3*UP + 4*RIGHT)
        self.phase_value = always_redraw(
            lambda: DecimalNumber(num_decimal_places=1)
            .set_value(phase_input.get_value())
            .next_to(self.phase_value_tex, RIGHT, buff=0.1)
        )

        # Updaters
        def time_updater(mob, dt):
            self.timer += dt * self.rate
            return

        def wave_updater(mob):
            temp_mob = self.make_electric_field(
                mob.amp, mob.k, mob.freq, self.timer, mob.phase, mob.start, mob.end, mob.number_of_lines, mob.color, 
                mob.amp_scaler, mob.k_scaler, mob.freq_scaler, mob.phase_offset
            )
            mob.become(temp_mob)
            return 

        def standing_updater(mob):
            fmob = mob.forward_beam
            bmob = mob.backward_beam
            temp_fmob = self.make_electric_field(
                fmob.amp, fmob.k, fmob.freq, self.timer, fmob.phase, fmob.start, fmob.end, fmob.number_of_lines, fmob.color, 
                fmob.amp_scaler, fmob.k_scaler, fmob.freq_scaler, fmob.phase_offset
            )
            temp_bmob = self.make_electric_field(
                bmob.amp, bmob.k, bmob.freq, self.timer, bmob.phase, bmob.start, bmob.end, bmob.number_of_lines, bmob.color, 
                bmob.amp_scaler, bmob.k_scaler, bmob.freq_scaler, bmob.phase_offset
            )
            temp_mob = self.sum_electric_fields(temp_fmob, temp_bmob)
            mob.become(temp_mob)
            return 

        # Set the scene and play it
        self.show_axis()

        self.play(Create(self.input_tex))
        self.play(Create(self.input_beam))
        self.wait(0.5)

        self.play(Create(self.right_self.mirror_tex))
        self.play(Create(self.right_mirror))
        self.wait(0.5)

        self.play(Create(self.refl_tex))
        self.play(Create(self.refl_beam))
        self.wait(0.5)

        self.play(Create(self.standing_tex))
        self.play(Create(self.standing_beam))
        self.wait(0.5)

        self.play(Create(self.phase_value_tex))
        self.play(Create(self.phase_value))
        self.wait(0.5)

        # self.input_beam.add_updater(time_updater)
        self.input_beam.add_updater(wave_updater)
        self.refl_beam.add_updater(wave_updater)
        self.standing_beam.add_updater(standing_updater)

        self.play(phase_input.animate.set_value(-4*PI), run_time=4*PI)

        seconds_to_oscillate = 1
        self.wait(seconds_to_oscillate/self.rate)


  
class InputBeamPlusMirrorVsMirrorMove(Scene):
    def get_phase(self, k=1, x=0, freq=1, time=0, phase=0):
        return k * x - 2 * PI * freq * time + phase
    def get_sine_wave(self, amp=1, k=1, x=0, freq=1, time=0, phase=0):
        return  amp * np.sin(k * x - 2 * PI * freq * time + phase)

    def rotate(self, x1, y1, theta, origin=np.array([0,0,0])):
        """Return x2 and y2 being rotated about the origin by theta.
        """
        x0 = origin[0]
        y0 = origin[1]
        x2 = (x1 - x0) * np.cos(theta) - (y1 - y0) * np.sin(theta)
        y2 = (x1 - x0) * np.sin(theta) + (y1 - y0) * np.cos(theta)
        return x2, y2

    def show_axis(self):
        x_start = np.array([-5,0,0])
        x_end = np.array([5,0,0])

        y_start = np.array([0,-3,0])
        y_end = np.array([0,3,0])

        x_axis = Line(x_start, x_end)
        y_axis = Line(y_start, y_end)

        # self.add(x_axis, y_axis)

        self.origin_point = np.array([0,0,0])

        return

    def make_electric_field(
        self, 
        amp=ValueTracker(1), 
        k=ValueTracker(1), 
        freq=ValueTracker(1), 
        time=0, 
        phase=ValueTracker(0), 
        start=np.array([-5,0,0]), 
        end=np.array([5,0,0]), 
        number_of_lines=50, 
        color=RED,
        amp_scaler=1,
        k_scaler=1,
        freq_scaler=1,
        phase_offset=0,
        x_start=None,
        x_end=None,
    ):
        """Makes an electric field VGroup of Lines.
        Electric field is defined by amp, k, freq, time, and phase ValueTrackers.
        start and end are 3D vectors defining the beginning and end of the wave.
        number_of_lines defines how many Line segments the wave is broken up into.
        x_start and x_end should either be None if we don't want to move the sine wave
        starting points, or a ValueTracker if we do.
        Returns the VGroup.
        """
        # Define the VGroup of Lines which make up the E-field, and store some ValueTrackers
        beam = VGroup()
        beam.amp = amp
        beam.k = k
        beam.freq = freq
        beam.time = time
        beam.phase = phase

        beam.x_start = x_start
        beam.x_end = x_end

        beam.amp_scaler = amp_scaler
        beam.k_scaler = k_scaler
        beam.freq_scaler = freq_scaler
        beam.phase_offset = phase_offset

        amp_float = amp_scaler * amp.get_value()
        k_float = k_scaler * k.get_value()
        freq_float = freq_scaler * freq.get_value()
        phase_float = phase_offset + phase.get_value()

        # Check if ValueTrackers exist
        if x_start:
            start[0] = x_start.get_value()
        if x_end:
            end[0] = x_end.get_value()

        # Get the start and end vector info.
        # This function will first calculate the wave along the x axis,
        # then rotate it appropriately.
        span = end - start
        origin = (start + end) / 2
        total_length = np.sqrt(np.sum(span**2))
        norm = span / total_length
        theta = np.arctan2(norm[1], norm[0])

        x_diff = total_length / number_of_lines

        phase_output = self.get_phase(k=k_float, x=total_length, freq=freq_float, time=time, phase=0)

        # Store some additional info
        beam.phase_output = phase_output
        beam.start = start
        beam.end = end
        beam.origin = origin
        beam.theta = theta
        beam.number_of_lines = number_of_lines
        beam.color = color

        for ii in range(number_of_lines):
            # Calculate wave in 1D
            temp_x_start = ii * x_diff
            temp_x_end = (ii + 1) * x_diff

            temp_y_start = self.get_sine_wave(amp_float, k_float, temp_x_start, freq_float, time, phase_float)
            temp_y_end = self.get_sine_wave(amp_float, k_float, temp_x_end, freq_float, time, phase_float)

            # Translate to origin = (0, 0)
            temp_x_start -= total_length/2
            temp_x_end -= total_length/2
            # temp_y_start += origin[1]
            # temp_y_end += origin[1]

            # Rotate about origin
            temp_x_start, temp_y_start = self.rotate(temp_x_start, temp_y_start, theta)
            temp_x_end, temp_y_end = self.rotate(temp_x_end, temp_y_end, theta)

            # Translate to sine wave position beam.origin
            temp_x_start += origin[0]
            temp_x_end += origin[0]
            temp_y_start += origin[1]
            temp_y_end += origin[1]

            # Define the Line
            temp_start = np.array([temp_x_start, temp_y_start, 0])
            temp_end = np.array([temp_x_end, temp_y_end, 0])

            temp_line = Line(temp_start, temp_end, color=color)
            beam.add(temp_line)

        return beam

    def sum_electric_fields(self, field1, field2, color=YELLOW, reverse=True):
        """Add together two VGroups of electric fields
        """
        if field1.number_of_lines != field2.number_of_lines:
            print()
            print("Cannot sum two fields of different lengths")
            return
        
        beam = VGroup()
        for ii in range(field1.number_of_lines):
            if reverse:
                jj = (field1.number_of_lines - 1) - ii
            else:
                jj = ii
        
            line1 = field1[ii]
            line2 = field2[jj]

            if reverse:
                new_line_start = line1.start
                new_line_end = line1.end
                new_line_start[1] = line1.start[1] + line2.end[1]
                new_line_end[1] = line1.end[1] + line2.start[1]
            else:
                new_line_start = line1.start 
                new_line_end = line1.end 
                new_line_start[1] = line1.start[1] + line2.start[1]
                new_line_end[1] = line1.end[1] + line2.end[1]

            new_line = Line(new_line_start, new_line_end, color=color)
            beam.add(new_line)

        return beam


    def construct(self):
        # Set up a timer variable
        self.timer = 0.0
        self.rate = 0.25
        number_of_lines = 100

        # Input Beam
        amp_input = ValueTracker(0.75)
        k_input = ValueTracker(2*PI/5)
        freq_input = ValueTracker(1)
        time_input = 0.0
        phase_input = ValueTracker(PI/2)
        x_start_input = ValueTracker(-5)
        x_end_input = ValueTracker(5)

        start_input = np.array([-5.0, 0.0, 0.0])
        end_input = np.array([5.0, 0.0, 0.0])

        self.input_beam = self.make_electric_field(
            amp_input, 
            k_input, 
            freq_input, 
            time_input, 
            phase_input, 
            start_input, 
            end_input, 
            number_of_lines,
            x_start=x_start_input,
            x_end=x_end_input
        )

        # Refl Beam
        amp_refl = amp_input
        k_refl = k_input
        freq_refl = freq_input
        time_refl = 0.0
        phase_refl = phase_input
        x_start_refl = x_end_input
        x_end_refl = x_start_input

        start_refl = np.array([5.0, 0.0, 0.0])
        end_refl = np.array([-5.0, 0.0, 0.0])

        amp_refl_scaler = 1
        k_refl_scaler = 1
        freq_refl_scaler = 1
        phase_refl_offset = self.input_beam.phase_output

        self.refl_beam = self.make_electric_field(
            amp_refl, 
            k_refl, 
            freq_refl, 
            time_refl, 
            phase_refl, 
            start_refl, 
            end_refl, 
            number_of_lines,
            color=ORANGE,
            amp_scaler=amp_refl_scaler,
            k_scaler=k_refl_scaler,
            freq_scaler=freq_refl_scaler,
            phase_offset=phase_refl_offset,
            x_start=x_start_refl,
            x_end=x_end_refl
        )

        # Standing Wave
        self.standing_beam = self.sum_electric_fields(self.input_beam, self.refl_beam)

        self.standing_beam.forward_beam = self.input_beam
        self.standing_beam.backward_beam = self.refl_beam

        # Mirror
        self.mirror_width = 0.5
        self.right_mirror = always_redraw(
            lambda: Rectangle(height=3.2, width=self.mirror_width, fill_color=BLUE, fill_opacity=1, color=BLUE)
            .shift((x_end_input.get_value() + self.mirror_width/2) * RIGHT)
        )

        ## Tex
        # Names of waves
        self.input_tex = Tex(r"Input beam", color=RED).shift(3*UP + 4*LEFT)
        self.refl_tex = Tex(r"Reflected beam", color=ORANGE).next_to(self.input_tex, DOWN, buff=0.2)
        self.standing_tex = Tex(r"Standing wave", color=YELLOW).next_to(self.refl_tex, DOWN, buff=0.2)

        self.right_mirror_tex = Tex(r"Mirror", color=BLUE).next_to(self.right_mirror, DOWN, buff=0.25)

        # Time as Decimal Number
        self.length_value_tex = Tex(r"$L = $").shift(3*UP + 4*RIGHT)
        self.length_value = always_redraw(
            lambda: DecimalNumber(num_decimal_places=1)
            .set_value(x_end_input.get_value() - x_start_input.get_value())
            .next_to(self.length_value_tex, RIGHT, buff=0.1)
        )

        # Updaters
        def time_updater(mob, dt):
            self.timer += dt * self.rate
            return

        def wave_updater(mob):
            temp_mob = self.make_electric_field(
                mob.amp, mob.k, mob.freq, self.timer, mob.phase, mob.start, mob.end, mob.number_of_lines, mob.color, 
                mob.amp_scaler, mob.k_scaler, mob.freq_scaler, mob.phase_offset, mob.x_start, mob.x_end
            )
            mob.become(temp_mob)
            return 

        def standing_updater(mob):
            fmob = mob.forward_beam
            bmob = mob.backward_beam
            temp_fmob = self.make_electric_field(
                fmob.amp, fmob.k, fmob.freq, self.timer, fmob.phase, fmob.start, fmob.end, fmob.number_of_lines, fmob.color, 
                fmob.amp_scaler, fmob.k_scaler, fmob.freq_scaler, fmob.phase_offset, fmob.x_start, fmob.x_end
            )
            temp_bmob = self.make_electric_field(
                bmob.amp, bmob.k, bmob.freq, self.timer, bmob.phase, bmob.start, bmob.end, bmob.number_of_lines, bmob.color, 
                bmob.amp_scaler, bmob.k_scaler, bmob.freq_scaler, bmob.phase_offset, bmob.x_start, bmob.x_end
            )
            temp_mob = self.sum_electric_fields(temp_fmob, temp_bmob)
            mob.become(temp_mob)
            return 

        # Set the scene and play it
        self.show_axis()

        self.play(Create(self.input_tex))
        self.play(Create(self.input_beam))
        self.wait(0.5)

        self.play(Create(self.right_mirror_tex))
        self.play(Create(self.right_mirror))
        self.wait(0.5)

        self.play(Create(self.refl_tex))
        self.play(Create(self.refl_beam))
        self.wait(0.5)

        self.play(Create(self.standing_tex))
        self.play(Create(self.standing_beam))
        self.wait(0.5)

        self.play(Create(self.length_value_tex))
        self.play(Create(self.length_value))
        self.wait(0.5)

        # self.input_beam.add_updater(time_updater)
        self.input_beam.add_updater(wave_updater)
        self.refl_beam.add_updater(wave_updater)
        self.standing_beam.add_updater(standing_updater)

        self.play(x_end_input.animate.set_value(2.5), run_time=2*PI)
        self.wait(2)
        
        self.play(x_end_input.animate.set_value(6.5), run_time=2*PI)
        self.wait(2)

        self.play(x_end_input.animate.set_value(5.0), run_time=2*PI)
        self.wait(2)
