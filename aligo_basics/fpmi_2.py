"""fpmi_2.py

Derivation of a Fabry Perot Michelson resp
"""

from manim import *

class FPMI2(Scene):
    def construct(self):

        ## Tex
        # Core technology tex
        self.core_technology_tex = Tex(
            r"{{ Fabry P\'erot }}{{ Michelson }}\\interferometer"
        ).move_to(3*UP + 4*LEFT)
        self.core_technology_tex[0].set_color(PURPLE)
        self.core_technology_tex[1].set_color(RED)

        # Want to find the change in the power in the photodetector when a GW hits
        self.purpose_tex_1 = Tex(
            r"Find the change in the power\\on the photodetector $P_\mathrm{trans}$"
        ).scale(0.7).next_to(self.core_technology_tex, DOWN).shift(0.5*DOWN)
        self.purpose_tex_2 = Tex(
            r"when a gravitational wave hits: "
        ).scale(0.7).next_to(self.purpose_tex_1, DOWN)

        self.purpose_tex_3 = MathTex(
            r"{ {{ P_\mathrm{trans} }} \over {{ \Delta L }} }"
        ).scale(0.7).next_to(self.purpose_tex_2, RIGHT)
        self.purpose_tex_3[1].set_color(GREEN)
        self.purpose_tex_3[3].set_color(ORANGE)

        # Michelson trans
        self.mich_trans_tex = Tex(
            "Michelson transmission",
            color=GREEN
        ).scale(0.7).next_to(self.purpose_tex_2, DOWN).shift(0.3*DOWN)

        self.trans_eq = MathTex(
            r"{ {{ E_\mathrm{trans} }} \over {{ E_\mathrm{in} }} }{{ = }}-i e^{ i 2 k {{ \ell_c }} }{{ }}\sin{( 2 k{{ \ell_d }} )}"
        ).scale(0.7).next_to(self.mich_trans_tex, DOWN)
        self.trans_eq[1].set_color(GREEN)
        self.trans_eq[3].set_color(RED)
        self.trans_eq[7].set_color(TEAL)
        self.trans_eq[11].set_color(MAROON)

        # Fabry Perot tex
        self.fabry_perot_derivative_tex = Tex(
            r"Fabry P\'erot length response",
            color=ORANGE
        ).scale(0.7).next_to(self.trans_eq, DOWN).shift(0.5*DOWN)

        self.electric_field_refl_deriv_3 = MathTex(
            r"{ d {{ r_\mathrm{cav} }} \over dL }{{ = }}{ i 2 k {{ \tau_i }}^2 {{ r_e }} \over ( 1 - {{ r_i }}{{ r_e }} )^2 }"
        ).scale(0.7).next_to(self.fabry_perot_derivative_tex, DOWN)
        self.electric_field_refl_deriv_3[1].set_color(ORANGE)
        self.electric_field_refl_deriv_3[5].set_color(BLUE)
        self.electric_field_refl_deriv_3[7].set_color(BLUE)
        self.electric_field_refl_deriv_3[9].set_color(BLUE)
        self.electric_field_refl_deriv_3[10].set_color(BLUE)

        deriv_location = self.electric_field_refl_deriv_3.get_center()

        # We've made our simple Michelson more complex by replacing 
        # the perfectly reflective end mirrors with long Fabry-Perot cavities
        # Replace our michelson differential (2 k \ell_d) with 
        # the differential phase we get from the arm lengths \phi_d

        trans_eq_location = 2.5*UP + 3*RIGHT

        self.diff_arm_phase_mathtex = MathTex(
            r"{{ \phi_d }} = \phi_\mathrm{Xarm} - \phi_\mathrm{Yarm}"
        ).scale(0.7).move_to(trans_eq_location).shift(0.8*DOWN)
        self.diff_arm_phase_mathtex[0].set_color(YELLOW)

        self.replace_phase_mathtex = MathTex(
            r"2 k {{ \ell_d }} \rightarrow {{ \phi_d }}"
        ).scale(0.7).next_to(self.diff_arm_phase_mathtex, DOWN)
        self.replace_phase_mathtex[1].set_color(MAROON)
        self.replace_phase_mathtex[3].set_color(YELLOW)

        self.comm_arm_phase_mathtex = MathTex(
            r"e^{ i 2 k {{ \ell_c }} } \rightarrow 1",
        ).scale(0.7).next_to(self.replace_phase_mathtex, DOWN)
        self.comm_arm_phase_mathtex[1].set_color(TEAL)

        # More FPMI trans equations

        self.fpmi_trans_tex = Tex(
            r"Fabry P\'erot Michelson transmission",
            color=GREEN
        ).scale(0.7).move_to(trans_eq_location).shift(0.8*UP)

        self.trans_eq_2 = MathTex(
            r"{ {{ E_\mathrm{trans} }} \over {{ E_\mathrm{in} }} }{{ = }}-i {{ }}\sin{({{ \phi_d }} )}"
        ).scale(0.7).move_to(trans_eq_location)
        self.trans_eq_2[1].set_color(GREEN)
        self.trans_eq_2[3].set_color(RED)
        self.trans_eq_2[9].set_color(YELLOW)

        self.trans_eq_3 = MathTex(
            r"{ {{ E_\mathrm{trans} }} \over {{ E_\mathrm{in} }} }{{ = }}-i {{ \phi_d }}"
        ).scale(0.7).move_to(trans_eq_location)
        self.trans_eq_3[1].set_color(GREEN)
        self.trans_eq_3[3].set_color(RED)
        self.trans_eq_3[7].set_color(YELLOW)

        self.trans_eq_4 = MathTex(
            r"{ {{ E_\mathrm{trans} }} \over {{ E_\mathrm{in} }} }{{ = }} -i 2 { d {{ r_\mathrm{cav} }} \over dL }{{ \Delta L }}"
        ).scale(0.7).move_to(trans_eq_location)
        self.trans_eq_4[1].set_color(GREEN)
        self.trans_eq_4[3].set_color(RED)
        self.trans_eq_4[7].set_color(ORANGE)
        self.trans_eq_4[9].set_color(ORANGE)

        self.trans_eq_5 = MathTex(
            r"{{ E_\mathrm{trans} }} = -i 2 { d {{ r_\mathrm{cav} }} \over dL }{{ \Delta L }}{{ E_\mathrm{in} }}"
        ).scale(0.7).move_to(trans_eq_location)
        self.trans_eq_5[0].set_color(GREEN)
        self.trans_eq_5[2].set_color(ORANGE)
        self.trans_eq_5[4].set_color(ORANGE)
        self.trans_eq_5[5].set_color(RED)


        # What is the phase change of the arm?
        self.what_arm_phase = Tex(
            r"But what is the phase change\\of the arm $\phi_\mathrm{Xarm}, \phi_\mathrm{Yarm}$?"
        ).scale(0.7).next_to(self.diff_arm_phase_mathtex, DOWN)
        
        # First of all, we need the right units
        self.r_cav_approximations_1 = Tex(
            r"Fabry P\'erot on resonance:"
        ).scale(0.7).next_to(self.diff_arm_phase_mathtex, DOWN).shift(0.3*DOWN)
        self.r_cav_approximations_2 = MathTex(
            r"\Rightarrow {{ r_\mathrm{cav} }} \approx 1"
        ).scale(0.7).next_to(self.r_cav_approximations_1, DOWN)
        self.r_cav_approximations_2[1].set_color(ORANGE)

        self.r_cav_approximations_3 = MathTex(
            r"{{ r_\mathrm{cav} }} \approx 1 + { d {{ r_\mathrm{cav} }} \over dL }{{ \Delta L }}"
        ).scale(0.7).next_to(self.r_cav_approximations_2, DOWN)
        self.r_cav_approximations_3[0].set_color(ORANGE)
        self.r_cav_approximations_3[2].set_color(ORANGE)
        self.r_cav_approximations_3[4].set_color(ORANGE)

        self.r_cav_approximations_4 = MathTex(
            r"\phi_\mathrm{Xarm} = { d {{ r_\mathrm{cav} }} \over dL }{{ \Delta L }}"
        ).scale(0.7).next_to(self.r_cav_approximations_3, DOWN)
        self.r_cav_approximations_4[1].set_color(ORANGE)
        self.r_cav_approximations_4[3].set_color(ORANGE)

        self.r_cav_approximations_5 = MathTex(
            r"\phi_\mathrm{Yarm} = { d {{ r_\mathrm{cav} }} \over dL }({{ -\Delta L }})"
        ).scale(0.7).next_to(self.r_cav_approximations_4, DOWN)
        self.r_cav_approximations_5[1].set_color(ORANGE)
        self.r_cav_approximations_5[3].set_color(ORANGE)

        self.r_cav_approximations_6 = MathTex(
            r"{{ \phi_d }} = 2 { d {{ r_\mathrm{cav} }} \over dL }{{ \Delta L }}"
        ).scale(0.7).next_to(self.r_cav_approximations_5, DOWN)
        self.r_cav_approximations_6[0].set_color(YELLOW)
        self.r_cav_approximations_6[2].set_color(ORANGE)
        self.r_cav_approximations_6[4].set_color(ORANGE)

        # DARM offset text 
        self.darm_offset_tex = Tex(
            r"Differential arm offset "
        ).scale(0.7).next_to(self.r_cav_approximations_3, LEFT).shift(LEFT)
        self.darm_offset_mathtex = MathTex(
            r"\Delta L \ll 1",
            color=ORANGE
        ).scale(0.7).next_to(self.darm_offset_tex, DOWN)


        # Total Transmitted Power
        self.trans_power_eq_1 = MathTex(
            r"{{ P_\mathrm{trans} }} = |{{ E_\mathrm{trans} }}|^2"
        ).scale(0.7).next_to(self.trans_eq_5, DOWN)
        self.trans_power_eq_1[0].set_color(GREEN)
        self.trans_power_eq_1[2].set_color(GREEN)

        self.trans_power_eq_2 = MathTex(
            r"{{ P_\mathrm{trans} }} = |-i 2 { d {{ r_\mathrm{cav} }} \over dL }{{ \Delta L }}{{ E_\mathrm{in} }}|^2"
        ).scale(0.7).next_to(self.trans_power_eq_1, DOWN)
        self.trans_power_eq_2[0].set_color(GREEN)
        self.trans_power_eq_2[2].set_color(ORANGE)
        self.trans_power_eq_2[4].set_color(ORANGE)
        self.trans_power_eq_2[5].set_color(RED)

        self.trans_power_eq_3 = MathTex(
            r"{{ P_\mathrm{trans} }} = 4 ({ d {{ r_\mathrm{cav} }} \over dL })^2{{ \Delta L^2 }}{{ P_\mathrm{in} }}"
        ).scale(0.7).next_to(self.trans_power_eq_2, DOWN)
        self.trans_power_eq_3[0].set_color(GREEN)
        self.trans_power_eq_3[2].set_color(ORANGE)
        self.trans_power_eq_3[4].set_color(ORANGE)
        self.trans_power_eq_3[5].set_color(RED)


        # Derivative of transmitted power
        self.trans_deriv_1 = MathTex(
            r"{ d {{ P_\mathrm{trans} }} \over d {{ \Delta L }} }{{ = }} { d \over d {{ \Delta L }} }{{ ( }} 4 ({ d {{ r_\mathrm{cav} }} \over dL })^2{{ \Delta L^2 }}{{ P_\mathrm{in} }} )"
        ).scale(0.7).next_to(self.trans_power_eq_3, DOWN)
        self.trans_deriv_1[1].set_color(GREEN)
        self.trans_deriv_1[3].set_color(ORANGE)
        self.trans_deriv_1[7].set_color(ORANGE)
        self.trans_deriv_1[11].set_color(ORANGE)
        self.trans_deriv_1[13].set_color(ORANGE)
        self.trans_deriv_1[14].set_color(RED)

        self.trans_deriv_2 = MathTex(
            r"{ d {{ P_\mathrm{trans} }} \over d {{ \Delta L }} }{{ = }} 8 ({ d {{ r_\mathrm{cav} }} \over dL })^2{{ \Delta L }}{{ P_\mathrm{in} }}"
        ).scale(0.7).next_to(self.trans_power_eq_3, DOWN)
        self.trans_deriv_2[1].set_color(GREEN)
        self.trans_deriv_2[3].set_color(ORANGE)
        self.trans_deriv_2[7].set_color(ORANGE)
        self.trans_deriv_2[9].set_color(ORANGE)
        self.trans_deriv_2[10].set_color(RED)

        ###   Scene   ###

        self.add(self.core_technology_tex)
        self.add(self.purpose_tex_1)
        self.add(self.purpose_tex_2)
        self.add(self.purpose_tex_3)
        self.add(self.mich_trans_tex)
        self.add(self.trans_eq)
        self.add(self.fabry_perot_derivative_tex)
        self.add(self.electric_field_refl_deriv_3)
        
        self.play(
            self.trans_eq.animate.move_to(trans_eq_location),
            run_time=2
        )
        self.wait(1)

        self.play(
            Write(self.diff_arm_phase_mathtex)
        )
        self.play(
            Write(self.replace_phase_mathtex)
        )
        self.wait(2)
        self.play(
            Write(self.comm_arm_phase_mathtex)
        )
        self.wait(2)

        # Get FPMI trans expression of sin(phi)
        self.play(
            TransformMatchingTex(self.trans_eq, self.trans_eq_2),
            run_time=2
        )
        self.play(
            TransformMatchingTex(self.mich_trans_tex, self.fpmi_trans_tex),
            run_time=2
        )
        self.wait(1)

        self.play(
            TransformMatchingTex(self.trans_eq_2, self.trans_eq_3),
            run_time=2
        )
        self.wait(2)

        # Clean up
        self.play(
            FadeOut(self.replace_phase_mathtex),
        )
        self.play(
            FadeOut(self.comm_arm_phase_mathtex)
        )
        self.wait(1)

        # What is the phase change of the arm?
        self.play(
            Write(self.what_arm_phase)
        )
        self.wait(3)
        
        self.play(
            Unwrite(self.what_arm_phase)
        )
        self.wait(1)

        # Well, we have a suspiciously nice-looking equation in the bottom there...
        self.play(
            self.electric_field_refl_deriv_3.animate.move_to(1.5*DOWN),
            run_time=4,
            rate_func=rate_functions.linear
        )
        self.wait(0.5)
        self.play(
            self.electric_field_refl_deriv_3.animate.move_to(deriv_location),
            run_time=0.5,
            rate_func=rate_functions.running_start
        )
        self.wait(1)

        # Let's justify this first
        self.play(
            Write(self.r_cav_approximations_1)
        )
        self.play(
            Write(self.r_cav_approximations_2)
        )
        self.wait(2)

        self.r_cav_approximations_2_copy = self.r_cav_approximations_2.copy()
        self.play(
            TransformMatchingTex(self.r_cav_approximations_2_copy, self.r_cav_approximations_3),
            run_time=2
        )
        self.wait(1)

        # Write the DARM offset stuff
        self.play(
            Write(self.darm_offset_tex)
        )
        self.play(
            Write(self.darm_offset_mathtex)
        )
        self.wait(3)

        # Continue with the derivation
        self.r_cav_approximations_3_copy = self.r_cav_approximations_3.copy()
        self.play(
            TransformMatchingTex(self.r_cav_approximations_3_copy, self.r_cav_approximations_4),
            run_time=2
        )
        self.wait(1)

        self.r_cav_approximations_3_copy = self.r_cav_approximations_3.copy()
        self.play(
            TransformMatchingTex(self.r_cav_approximations_3_copy, self.r_cav_approximations_5),
            run_time=2
        )
        self.wait(1)

        self.r_cav_approximations_4_copy = self.r_cav_approximations_4.copy()
        self.r_cav_approximations_5_copy = self.r_cav_approximations_5.copy()
        self.play(
            TransformMatchingTex(self.r_cav_approximations_4_copy, self.r_cav_approximations_6),
            TransformMatchingTex(self.r_cav_approximations_5_copy, self.r_cav_approximations_6),
            run_time=2
        )
        self.wait(1)
        
        # Unwrite the DARM offset stuff and everything else
        self.play(
            Unwrite(self.darm_offset_tex),
            Unwrite(self.darm_offset_mathtex),
            Unwrite(self.diff_arm_phase_mathtex),
            Unwrite(self.r_cav_approximations_1),
            Unwrite(self.r_cav_approximations_2),
            Unwrite(self.r_cav_approximations_3),
            Unwrite(self.r_cav_approximations_4),
            Unwrite(self.r_cav_approximations_5),
        )
        self.wait(1)

        # Move new result up
        self.play(
            self.r_cav_approximations_6.animate.next_to(self.trans_eq_3, DOWN)
        )
        self.wait(1)

        # Sub it in
        self.play(
            TransformMatchingTex(self.trans_eq_3, self.trans_eq_4),
            TransformMatchingTex(self.r_cav_approximations_6, self.trans_eq_4),
            run_time=2
        )
        self.wait(1)

        # Separate out fraction
        self.play(
            TransformMatchingTex(self.trans_eq_4, self.trans_eq_5),
            run_time=2
        )
        self.wait(1)

        # Write the power transmission equation
        self.play(
            Write(self.trans_power_eq_1)
        )
        self.wait(1)

        self.trans_power_eq_1_copy = self.trans_power_eq_1.copy()
        self.play(
            TransformMatchingTex(self.trans_power_eq_1_copy, self.trans_power_eq_2),
            run_time=2
        )
        self.wait(1)

        self.trans_power_eq_2_copy = self.trans_power_eq_2.copy()
        self.play(
            TransformMatchingTex(self.trans_power_eq_2_copy, self.trans_power_eq_3),
            run_time=2
        )
        self.wait(2)

        # Take the derivative
        self.trans_power_eq_3_copy = self.trans_power_eq_3.copy()
        self.play(
            TransformMatchingTex(self.trans_power_eq_3_copy, self.trans_deriv_1),
            run_time=2
        )
        self.wait(1)

        self.play(
            TransformMatchingTex(self.trans_deriv_1, self.trans_deriv_2),
            run_time=2
        )
        self.wait(1)

