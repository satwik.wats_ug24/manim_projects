"""michelson_derivation.py
Derives the power inside a Michelson interferometer,
does the change of basis calculation to demonstrate how differential motion is the important quantity
"""

from manim import *

class MichelsonDerivation2(Scene):

    def construct(self):
        
        self.color_map = {
            "E_in" : RED,
            "E_x1" : RED_A,
            "E_y1" : RED_E,
            "E_x2" : ORANGE,
            "E_y2" : PINK,
            "E_refl" : YELLOW,
            "E_refl_x" : YELLOW_A,
            "E_refl_y" : YELLOW_E,
            "E_trans" : GREEN,
            "E_trans_x" : PURPLE,
            "E_trans_y" : GREY,
            "mirror" : BLUE,
            "lc" : TEAL,
            "ld" : MAROON,
        }

        ## Tex
        # Equations
        self.x_arm_eq = MathTex(
            r"{{ E_x }} = {{ \tau }} e^{i 2 k \ell_x} {{ E_\mathrm{in} }}"
        ).scale(0.8).move_to(3.5*UP + 5*RIGHT)
        self.x_arm_eq[0].set_color(self.color_map["E_x2"])
        self.x_arm_eq[2].set_color(self.color_map["mirror"])
        self.x_arm_eq[4].set_color(self.color_map["E_in"])

        self.y_arm_eq = MathTex(
            r"{{ E_y }} = {{ r }} e^{i 2 k \ell_y} {{ E_\mathrm{in} }}"
        ).scale(0.8).next_to(self.x_arm_eq, DOWN)
        self.y_arm_eq[0].set_color(self.color_map["E_y2"])
        self.y_arm_eq[2].set_color(self.color_map["mirror"])
        self.y_arm_eq[4].set_color(self.color_map["E_in"])

        self.refl_eq_1 = MathTex(
            r"{{ E_\mathrm{refl} }} = {{ \tau }}{{ E_x }}{{ + r }}{{ E_y }}"
        ).scale(0.8).next_to(self.y_arm_eq, DOWN)
        self.refl_eq_1[0].set_color(self.color_map["E_refl"])
        self.refl_eq_1[2].set_color(self.color_map["mirror"])
        self.refl_eq_1[3].set_color(self.color_map["E_x2"])
        self.refl_eq_1[4].set_color(self.color_map["mirror"])
        self.refl_eq_1[5].set_color(self.color_map["E_y2"])

        self.trans_eq_1 = MathTex(
            r"{{ E_\mathrm{trans} }} = {{ - r }}{{ E_x }} + {{ \tau }}{{ E_y }}"
        ).scale(0.8).next_to(self.refl_eq_1, DOWN)
        self.trans_eq_1[0].set_color(self.color_map["E_trans"])
        self.trans_eq_1[2].set_color(self.color_map["mirror"])
        self.trans_eq_1[3].set_color(self.color_map["E_x2"])
        self.trans_eq_1[5].set_color(self.color_map["mirror"])
        self.trans_eq_1[6].set_color(self.color_map["E_y2"])


        # locations to move equations to
        x_eq_location = 3.5*UP + 2*RIGHT
        y_eq_location = 3.5*UP + 5*RIGHT
        refl_eq_location = 2.5*UP + 3.5*LEFT
        trans_eq_location = 2.5*UP + 3.5*RIGHT

        # Further refl eqs
        self.refl_eq_2 = MathTex(
            r"{{ E_\mathrm{refl} }} = {{ \tau }}^2 {{ e^{i 2 k \ell_x} }} {{ E_\mathrm{in} }} + {{ r }}^2 {{ e^{i 2 k \ell_y} }} {{ E_\mathrm{in} }}"
        ).scale(0.8).next_to(refl_eq_location, DOWN).shift(0.5*DOWN)
        self.refl_eq_2[0].set_color(self.color_map["E_refl"])
        self.refl_eq_2[2].set_color(self.color_map["mirror"])
        self.refl_eq_2[6].set_color(self.color_map["E_in"])
        self.refl_eq_2[8].set_color(self.color_map["mirror"])
        self.refl_eq_2[12].set_color(self.color_map["E_in"])

        self.refl_eq_3 = MathTex(
            r"{{ E_\mathrm{refl} }} = { {{ E_\mathrm{in} }} \over 2} ( {{ e^{i 2 k \ell_x} }} + {{ e^{i 2 k \ell_y} }} ) "
        ).scale(0.8).next_to(self.refl_eq_2, DOWN)
        self.refl_eq_3[0].set_color(self.color_map["E_refl"])
        self.refl_eq_3[2].set_color(self.color_map["E_in"])

        self.refl_eq_4 = MathTex(
            r"{{ E_\mathrm{refl} }} = { {{ E_\mathrm{in} }} \over 2}{{ ( }}e^{ i 2 k ({{ \ell_c }} + {{ \ell_d }}) }{{ + }}e^{ i 2 k ({{ \ell_c }} - {{ \ell_d }}) } ) "
        ).scale(0.8).next_to(self.refl_eq_3, DOWN)
        self.refl_eq_4[0].set_color(self.color_map["E_refl"])
        self.refl_eq_4[2].set_color(self.color_map["E_in"])
        self.refl_eq_4[6].set_color(self.color_map["lc"])
        self.refl_eq_4[8].set_color(self.color_map["ld"])
        self.refl_eq_4[12].set_color(self.color_map["lc"])
        self.refl_eq_4[14].set_color(self.color_map["ld"])

        self.refl_eq_5 = MathTex(
            r"{{ E_\mathrm{refl} }} = { {{ E_\mathrm{in} }} \over 2}{{ }}e^{i 2 k {{ \ell_c }}}{{ ( }}e^{ i 2 k {{ \ell_d }} }{{ + }}e^{-i 2 k {{ \ell_d }} } ) "
        ).scale(0.8).next_to(self.refl_eq_4, DOWN)
        self.refl_eq_5[0].set_color(self.color_map["E_refl"])
        self.refl_eq_5[2].set_color(self.color_map["E_in"])
        self.refl_eq_5[6].set_color(self.color_map["lc"])
        self.refl_eq_5[10].set_color(self.color_map["ld"])
        self.refl_eq_5[14].set_color(self.color_map["ld"])

        self.refl_eq_6 = MathTex(
            r"{{ E_\mathrm{refl} }} = {{ E_\mathrm{in} }}e^{ i 2 k {{ \ell_c }} }{{ }}\cos{( 2 k {{ \ell_d }} )}"
        ).scale(0.8).next_to(self.refl_eq_5, DOWN)
        self.refl_eq_6[0].set_color(self.color_map["E_refl"])
        self.refl_eq_6[2].set_color(self.color_map["E_in"])
        self.refl_eq_6[4].set_color(self.color_map["lc"])
        self.refl_eq_6[8].set_color(self.color_map["ld"])

        self.refl_eq_7 = MathTex(
            r"{ {{ E_\mathrm{refl} }} \over {{ E_\mathrm{in} }} }{{ = }}e^{ i 2 k {{ \ell_c }} }{{ }}\cos{( 2 k {{ \ell_d }} )}"
        ).scale(0.8).next_to(self.refl_eq_5, DOWN)
        self.refl_eq_7[1].set_color(self.color_map["E_refl"])
        self.refl_eq_7[3].set_color(self.color_map["E_in"])
        self.refl_eq_7[7].set_color(self.color_map["lc"])
        self.refl_eq_7[11].set_color(self.color_map["ld"])

        self.michelson_refl_tex = Tex(
            r"Michelson reflectivity $r_\mathrm{mich}$",
            color=self.color_map["E_refl"]
        ).next_to(self.refl_eq_7, DOWN)


        # Further trans eqs
        self.trans_eq_2 = MathTex(
            r"{{ E_\mathrm{trans} }} = {{ - r }}{{ \tau }}{{ e^{i 2 k \ell_x} }} {{ E_\mathrm{in} }} + {{ r }}{{ \tau }}{{ e^{i 2 k \ell_y} }} {{ E_\mathrm{in} }}"
        ).scale(0.8).next_to(trans_eq_location, DOWN).shift(0.5*DOWN)
        self.trans_eq_2[0].set_color(self.color_map["E_trans"])
        self.trans_eq_2[2].set_color(self.color_map["mirror"])
        self.trans_eq_2[3].set_color(self.color_map["mirror"])
        self.trans_eq_2[6].set_color(self.color_map["E_in"])
        self.trans_eq_2[8].set_color(self.color_map["mirror"])
        self.trans_eq_2[9].set_color(self.color_map["mirror"])
        self.trans_eq_2[12].set_color(self.color_map["E_in"])

        self.trans_eq_3 = MathTex(
            r"{{ E_\mathrm{trans} }} = { {{ E_\mathrm{in} }} \over 2} ( - {{ e^{i 2 k \ell_x} }} + {{ e^{i 2 k \ell_y} }} ) "
        ).scale(0.8).next_to(self.trans_eq_2, DOWN)
        self.trans_eq_3[0].set_color(self.color_map["E_trans"])
        self.trans_eq_3[2].set_color(self.color_map["E_in"])

        self.trans_eq_4 = MathTex(
            r"{{ E_\mathrm{trans} }} = { {{ E_\mathrm{in} }} \over 2}{{ ( }}-e^{ i 2 k ({{ \ell_c }} + {{ \ell_d }}) }{{ + }}e^{ i 2 k ({{ \ell_c }} - {{ \ell_d }}) } ) "
        ).scale(0.8).next_to(self.trans_eq_3, DOWN)
        self.trans_eq_4[0].set_color(self.color_map["E_trans"])
        self.trans_eq_4[2].set_color(self.color_map["E_in"])
        self.trans_eq_4[6].set_color(self.color_map["lc"])
        self.trans_eq_4[8].set_color(self.color_map["ld"])
        self.trans_eq_4[12].set_color(self.color_map["lc"])
        self.trans_eq_4[14].set_color(self.color_map["ld"])

        self.trans_eq_5 = MathTex(
            r"{{ E_\mathrm{trans} }} = { {{ E_\mathrm{in} }} \over 2}{{ }}e^{i 2 k {{ \ell_c }}}{{ ( }}-e^{ i 2 k {{ \ell_d }} }{{ + }}e^{-i 2 k {{ \ell_d }} } ) "
        ).scale(0.8).next_to(self.trans_eq_4, DOWN)
        self.trans_eq_5[0].set_color(self.color_map["E_trans"])
        self.trans_eq_5[2].set_color(self.color_map["E_in"])
        self.trans_eq_5[6].set_color(self.color_map["lc"])
        self.trans_eq_5[10].set_color(self.color_map["ld"])
        self.trans_eq_5[14].set_color(self.color_map["ld"])

        self.trans_eq_6 = MathTex(
            r"{{ E_\mathrm{trans} }} = -i{{ E_\mathrm{in} }}e^{ i 2 k {{ \ell_c }} }{{ }}\sin{( 2 k {{ \ell_d }} )}"
        ).scale(0.8).next_to(self.trans_eq_5, DOWN)
        self.trans_eq_6[0].set_color(self.color_map["E_trans"])
        self.trans_eq_6[2].set_color(self.color_map["E_in"])
        self.trans_eq_6[4].set_color(self.color_map["lc"])
        self.trans_eq_6[8].set_color(self.color_map["ld"])

        self.trans_eq_7 = MathTex(
            r"{ {{ E_\mathrm{trans} }} \over {{ E_\mathrm{in} }} }{{ = }}-i e^{ i 2 k {{ \ell_c }} }{{ }}\sin{( 2 k {{ \ell_d }} )}"
        ).scale(0.8).next_to(self.trans_eq_5, DOWN)
        self.trans_eq_7[1].set_color(self.color_map["E_trans"])
        self.trans_eq_7[3].set_color(self.color_map["E_in"])
        self.trans_eq_7[7].set_color(self.color_map["lc"])
        self.trans_eq_7[11].set_color(self.color_map["ld"])

        self.michelson_trans_tex = Tex(
            r"Michelson transmissivity $\tau_\mathrm{mich}$",
            color=self.color_map["E_trans"]
        ).next_to(self.trans_eq_7, DOWN)



        # BS refl
        self.bs_refl_tex = MathTex(
            r"{{ r }}^2 = {{ \tau }}^2 = {1 \over 2}"
        ).scale(0.8).next_to(self.refl_eq_2, RIGHT).shift(RIGHT)
        self.bs_refl_tex[0].set_color(self.color_map["mirror"])
        self.bs_refl_tex[2].set_color(self.color_map["mirror"])

        # Change of basis
        self.change_of_basis_tex = Tex(
            "Change of basis:"
        ).scale(0.8).next_to(self.refl_eq_3, RIGHT).shift(RIGHT)

        self.change_of_basis_eq_1 = MathTex(
            r"{{ \ell_c }} = { {{ \ell_x }} + {{ \ell_y }} \over 2 }"
        ).scale(0.8).next_to(self.change_of_basis_tex, DOWN)
        self.change_of_basis_eq_1[0].set_color(self.color_map["lc"])

        self.change_of_basis_eq_2 = MathTex(
            r"{{ \ell_d }} = { {{ \ell_x }} - {{ \ell_y }} \over 2 }"
        ).scale(0.8).next_to(self.change_of_basis_eq_1, DOWN)
        self.change_of_basis_eq_2[0].set_color(self.color_map["ld"])

        self.change_of_basis_eq_x = MathTex(
            r"{{ \ell_x }} = {{ \ell_c }} + {{ \ell_d }}"
        ).scale(0.8).move_to(self.change_of_basis_eq_1)
        self.change_of_basis_eq_x[2].set_color(self.color_map["lc"])
        self.change_of_basis_eq_x[4].set_color(self.color_map["ld"])

        self.change_of_basis_eq_y = MathTex(
            r"{{ \ell_y }} = {{ \ell_c }} - {{ \ell_d }}"
        ).scale(0.8).move_to(self.change_of_basis_eq_2)
        self.change_of_basis_eq_y[2].set_color(self.color_map["lc"])
        self.change_of_basis_eq_y[4].set_color(self.color_map["ld"])

        ###   Scene   ###

        # Add the equations 
        self.add(self.x_arm_eq)
        self.add(self.y_arm_eq)
        self.add(self.refl_eq_1)
        self.add(self.trans_eq_1)

        # Move X and Y equations out of the way
        self.play(
            self.x_arm_eq.animate.move_to(x_eq_location),
            self.y_arm_eq.animate.move_to(y_eq_location),
            run_time=2
        )

        # Move REFL left and TRANS right
        self.play(
            self.refl_eq_1.animate.move_to(refl_eq_location),
            self.trans_eq_1.animate.move_to(trans_eq_location),
            run_time=2
        )
        self.wait(1)

        # Go through refl simplification
        self.x_arm_eq_copy = self.x_arm_eq.copy()
        self.y_arm_eq_copy = self.y_arm_eq.copy()
        self.refl_eq_1_copy = self.refl_eq_1.copy()
        self.play(
            TransformMatchingTex(self.refl_eq_1_copy, self.refl_eq_2),
            TransformMatchingTex(self.x_arm_eq_copy, self.refl_eq_2),
            TransformMatchingTex(self.y_arm_eq_copy, self.refl_eq_2),
            run_time=2
        )
        self.wait(3)

        # Set the 50:50 beamsplitter, and simpify our equations
        self.play(
            Write(self.bs_refl_tex)
        )
        self.wait(1)

        self.refl_eq_2_copy = self.refl_eq_2.copy()
        self.play(
            TransformMatchingTex(self.refl_eq_2_copy, self.refl_eq_3),
            run_time=2
        )
        self.wait(3)

        # Now do the Change of Basis
        self.play(
            Write(self.change_of_basis_tex)
        )
        self.wait(1)

        self.play(
            Write(self.change_of_basis_eq_1)
        )
        self.play(
            Write(self.change_of_basis_eq_2)
        )
        self.wait(2)

        self.play(
            TransformMatchingTex(self.change_of_basis_eq_1, self.change_of_basis_eq_x),
            TransformMatchingTex(self.change_of_basis_eq_2, self.change_of_basis_eq_y),
            run_time=2
        )
        self.wait(3)

        # Go back to the derivation
        self.refl_eq_3_copy = self.refl_eq_3.copy()
        self.change_of_basis_eq_x_copy = self.change_of_basis_eq_x.copy()
        self.change_of_basis_eq_y_copy = self.change_of_basis_eq_y.copy()
        self.play(
            TransformMatchingTex(self.refl_eq_3_copy, self.refl_eq_4),
            TransformMatchingTex(self.change_of_basis_eq_x_copy, self.refl_eq_4),
            TransformMatchingTex(self.change_of_basis_eq_y_copy, self.refl_eq_4),
            run_time=2
        )
        self.wait(3)

        self.refl_eq_4_copy = self.refl_eq_4.copy()
        self.play(
            TransformMatchingTex(self.refl_eq_4_copy, self.refl_eq_5),
            run_time=2
        )
        self.wait(2)

        self.refl_eq_5_copy = self.refl_eq_5.copy()
        self.play(
            TransformMatchingTex(self.refl_eq_5_copy, self.refl_eq_6),
            run_time=2
        )
        self.wait(2)

        self.play(
            TransformMatchingTex(self.refl_eq_6, self.refl_eq_7),
            run_time=2
        )
        self.wait(2)
        self.play(
            Write(self.michelson_refl_tex)
        )
        self.wait(2)

        # Clean up trans section

        self.play(
            self.change_of_basis_eq_x.animate.move_to(3.5*UP + 6*LEFT)
        )
        self.play(
            self.change_of_basis_eq_y.animate.next_to(self.change_of_basis_eq_x, RIGHT).shift(0.2*RIGHT)
        )
        self.play(
            self.bs_refl_tex.animate.move_to(3.5*UP + 1*LEFT)
        )
        self.play(
            Unwrite(self.change_of_basis_tex)
        )
        self.wait(1)
        
        # Start trans equations
        self.x_arm_eq_copy = self.x_arm_eq.copy()
        self.y_arm_eq_copy = self.y_arm_eq.copy()
        self.trans_eq_1_copy = self.trans_eq_1.copy()
        self.play(
            TransformMatchingTex(self.trans_eq_1_copy, self.trans_eq_2),
            TransformMatchingTex(self.x_arm_eq_copy, self.trans_eq_2),
            TransformMatchingTex(self.y_arm_eq_copy, self.trans_eq_2),
            run_time=2
        )
        self.wait(3)

        self.trans_eq_2_copy = self.trans_eq_2.copy()
        self.play(
            TransformMatchingTex(self.trans_eq_2_copy, self.trans_eq_3),
            run_time=2
        )
        self.wait(3)

        self.trans_eq_3_copy = self.trans_eq_3.copy()
        self.change_of_basis_eq_x_copy = self.change_of_basis_eq_x.copy()
        self.change_of_basis_eq_y_copy = self.change_of_basis_eq_y.copy()
        self.play(
            TransformMatchingTex(self.trans_eq_3_copy, self.trans_eq_4),
            TransformMatchingTex(self.change_of_basis_eq_x_copy, self.trans_eq_4),
            TransformMatchingTex(self.change_of_basis_eq_y_copy, self.trans_eq_4),
            run_time=2
        )
        self.wait(3)

        self.trans_eq_4_copy = self.trans_eq_4.copy()
        self.play(
            TransformMatchingTex(self.trans_eq_4_copy, self.trans_eq_5),
            run_time=2
        )
        self.wait(2)

        self.trans_eq_5_copy = self.trans_eq_5.copy()
        self.play(
            TransformMatchingTex(self.trans_eq_5_copy, self.trans_eq_6),
            run_time=2
        )
        self.wait(2)

        self.play(
            TransformMatchingTex(self.trans_eq_6, self.trans_eq_7),
            run_time=2
        )
        self.wait(2)
        self.play(
            Write(self.michelson_trans_tex)
        )
        self.wait(2)

        self.play(
            Unwrite(self.change_of_basis_eq_x),
            Unwrite(self.change_of_basis_eq_y),
            Unwrite(self.bs_refl_tex),
            Unwrite(self.x_arm_eq),
            Unwrite(self.y_arm_eq),
            # Unwrite(self.refl_eq_7),
            Unwrite(self.refl_eq_5),
            Unwrite(self.refl_eq_4),
            Unwrite(self.refl_eq_3),
            Unwrite(self.refl_eq_2),
            Unwrite(self.refl_eq_1),
            Unwrite(self.michelson_refl_tex),
            # Unwrite(self.trans_eq_7),
            Unwrite(self.trans_eq_5),
            Unwrite(self.trans_eq_4),
            Unwrite(self.trans_eq_3),
            Unwrite(self.trans_eq_2),
            Unwrite(self.trans_eq_1),
            Unwrite(self.michelson_trans_tex),
        )
        self.wait(1)