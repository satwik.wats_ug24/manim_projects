"""mirror_basics.py

Manimation of the basics of a thin mirror

Craig Cahillane
August 6, 2021
"""

from manim import *

def get_mirror_trans(refl):
    """Get amplitude transmission through mirror from amplitude reflectivity refl"""
    return np.sqrt(1 - refl**2)

class MirrorBasics(Scene):
    """Make a monochromatic, continuous input laser,
    show off the phase as a function of time and space,
    then add a simple thin mirror,
    show the reflection matrix for input beams
    """
    def get_phase(self, k=1, x=0, freq=1, time=0, phase=0):
        return 2 * PI * freq * time + k * x  + phase
    def get_sine_wave(self, amp=1, k=1, x=0, freq=1, time=0, phase=0):
        return  amp * np.sin(2 * PI * freq * time + k * x + phase)

    def rotate(self, x1, y1, theta, origin=np.array([0,0,0])):
        """Return x2 and y2 being rotated about the origin by theta.
        """
        x0 = origin[0]
        y0 = origin[1]
        x2 = (x1 - x0) * np.cos(theta) - (y1 - y0) * np.sin(theta)
        y2 = (x1 - x0) * np.sin(theta) + (y1 - y0) * np.cos(theta)
        return x2, y2

    def show_axis(self):
        x_start = np.array([-5,0,0])
        x_end = np.array([5,0,0])

        y_start = np.array([0,-3,0])
        y_end = np.array([0,3,0])

        x_axis = Line(x_start, x_end)
        y_axis = Line(y_start, y_end)

        # self.add(x_axis, y_axis)

        self.origin_point = np.array([0,0,0])

        return

    def make_electric_field(
        self, 
        amp=ValueTracker(1), 
        k=ValueTracker(1), 
        freq=ValueTracker(1), 
        time=0, 
        phase=ValueTracker(0), 
        start=np.array([-5,0,0]), 
        end=np.array([5,0,0]), 
        number_of_lines=50, 
        color=RED,
        amp_scaler=ValueTracker(1),
        k_scaler=1,
        freq_scaler=1,
        phase_offset=ValueTracker(0.0),
        x_start=None,
        x_end=None,
    ):
        """Makes an electric field VGroup of Lines.
        Electric field is defined by amp, k, freq, time, and phase ValueTrackers.
        start and end are 3D vectors defining the beginning and end of the wave.
        number_of_lines defines how many Line segments the wave is broken up into.
        x_start and x_end should either be None if we don't want to move the sine wave
        starting points, or a ValueTracker if we do.
        Returns the VGroup.
        """
        # Define the VGroup of Lines which make up the E-field, and store some ValueTrackers
        beam = VGroup()
        beam.amp = amp
        beam.k = k
        beam.freq = freq
        beam.time = time
        beam.phase = phase

        beam.x_start = x_start
        beam.x_end = x_end

        beam.amp_scaler = amp_scaler
        beam.k_scaler = k_scaler
        beam.freq_scaler = freq_scaler
        beam.phase_offset = phase_offset

        amp_float = amp_scaler.get_value() * amp.get_value()
        k_float = k_scaler * k.get_value()
        freq_float = freq_scaler * freq.get_value()
        phase_float = phase_offset.get_value() + phase.get_value()

        # Check if ValueTrackers exist
        if x_start:
            start[0] = x_start.get_value()
        if x_end:
            end[0] = x_end.get_value()

        # Get the start and end vector info.
        # This function will first calculate the wave along the x axis,
        # then rotate it appropriately.
        span = end - start
        origin = (start + end) / 2
        total_length = np.sqrt(np.sum(span**2))
        norm = span / total_length
        theta = np.arctan2(norm[1], norm[0])

        x_diff = total_length / number_of_lines

        phase_output = self.get_phase(k=k_float, x=total_length, freq=freq_float, time=time, phase=0)

        # Store some additional info
        beam.phase_output = phase_output
        beam.start = start
        beam.end = end
        beam.origin = origin
        beam.theta = theta
        beam.number_of_lines = number_of_lines
        beam.color = color

        for ii in range(number_of_lines):
            # Calculate wave in 1D
            temp_x_start = ii * x_diff
            temp_x_end = (ii + 1) * x_diff

            temp_y_start = self.get_sine_wave(amp_float, k_float, temp_x_start, freq_float, time, phase_float)
            temp_y_end = self.get_sine_wave(amp_float, k_float, temp_x_end, freq_float, time, phase_float)

            # Translate to origin = (0, 0)
            temp_x_start -= total_length/2
            temp_x_end -= total_length/2
            # temp_y_start += origin[1]
            # temp_y_end += origin[1]

            # Rotate about origin
            temp_x_start, temp_y_start = self.rotate(temp_x_start, temp_y_start, theta)
            temp_x_end, temp_y_end = self.rotate(temp_x_end, temp_y_end, theta)

            # Translate to sine wave position beam.origin
            temp_x_start += origin[0]
            temp_x_end += origin[0]
            temp_y_start += origin[1]
            temp_y_end += origin[1]

            # Define the Line
            temp_start = np.array([temp_x_start, temp_y_start, 0])
            temp_end = np.array([temp_x_end, temp_y_end, 0])

            temp_line = Line(temp_start, temp_end, color=color)
            beam.add(temp_line)

        return beam

    def sum_electric_fields(self, field1, field2, color=YELLOW, reverse=True):
        """Add together two VGroups of electric fields
        """
        if field1.number_of_lines != field2.number_of_lines:
            print()
            print("Cannot sum two fields of different lengths")
            return
        
        beam = VGroup()
        beam.color = color
        for ii in range(field1.number_of_lines):
            if reverse:
                jj = (field1.number_of_lines - 1) - ii
            else:
                jj = ii
        
            line1 = field1[ii]
            line2 = field2[jj]

            if reverse:
                new_line_start = line1.start
                new_line_end = line1.end
                new_line_start[1] = line1.start[1] + line2.end[1]
                new_line_end[1] = line1.end[1] + line2.start[1]
            else:
                new_line_start = line1.start 
                new_line_end = line1.end 
                new_line_start[1] = line1.start[1] + line2.start[1]
                new_line_end[1] = line1.end[1] + line2.end[1]

            new_line = Line(new_line_start, new_line_end, color=color)
            beam.add(new_line)

        return beam

    def construct(self):
        # Set up a timer variable
        self.timer = 0.0
        self.rate = 0.20
        number_of_lines = 500

        x_start_input = ValueTracker(-7.5)
        x_end_input = ValueTracker(1.0)

        # Mirror
        self.mirror_width = 0.5
        self.end_mirror_refl = ValueTracker(0.707)
        self.end_mirror_trans = ValueTracker(0.707)
        self.end_mirror_trans = self.end_mirror_trans.add_updater(
            lambda mob: mob.set_value( get_mirror_trans(self.end_mirror_refl.get_value()) )
        )

        self.end_mirror = always_redraw(
            lambda: Rectangle(
                height=3.2, 
                width=self.mirror_width, 
                fill_color=BLUE, 
                fill_opacity=self.end_mirror_refl.get_value()**2, 
                color=BLUE
            )
            .shift((x_end_input.get_value() + self.mirror_width/2) * RIGHT)
        )

        # Input Beam
        amp_input = ValueTracker(0.75)
        k_input = ValueTracker(PI / 2)
        freq_input = ValueTracker(1)
        time_input = 0.0
        phase_input = ValueTracker(0.0)
        
        start_input = np.array([x_start_input.get_value(), 0.0, 0.0])
        end_input = np.array([x_end_input.get_value(), 0.0, 0.0])

        self.input_beam = self.make_electric_field(
            amp_input, 
            k_input, 
            freq_input, 
            time_input, 
            phase_input, 
            start_input, 
            end_input, 
            number_of_lines,
            x_start=x_start_input,
            x_end=x_end_input,
        )

        # Refl Beam
        amp_refl = amp_input
        k_refl = k_input
        freq_refl = freq_input
        time_refl = 0.0
        phase_refl = phase_input
        x_start_refl = x_end_input
        x_end_refl = x_start_input

        start_refl = np.array([x_end_input.get_value(), 0.0, 0.0])
        end_refl = np.array([x_start_input.get_value(), 0.0, 0.0])

        amp_refl_scaler = self.end_mirror_refl
        k_refl_scaler = 1
        freq_refl_scaler = 1
        phase_refl_offset = ValueTracker(0.0)
        phase_refl_offset.add_updater(
            lambda mob: mob.set_value( 
                self.get_phase(
                    k_input.get_value(), 
                    x_end_input.get_value() - x_start_input.get_value(), 
                    freq_input.get_value(), 
                    time=0, # this is already account for in wave_updater
                    phase=0 # this is already accounted for in phase_refl
                ) 
            )
        )

        self.refl_beam = self.make_electric_field(
            amp_refl, 
            k_refl, 
            freq_refl, 
            time_refl, 
            phase_refl, 
            start_refl, 
            end_refl, 
            number_of_lines,
            color=ORANGE,
            amp_scaler=amp_refl_scaler,
            k_scaler=k_refl_scaler,
            freq_scaler=freq_refl_scaler,
            phase_offset=phase_refl_offset,
            x_start=x_start_refl,
            x_end=x_end_refl
        )

        # Transmitted Beam
        amp_trans = amp_input
        k_trans = k_input
        freq_trans = freq_input
        time_trans = 0.0
        phase_trans = phase_input
        x_start_trans = x_end_input
        x_end_trans = ValueTracker(7.0)

        start_trans = np.array([x_start_trans.get_value(), 0.0, 0.0])
        end_trans = np.array([x_end_trans.get_value(), 0.0, 0.0])

        amp_trans_scaler = self.end_mirror_trans
        k_trans_scaler = 1
        freq_trans_scaler = 1
        phase_trans_offset = phase_refl_offset

        self.trans_beam = self.make_electric_field(
            amp_trans, 
            k_trans, 
            freq_trans, 
            time_trans, 
            phase_trans, 
            start_trans, 
            end_trans, 
            number_of_lines,
            color=YELLOW,
            amp_scaler=amp_trans_scaler,
            k_scaler=k_trans_scaler,
            freq_scaler=freq_trans_scaler,
            phase_offset=phase_trans_offset,
            x_start=x_start_trans,
            x_end=x_end_trans
        )

        ## Tex
        # Electric field equations
        self.color_map = {
            r"E_\mathrm{in}" : RED,
            r"E_\mathrm{refl}" : ORANGE,
            r"E_\mathrm{trans}" : YELLOW,
            r"f" : GREEN, 
            r"\lambda" : RED_E,
            r"r" : BLUE,
            r"t" : BLUE,
        }

        self.input_tex = always_redraw(
            lambda: MathTex(r"E_\mathrm{in}", color=RED)
            .shift( 3*LEFT + 2.5*UP )
        )
        self.refl_tex_init = always_redraw(
            lambda: MathTex(r"E_\mathrm{refl}", color=ORANGE)
            .shift( 3*LEFT + 2*UP )
        )
        self.trans_tex_init = always_redraw(
            lambda: MathTex(r"E_\mathrm{trans}", color=YELLOW)
            .shift( 3*RIGHT + 2*UP )
        )
        self.refl_tex = MathTex(r"{{ E_\mathrm{refl} }} = {{ -r }} E_\mathrm{in}")
        self.refl_tex[0].set_color(ORANGE)
        self.refl_tex[2].set_color(BLUE)
        self.refl_tex[3].set_color(RED)
        self.refl_tex.shift( 3*LEFT + 2*UP )
        
        self.trans_tex = MathTex(r"{{ E_\mathrm{trans} }} = {{ \tau }} E_\mathrm{in}")
        self.trans_tex[0].set_color(YELLOW)
        self.trans_tex[2].set_color(BLUE)
        self.trans_tex[3].set_color(RED)
        self.trans_tex.shift( 4*RIGHT + 2*UP )

        self.electric_wave_init_tex = MathTex(
            r"E = E_0 e^{ i \phi }",
            substrings_to_isolate=(r"E_0", r"\phi")
        ).shift( 3*UP )
        self.electric_wave_tex = MathTex(
            r"E = E_0 e^{ i (\omega t + k x)  }",
            substrings_to_isolate=(r"E_0", r"(\omega t + k x)"),
        ).shift( 3*UP )
        self.electric_wave_tex.set_color_by_tex(r"(\omega t + k x)", YELLOW)

        # self.electric_wave_tex.set_color_by_tex_to_color_map(self.color_map)

        # Mirrors
        self.end_mirror_tex = always_redraw(
            lambda: Tex(r"Mirror", color=BLUE)
            .next_to(self.end_mirror, DOWN, buff=0.3)
        )
        self.end_mirror_refl_value_tex = MathTex(
            r"|r|^2 = ", 
            substrings_to_isolate=(r"r")
        )
        self.end_mirror_refl_value = always_redraw(
            lambda: DecimalNumber(num_decimal_places=2, color=BLUE)
            .set_value(self.end_mirror_refl.get_value()**2)
            .next_to(self.end_mirror_refl_value_tex, RIGHT, buff=0.1)
        )
        self.end_mirror_refl_tex_group = always_redraw(
            lambda: VGroup(self.end_mirror_refl_value_tex, self.end_mirror_refl_value)
            .next_to(self.end_mirror_tex, DOWN, buff=0.3) 
        )

        self.end_mirror_trans_value_tex = MathTex(
            r"| {{ \tau }} |^2 = "
        )
        self.end_mirror_trans_value = always_redraw(
            lambda: DecimalNumber(num_decimal_places=2, color=BLUE)
            .set_value(self.end_mirror_trans.get_value()**2)
            .next_to(self.end_mirror_trans_value_tex, RIGHT, buff=0.1)
        )
        self.end_mirror_trans_tex_group = always_redraw(
            lambda: VGroup(self.end_mirror_trans_value_tex, self.end_mirror_trans_value)
            .next_to(self.end_mirror_refl_tex_group, DOWN, buff=0.2)     
        )

        # Time as Decimal Number
        self.timer_value_tex = MathTex(
            r"t = ",
            substrings_to_isolate=r"t"
        ).shift(3*UP + 4*RIGHT)
        self.timer_value_tex.set_color_by_tex(r"t", YELLOW)
        self.timer_value = always_redraw(
            lambda: DecimalNumber(num_decimal_places=1)
            .set_value(self.timer)
            .next_to(self.timer_value_tex, RIGHT, buff=0.15)
        )
        self.timer_value_group = VGroup(self.timer_value_tex, self.timer_value)

        # Lambda as Decimal Number
        self.lambda_value_tex = MathTex(
            r"{{ \lambda }} = "
        ).shift(2.5*UP + 4*RIGHT)
        self.lambda_value_tex[0].set_color(GOLD)
        self.lambda_value = always_redraw(
            lambda: DecimalNumber(num_decimal_places=1)
            .set_value(2*PI/k_input.get_value())
            .next_to(self.lambda_value_tex, RIGHT, buff=0.15)
        )
        self.lambda_value_group = VGroup(self.lambda_value_tex, self.lambda_value)

        ## Updaters
        def time_updater(mob, dt):
            self.timer += dt * self.rate
            return

        def wave_updater(mob):
            temp_mob = self.make_electric_field(
                mob.amp, mob.k, mob.freq, self.timer, mob.phase, mob.start, mob.end, mob.number_of_lines, mob.color, 
                mob.amp_scaler, mob.k_scaler, mob.freq_scaler, mob.phase_offset, mob.x_start, mob.x_end
            )
            mob.become(temp_mob)
            return 

        def standing_updater(mob):
            fmob = mob.forward_beam
            bmob = mob.backward_beam
            temp_fmob = self.make_electric_field(
                fmob.amp, fmob.k, fmob.freq, self.timer, fmob.phase, fmob.start, fmob.end, fmob.number_of_lines, fmob.color, 
                fmob.amp_scaler, fmob.k_scaler, fmob.freq_scaler, fmob.phase_offset, fmob.x_start, fmob.x_end
            )
            temp_bmob = self.make_electric_field(
                bmob.amp, bmob.k, bmob.freq, self.timer, bmob.phase, bmob.start, bmob.end, bmob.number_of_lines, bmob.color, 
                bmob.amp_scaler, bmob.k_scaler, bmob.freq_scaler, bmob.phase_offset, bmob.x_start, bmob.x_end
            )
            temp_mob = self.sum_electric_fields(temp_fmob, temp_bmob, color=mob.color)
            mob.become(temp_mob)
            return 

        # Add updaters to reflection ValueTrackers which need to be updated based on the cavity length
        # Remember to add these ValueTrackers to the Scene itself with self.add()
        self.add(self.end_mirror_trans)
        self.add(amp_refl_scaler)
        self.add(phase_refl_offset)
        self.add(amp_trans_scaler)
        self.add(phase_trans_offset)

        # self.add(self.end_mirror_refl_tex_group)
        # self.add(self.end_mirror_trans_tex_group)
        self.add(x_end_input)
        x_end_input.set_value(7.5)

        # self.input_beam.add_updater(time_updater)
        self.input_beam.add_updater(wave_updater)
        self.refl_beam.add_updater(wave_updater)
        self.trans_beam.add_updater(wave_updater)

        ### Set the scene and play it

        # Create input beam
        self.play(Create(self.input_beam))
        self.play(Create(self.input_tex))
        self.wait(0.5)

        # Phase tex
        self.play(Create(self.electric_wave_init_tex))
        self.play(self.electric_wave_init_tex.animate.set_color_by_tex(r"\phi", YELLOW))
        self.wait(2)

        self.play(TransformMatchingTex(self.electric_wave_init_tex, self.electric_wave_tex))

        # Evolve phase with time
        self.play(Create(self.timer_value_group))
        self.wait(1)
        self.input_beam.add_updater(time_updater)
        self.wait(5)
        self.input_beam.remove_updater(time_updater)
        self.wait(1)

        # Evolve wavenumber with time
        self.play(Create(self.lambda_value_group))
        self.wait(1)
        self.play(
            k_input.animate.set_value( 2*PI/8.0 ), 
            run_time=3
        )
        self.wait(2)
        self.play(
            k_input.animate.set_value( 2*PI/0.5 ), 
            run_time=3
        )
        self.wait(2)
        self.play(
            k_input.animate.set_value( 2*PI/4.0 ), 
            run_time=3
        )
        self.wait(2)

        # Bring in mirror
        self.add(self.end_mirror)
        self.play(
            x_end_input.animate.set_value(1.0), 
            run_time=5
        )
        self.wait(1.0)

        self.play(Create(self.end_mirror_tex))
        self.wait(0.5)

        self.play(
            Create(self.refl_beam),
            Create(self.trans_beam)
        )
        self.play(
            Create(self.refl_tex_init),
            Create(self.trans_tex_init)
        )
        self.wait(0.5)


        # Bring in some variable definitions
        self.play(Create(self.end_mirror_refl_tex_group))
        self.play(Create(self.end_mirror_trans_tex_group))
        self.wait(1.0)

        self.play(self.end_mirror_refl_value_tex.animate.set_color_by_tex(r"r", BLUE))
        self.play(self.end_mirror_trans_value_tex[1].animate.set_color(BLUE))
        self.wait(1)

        self.play(TransformMatchingTex(self.refl_tex_init, self.refl_tex))
        self.play(TransformMatchingTex(self.trans_tex_init, self.trans_tex))


        # Change mirror position with refl beam on
        self.play(
            x_end_input.animate.set_value(0.0), 
            run_time=3
        )
        self.wait(1.0)
        self.play(
            x_end_input.animate.set_value(3.0), 
            run_time=3
        )
        self.wait(1.0)
        self.play(
            x_end_input.animate.set_value(1.0), 
            run_time=3
        )
        self.wait(3.0)

        # Change mirror reflectivity
        self.play(
            self.end_mirror_refl.animate.set_value( np.sqrt(0.99) ), 
            run_time=5
        )
        self.wait(2)

        self.play(
            self.end_mirror_refl.animate.set_value( np.sqrt(0.0) ), 
            run_time=5
        )
        self.wait(2)

        self.input_beam.add_updater(time_updater)
        self.wait(5)

        self.play(
            self.end_mirror_refl.animate.set_value( 1.0 ), 
            run_time=5
        )
        self.wait(5)

        # self.play(x_end_input.animate.set_value(3.0), run_time=2*PI)
        # self.wait(2)
