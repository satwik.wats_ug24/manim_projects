"""fabry_perot_derivation.py
Derives the circulating power inside a Fabry Perot cavity.
"""

from manim import *

def get_cav_refl(phi, r1, r2):
    """Get the Fabry-Perot cavity amplitude reflection.
    phi is single-pass phase of the light.
    """
    return (-r1 + r2 * np.exp(2j * phi))/(1 - r1 * r2 * np.exp(2j * phi))

def get_cav_buildup_toward_end_mirror(phi, r1, r2):
    """Get the cavity buildup going toward the end mirror.
    phi is single-pass phase of the light.
    """
    t1 = np.sqrt(1 - r1**2)
    return t1 / (1 - r1 * r2 * np.exp(2j * phi))

def get_cav_buildup_toward_input_mirror(phi, r1, r2):
    """Get the cavity buildup going toward the end mirror.
    phi is single-pass phase of the light.
    """
    t1 = np.sqrt(1 - r1**2)
    return t1 * r2 * np.exp(1j * phi) / (1 - r1 * r2 * np.exp(2j * phi))

def get_cav_trans(phi, r1, r2):
    """Get the cavity buildup going toward the end mirror.
    phi is single-pass phase of the light.
    """
    t1 = np.sqrt(1 - r1**2)
    t2 = np.sqrt(1 - r2**2)
    return t1 * t2 * np.exp(1j * phi) / (1 - r1 * r2 * np.exp(2j * phi))

class FabryPerotDerivation(Scene):
    """Make a two-mirror cavity, with five propogating E-fields
    1) Input beam
    2) Refl beam
    3) Cavity beam heading to end mirror
    4) Cavity beam heading back to input mirror
    5) Transmitted beam
    """
    def get_phase(self, k=1, x=0, freq=1, time=0, phase=0):
        return k * x - 2 * PI * freq * time + phase
    def get_sine_wave(self, amp=1, k=1, x=0, freq=1, time=0, phase=0):
        return  amp * np.sin(k * x - 2 * PI * freq * time + phase)

    def rotate(self, x1, y1, theta, origin=np.array([0,0,0])):
        """Return x2 and y2 being rotated about the origin by theta.
        """
        x0 = origin[0]
        y0 = origin[1]
        x2 = (x1 - x0) * np.cos(theta) - (y1 - y0) * np.sin(theta)
        y2 = (x1 - x0) * np.sin(theta) + (y1 - y0) * np.cos(theta)
        return x2, y2

    def show_axis(self):
        x_start = np.array([-5,0,0])
        x_end = np.array([5,0,0])

        y_start = np.array([0,-3,0])
        y_end = np.array([0,3,0])

        x_axis = Line(x_start, x_end)
        y_axis = Line(y_start, y_end)

        # self.add(x_axis, y_axis)

        self.origin_point = np.array([0,0,0])

        return

    def make_electric_field(
        self, 
        amp=ValueTracker(1), 
        k=ValueTracker(1), 
        freq=ValueTracker(1), 
        time=0, 
        phase=ValueTracker(0), 
        start=np.array([-5,0,0]), 
        end=np.array([5,0,0]), 
        number_of_lines=50, 
        color=RED,
        amp_scaler=ValueTracker(1),
        k_scaler=1,
        freq_scaler=1,
        phase_offset=ValueTracker(0.0),
        x_start=None,
        x_end=None,
    ):
        """Makes an electric field VGroup of Lines.
        Electric field is defined by amp, k, freq, time, and phase ValueTrackers.
        start and end are 3D vectors defining the beginning and end of the wave.
        number_of_lines defines how many Line segments the wave is broken up into.
        x_start and x_end should either be None if we don't want to move the sine wave
        starting points, or a ValueTracker if we do.
        Returns the VGroup.
        """
        # Define the VGroup of Lines which make up the E-field, and store some ValueTrackers
        beam = VGroup()
        beam.amp = amp
        beam.k = k
        beam.freq = freq
        beam.time = time
        beam.phase = phase

        beam.x_start = x_start
        beam.x_end = x_end

        beam.amp_scaler = amp_scaler
        beam.k_scaler = k_scaler
        beam.freq_scaler = freq_scaler
        beam.phase_offset = phase_offset

        amp_float = amp_scaler.get_value() * amp.get_value()
        k_float = k_scaler * k.get_value()
        freq_float = freq_scaler * freq.get_value()
        phase_float = phase_offset.get_value() + phase.get_value()

        # Check if ValueTrackers exist
        if x_start:
            start[0] = x_start.get_value()
        if x_end:
            end[0] = x_end.get_value()

        # Get the start and end vector info.
        # This function will first calculate the wave along the x axis,
        # then rotate it appropriately.
        span = end - start
        origin = (start + end) / 2
        total_length = np.sqrt(np.sum(span**2))
        norm = span / total_length
        theta = np.arctan2(norm[1], norm[0])

        x_diff = total_length / number_of_lines

        phase_output = self.get_phase(k=k_float, x=total_length, freq=freq_float, time=time, phase=0)

        # Store some additional info
        beam.phase_output = phase_output
        beam.start = start
        beam.end = end
        beam.origin = origin
        beam.theta = theta
        beam.number_of_lines = number_of_lines
        beam.color = color

        for ii in range(number_of_lines):
            # Calculate wave in 1D
            temp_x_start = ii * x_diff
            temp_x_end = (ii + 1) * x_diff

            temp_y_start = self.get_sine_wave(amp_float, k_float, temp_x_start, freq_float, time, phase_float)
            temp_y_end = self.get_sine_wave(amp_float, k_float, temp_x_end, freq_float, time, phase_float)

            # Translate to origin = (0, 0)
            temp_x_start -= total_length/2
            temp_x_end -= total_length/2
            # temp_y_start += origin[1]
            # temp_y_end += origin[1]

            # Rotate about origin
            temp_x_start, temp_y_start = self.rotate(temp_x_start, temp_y_start, theta)
            temp_x_end, temp_y_end = self.rotate(temp_x_end, temp_y_end, theta)

            # Translate to sine wave position beam.origin
            temp_x_start += origin[0]
            temp_x_end += origin[0]
            temp_y_start += origin[1]
            temp_y_end += origin[1]

            # Define the Line
            temp_start = np.array([temp_x_start, temp_y_start, 0])
            temp_end = np.array([temp_x_end, temp_y_end, 0])

            temp_line = Line(temp_start, temp_end, color=color)
            beam.add(temp_line)

        return beam

    def sum_electric_fields(self, field1, field2, color=YELLOW, reverse=True):
        """Add together two VGroups of electric fields
        """
        if field1.number_of_lines != field2.number_of_lines:
            print()
            print("Cannot sum two fields of different lengths")
            return
        
        beam = VGroup()
        beam.color = color
        for ii in range(field1.number_of_lines):
            if reverse:
                jj = (field1.number_of_lines - 1) - ii
            else:
                jj = ii
        
            line1 = field1[ii]
            line2 = field2[jj]

            if reverse:
                new_line_start = line1.start
                new_line_end = line1.end
                new_line_start[1] = line1.start[1] + line2.end[1]
                new_line_end[1] = line1.end[1] + line2.start[1]
            else:
                new_line_start = line1.start 
                new_line_end = line1.end 
                new_line_start[1] = line1.start[1] + line2.start[1]
                new_line_end[1] = line1.end[1] + line2.end[1]

            new_line = Line(new_line_start, new_line_end, color=color)
            beam.add(new_line)

        return beam

    def construct(self):
        # Set up a timer variable
        self.timer = 0.0
        self.rate = 0.25
        number_of_lines = 100

        self.color_map = {
            "in" : RED,
            "refl" : ORANGE,
            "cav east" : PURPLE,
            "cav west" : PINK,
            "trans" : YELLOW
        }

        x_start_input = ValueTracker(-7.0)
        x_input_mirror = ValueTracker(-3.0)
        x_end_mirror = ValueTracker(3.0)
        x_end_input = ValueTracker(7.0)

        # Mirrors
        self.mirror_width = 0.25
        self.input_mirror_refl = ValueTracker(0.5)
        self.end_mirror_refl = ValueTracker(0.5)

        self.input_mirror = Rectangle(
            height=3.2, width=self.mirror_width, fill_color=BLUE, fill_opacity=self.input_mirror_refl.get_value(), color=BLUE
        ).shift((x_input_mirror.get_value() - self.mirror_width/2) * RIGHT)

        self.end_mirror = Rectangle(
            height=3.2, width=self.mirror_width, fill_color=BLUE, fill_opacity=self.end_mirror_refl.get_value(), color=BLUE
        ).shift((x_end_mirror.get_value() + self.mirror_width/2) * RIGHT)

        small_height = 2.2
        mirror_height = 1.2
        height_diff = 1.5
        x_shift = -2
        self.input_mirror_small = Rectangle(
            height=mirror_height, width=self.mirror_width, fill_color=BLUE, fill_opacity=self.input_mirror_refl.get_value(), color=BLUE
        )
        self.input_mirror_small.shift((x_input_mirror.get_value() - self.mirror_width/2 + x_shift) * RIGHT + small_height * UP)

        self.end_mirror_small = Rectangle(
            height=mirror_height, width=self.mirror_width, fill_color=BLUE, fill_opacity=self.end_mirror_refl.get_value(), color=BLUE
        ).shift((x_end_mirror.get_value() + self.mirror_width/2 + x_shift) * RIGHT + small_height * UP)

        # Input Beam
        amp_input = ValueTracker(0.75)
        k_input = ValueTracker(PI / 2)
        freq_input = ValueTracker(1)
        time_input = 0.0
        phase_input = ValueTracker(0.0)
        
        start_input = np.array([-7.0, 0.0, 0.0])
        end_input = np.array([-3.0, 0.0, 0.0])

        # Calculate the cavity params
        length_input = x_end_mirror.get_value() - x_input_mirror.get_value()
        phi_input = k_input.get_value() * (end_input[0] - start_input[0])

        phi = k_input.get_value() * length_input
        r1 = self.input_mirror_refl.get_value()
        r2 = self.end_mirror_refl.get_value()

        phase_input.set_value( -1 * phi_input )

        self.input_beam = self.make_electric_field(
            amp_input, 
            k_input, 
            freq_input, 
            time_input, 
            phase_input, 
            start_input, 
            end_input, 
            number_of_lines,
            color=self.color_map["in"],
            x_start=x_start_input,
            x_end=x_input_mirror,
        )

        # Refl Beam
        amp_refl = amp_input
        k_refl = k_input
        freq_refl = freq_input
        time_refl = 0.0
        phase_refl = phase_input
        x_start_refl = x_input_mirror
        x_end_refl = x_start_input

        start_refl = np.array([-3.0, 0.0, 0.0])
        end_refl = np.array([-7.0, 0.0, 0.0])

        cav_refl = get_cav_refl(phi, r1, r2)

        amp_refl_scaler = ValueTracker(np.abs(cav_refl))
        k_refl_scaler = 1
        freq_refl_scaler = 1
        phase_refl_offset = ValueTracker(np.angle(cav_refl))

        self.refl_beam = self.make_electric_field(
            amp_refl, 
            k_refl, 
            freq_refl, 
            time_refl, 
            phase_refl, 
            start_refl, 
            end_refl, 
            number_of_lines,
            color=self.color_map["refl"],
            amp_scaler=amp_refl_scaler,
            k_scaler=k_refl_scaler,
            freq_scaler=freq_refl_scaler,
            phase_offset=phase_refl_offset,
            x_start=x_start_refl,
            x_end=x_end_refl
        )

        # Cavity Toward End Mirror Beam
        amp_cav_east = amp_input
        k_cav_east = k_input
        freq_cav_east = freq_input
        time_cav_east = 0.0
        phase_cav_east = phase_input

        x_start_cav_east = x_input_mirror
        x_end_cav_east = x_end_mirror

        start_cav_east = np.array([-3.0, 0.0, 0.0])
        end_cav_east = np.array([3.0, 0.0, 0.0])

        cav_east = get_cav_buildup_toward_end_mirror(phi, r1, r2)

        amp_cav_east_scaler = ValueTracker(np.abs(cav_east))
        k_cav_east_scaler = 1
        freq_cav_east_scaler = 1
        phase_cav_east_offset = ValueTracker(np.angle(cav_east))

        self.cav_east_beam = self.make_electric_field(
            amp_cav_east, 
            k_cav_east, 
            freq_cav_east, 
            time_cav_east, 
            phase_cav_east, 
            start_cav_east, 
            end_cav_east, 
            number_of_lines,
            color=self.color_map["cav east"],
            amp_scaler=amp_cav_east_scaler,
            k_scaler=k_cav_east_scaler,
            freq_scaler=freq_cav_east_scaler,
            phase_offset=phase_cav_east_offset,
            x_start=x_start_cav_east,
            x_end=x_end_cav_east
        )

        # Cavity Toward Input Mirror Beam
        amp_cav_west = amp_input
        k_cav_west = k_input
        freq_cav_west = freq_input
        time_cav_west = 0.0
        phase_cav_west = phase_input

        x_start_cav_west = x_end_mirror
        x_end_cav_west = x_input_mirror

        start_cav_west = np.array([3.0, 0.0, 0.0])
        end_cav_west = np.array([-3.0, 0.0, 0.0])

        cav_west = get_cav_buildup_toward_input_mirror(phi, r1, r2)

        amp_cav_west_scaler = ValueTracker(np.abs(cav_west))
        k_cav_west_scaler = 1
        freq_cav_west_scaler = 1
        phase_cav_west_offset = ValueTracker(np.angle(cav_west))

        self.cav_west_beam = self.make_electric_field(
            amp_cav_west, 
            k_cav_west, 
            freq_cav_west, 
            time_cav_west, 
            phase_cav_west, 
            start_cav_west, 
            end_cav_west, 
            number_of_lines,
            color=self.color_map["cav west"],
            amp_scaler=amp_cav_west_scaler,
            k_scaler=k_cav_west_scaler,
            freq_scaler=freq_cav_west_scaler,
            phase_offset=phase_cav_west_offset,
            x_start=x_start_cav_west,
            x_end=x_end_cav_west
        )

        # Transmitted Beam
        amp_trans = amp_input
        k_trans = k_input
        freq_trans = freq_input
        time_trans = 0.0
        phase_trans = phase_input
        x_start_trans = x_end_mirror
        x_end_trans = x_end_input

        start_trans = np.array([3.0, 0.0, 0.0])
        end_trans = np.array([7.0, 0.0, 0.0])

        cav_trans = get_cav_trans(phi, r1, r2)

        amp_trans_scaler = ValueTracker(np.abs(cav_trans))
        k_trans_scaler = 1
        freq_trans_scaler = 1
        phase_trans_offset = ValueTracker(np.angle(cav_trans))

        self.trans_beam = self.make_electric_field(
            amp_trans, 
            k_trans, 
            freq_trans, 
            time_trans, 
            phase_trans, 
            start_trans, 
            end_trans, 
            number_of_lines,
            color=self.color_map["trans"],
            amp_scaler=amp_trans_scaler,
            k_scaler=k_trans_scaler,
            freq_scaler=freq_trans_scaler,
            phase_offset=phase_trans_offset,
            x_start=x_start_trans,
            x_end=x_end_trans
        )

        ## Arrows
        self.input_arrow = Arrow(
            start=np.array([x_start_input.get_value(), 0.5, 0.0]),
            end=np.array([x_input_mirror.get_value(), 0.5, 0.0]),
            color=self.color_map["in"]
        )
        self.refl_arrow = Arrow(
            start=np.array([x_input_mirror.get_value(), -0.5, 0.0]),
            end=np.array([x_start_input.get_value(), -0.5, 0.0]),
            color=self.color_map["refl"]
        )
        self.cav_east_arrow = Arrow(
            start=np.array([x_input_mirror.get_value(), 0.5, 0.0]),
            end=np.array([x_end_mirror.get_value(), 0.5, 0.0]),
            color=self.color_map["cav east"]
        )
        self.cav_west_arrow = Arrow(
            start=np.array([x_end_mirror.get_value(), -0.5, 0.0]),
            end=np.array([x_input_mirror.get_value(), -0.5, 0.0]),
            color=self.color_map["cav west"]
        )
        self.trans_arrow = Arrow(
            start=np.array([x_end_mirror.get_value(), 0.5, 0.0]),
            end=np.array([x_end_input.get_value(), 0.5, 0.0]),
            color=self.color_map["trans"]
        )

        # Small arrows
        arrow_height_adjust = 0.20
        self.input_arrow_small = Arrow(
            start=np.array([-7, small_height + arrow_height_adjust, 0.0]),
            end=np.array([self.input_mirror_small.get_x(), small_height + arrow_height_adjust, 0.0]),
            max_tip_length_to_length_ratio=0.1,
            color=self.color_map["in"]
        )
        self.cav_east_arrow_small = Arrow(
            start=np.array([self.input_mirror_small.get_x(), small_height + arrow_height_adjust, 0.0]),
            end=np.array([self.end_mirror_small.get_x(), small_height + arrow_height_adjust, 0.0]),
            max_tip_length_to_length_ratio=0.03,
            color=self.color_map["cav east"]
        )
        self.cav_west_arrow_small = Arrow(
            start=np.array([self.end_mirror_small.get_x(), small_height, 0.0]),
            end=np.array([self.input_mirror_small.get_x(), small_height, 0.0]),
            max_tip_length_to_length_ratio=0.03,
            color=self.color_map["cav west"]
        )

        # Make a group of the stuff to uncreate later
        self.small_cav_0_group = VGroup(
            self.input_mirror_small,
            self.end_mirror_small,
            self.input_arrow_small,
            self.cav_east_arrow_small
        )

        # Copy small FP cavity, but one height_diff down
        multi = 1
        self.input_mirror_small_1 = self.input_mirror_small.copy().shift(multi * height_diff * DOWN)
        self.end_mirror_small_1 = self.end_mirror_small.copy().shift(multi * height_diff * DOWN)
        self.input_arrow_small_1 = self.input_arrow_small.copy().shift(multi * height_diff * DOWN)
        self.cav_east_arrow_small_1_1 = self.cav_east_arrow_small.copy().shift(multi * height_diff * DOWN)
        self.cav_west_arrow_small_1 = self.cav_west_arrow_small.copy().shift(multi * height_diff * DOWN)
        self.cav_east_arrow_small_1_2 = self.cav_east_arrow_small.copy().shift(multi * height_diff * DOWN).shift(2 * arrow_height_adjust * DOWN)
        self.small_cav_1_group = VGroup(
            self.input_mirror_small_1,
            self.end_mirror_small_1,
            self.input_arrow_small_1,
            self.cav_east_arrow_small_1_1,
            self.cav_west_arrow_small_1,
            self.cav_east_arrow_small_1_2
        )

        # Again, copy small FP cavity, but two height_diff down
        multi = 2 
        self.input_mirror_small_2 = self.input_mirror_small.copy().shift(multi * height_diff * DOWN)
        self.end_mirror_small_2 = self.end_mirror_small.copy().shift(multi * height_diff * DOWN)
        self.input_arrow_small_2 = self.input_arrow_small.copy().shift(multi * height_diff * DOWN)
        self.cav_east_arrow_small_2_1 = self.cav_east_arrow_small.copy().shift(multi * height_diff * DOWN).shift(1 * arrow_height_adjust * UP)
        self.cav_west_arrow_small_2_1 = self.cav_west_arrow_small.copy().shift(multi * height_diff * DOWN).shift(1 * arrow_height_adjust * UP)
        self.cav_east_arrow_small_2_2 = self.cav_east_arrow_small.copy().shift(multi * height_diff * DOWN).shift(1 * arrow_height_adjust * DOWN)
        self.cav_west_arrow_small_2_2 = self.cav_west_arrow_small.copy().shift(multi * height_diff * DOWN).shift(1 * arrow_height_adjust * DOWN)
        self.cav_east_arrow_small_2_3 = self.cav_east_arrow_small.copy().shift(multi * height_diff * DOWN).shift(3 * arrow_height_adjust * DOWN)
        self.small_cav_2_group = VGroup(
            self.input_mirror_small_2,
            self.end_mirror_small_2,
            self.input_arrow_small_2,
            self.cav_east_arrow_small_2_1,
            self.cav_west_arrow_small_2_1,
            self.cav_east_arrow_small_2_2,
            self.cav_west_arrow_small_2_2,
            self.cav_east_arrow_small_2_3
        )

        # Again, copy small FP cavity, but three height_diff down
        multi = 3
        self.input_mirror_small_3 = self.input_mirror_small.copy().shift(multi * height_diff * DOWN)
        self.end_mirror_small_3 = self.end_mirror_small.copy().shift(multi * height_diff * DOWN)
        self.input_arrow_small_3 = self.input_arrow_small.copy().shift(multi * height_diff * DOWN)
        self.cav_east_arrow_small_3_1 = self.cav_east_arrow_small.copy().shift(multi * height_diff * DOWN).shift(2 * arrow_height_adjust * UP)
        self.cav_west_arrow_small_3_1 = self.cav_west_arrow_small.copy().shift(multi * height_diff * DOWN).shift(2 * arrow_height_adjust * UP)
        self.cav_east_arrow_small_3_2 = self.cav_east_arrow_small.copy().shift(multi * height_diff * DOWN)
        self.cav_west_arrow_small_3_2 = self.cav_west_arrow_small.copy().shift(multi * height_diff * DOWN)
        self.cav_east_arrow_small_3_3 = self.cav_east_arrow_small.copy().shift(multi * height_diff * DOWN).shift(2 * arrow_height_adjust * DOWN)
        self.cav_west_arrow_small_3_3 = self.cav_west_arrow_small.copy().shift(multi * height_diff * DOWN).shift(2 * arrow_height_adjust * DOWN)
        self.cav_east_arrow_small_3_4 = self.cav_east_arrow_small.copy().shift(multi * height_diff * DOWN).shift(4 * arrow_height_adjust * DOWN)
        self.small_cav_3_group = VGroup(
            self.input_mirror_small_3,
            self.end_mirror_small_3,
            self.input_arrow_small_3,
            self.cav_east_arrow_small_3_1,
            self.cav_west_arrow_small_3_1,
            self.cav_east_arrow_small_3_2,
            self.cav_west_arrow_small_3_2,
            self.cav_east_arrow_small_3_3,
            self.cav_west_arrow_small_3_3,
            self.cav_east_arrow_small_3_4
        )

        ## Tex
        # Electric field equations
        self.input_tex    = MathTex(r"E_\mathrm{in}", color=self.color_map["in"]).move_to(2*UP + 5*LEFT)
        self.refl_tex     = MathTex(r"E_\mathrm{refl}", color=self.color_map["refl"]).move_to(2*DOWN + 5*LEFT)
        self.cav_east_tex = MathTex(r"E_\mathrm{cav\,east}", color=self.color_map["cav east"]).move_to(2*UP)
        self.cav_west_tex = MathTex(r"E_\mathrm{cav\,west}", color=self.color_map["cav west"]).move_to(2*DOWN)
        self.trans_tex    = MathTex(r"E_\mathrm{trans}", color=self.color_map["trans"]).move_to(2*UP + 5*RIGHT)

        self.e0_tex    = MathTex(r"E_0").scale(0.7).next_to(self.end_mirror_small, RIGHT).shift(RIGHT)
        self.e1_tex    = MathTex(r"E_1").scale(0.7).next_to(self.end_mirror_small_1, RIGHT).shift(RIGHT)
        self.e2_tex    = MathTex(r"E_2").scale(0.7).next_to(self.end_mirror_small_2, RIGHT).shift(RIGHT)
        self.e3_tex    = MathTex(r"E_3").scale(0.7).next_to(self.end_mirror_small_3, RIGHT).shift(RIGHT)

        self.e0_rhs_tex    = MathTex(
            r"= {{ \tau_i }}{{ E_\mathrm{in} }}"
        )
        self.e0_rhs_tex[1].set_color(BLUE)
        self.e0_rhs_tex[2].set_color(RED)
        self.e0_rhs_tex.scale(0.7)
        self.e0_rhs_tex.next_to(self.e0_tex, RIGHT)

        self.e1_rhs_tex    = MathTex(
            r"= {{ \tau_i }}{{ r_i }}{{ r_e }} e^{ i k (2 L) } {{ E_\mathrm{in} }}"
        )
        self.e1_rhs_tex[1].set_color(BLUE)
        self.e1_rhs_tex[2].set_color(BLUE)
        self.e1_rhs_tex[3].set_color(BLUE)
        self.e1_rhs_tex[5].set_color(RED)
        self.e1_rhs_tex.scale(0.7)
        self.e1_rhs_tex.next_to(self.e1_tex, RIGHT)

        self.e2_rhs_tex    = MathTex(
            r" = {{ \tau_i }} ( {{r_i }}{{ r_e }} e^{ i k (2 L) } )^2 {{ E_\mathrm{in} }}"
        )
        self.e2_rhs_tex[1].set_color(BLUE)
        self.e2_rhs_tex[3].set_color(BLUE)
        self.e2_rhs_tex[4].set_color(BLUE)
        self.e2_rhs_tex[6].set_color(RED)

        self.e2_rhs_tex.scale(0.7)
        self.e2_rhs_tex.next_to(self.e2_tex, RIGHT)

        self.e3_rhs_tex    = MathTex(
            r" = {{ \tau_i }} ( {{ r_i }}{{ r_e }} e^{ i k (2 L) } )^3 {{ E_\mathrm{in} }}"
        )
        self.e3_rhs_tex[1].set_color(BLUE)
        self.e3_rhs_tex[3].set_color(BLUE)
        self.e3_rhs_tex[4].set_color(BLUE)
        self.e3_rhs_tex[6].set_color(RED)
        self.e3_rhs_tex.scale(0.7)
        self.e3_rhs_tex.next_to(self.e3_tex, RIGHT)   

        # Mirrors
        self.input_mirror_tex = MathTex(r"r_i", color=BLUE).move_to(np.array([self.input_mirror.get_x(), 0, 0]) + 2*DOWN)
        self.end_mirror_tex =  MathTex(r"r_e", color=BLUE).move_to(np.array([self.end_mirror.get_x(), 0, 0]) + 2*DOWN)

        # Cavity Length
        self.cavity_length_tex = MathTex(r"L").move_to(np.array([self.input_mirror.get_x() + self.end_mirror.get_x(), 0, 0]) + 3 * DOWN)

        # Cavity gain 
        self.derive_tex = Tex(r"Derivation of the cavity gain")

        self.cav_gain_value_tex_init = MathTex(
            r"g = { E_\mathrm{cav\,east} \over E_\mathrm{in} }", 
            substrings_to_isolate=(r"g", r"E_\mathrm{cav\,east}", r"E_\mathrm{in}"),
        )
        self.cav_gain_value_tex_init.set_color_by_tex(r"g", YELLOW)
        self.cav_gain_value_tex_init.set_color_by_tex(r"E_\mathrm{cav\,east}", PURPLE)
        self.cav_gain_value_tex_init.set_color_by_tex(r"E_\mathrm{in}", RED)
        self.cav_gain_value_tex_init.scale(0.8)
        self.cav_gain_value_tex_init.next_to(self.derive_tex, RIGHT)

        self.derive_tex_group = VGroup(self.derive_tex, self.cav_gain_value_tex_init).move_to(3.2 * UP)

        # Cavity gain Decimal number
        self.cav_gain_tex = MathTex(
            r"g = { E_\mathrm{cav\,east} \over E_\mathrm{in} } = { \tau_i \over 1 - r_i r_e e^{ {i k (2 L) } } = ", 
            substrings_to_isolate=(r"g", r"r_i", r"r_e"),
        )
        self.cav_gain_tex.set_color_by_tex(r"g", YELLOW)
        self.cav_gain_tex.set_color_by_tex(r"E_\mathrm{cav\,east}", PURPLE)
        self.cav_gain_tex.set_color_by_tex(r"E_\mathrm{in}", RED)
        self.cav_gain_tex.set_color_by_tex(r"r_i", BLUE)
        self.cav_gain_tex.set_color_by_tex(r"r_e", BLUE)
        self.cav_gain_tex.shift(3.5 * UP)

        self.cavity_length = ValueTracker(x_end_mirror.get_value() - x_input_mirror.get_value())
        self.cavity_length.add_updater(
            lambda mob: mob.set_value( x_end_mirror.get_value() - x_input_mirror.get_value() )
        )

        self.cav_gain = ValueTracker(1.0)
        self.cav_gain.add_updater(
            lambda mob: mob.set_value(
                get_cav_buildup_toward_end_mirror(
                    k_input.get_value() * self.cavity_length.get_value(),
                    self.input_mirror_refl.get_value(),
                    self.end_mirror_refl.get_value()
                )   
            )
        )

        self.cav_gain_value = DecimalNumber(
            num_decimal_places=1
        ).next_to(self.cav_gain_tex, RIGHT, buff=0.15)
        self.cav_gain_value.add_updater(
            lambda mob: mob.set_value(self.cav_gain.get_value())
        )

        self.cav_gain_value_group = VGroup(self.cav_gain_tex, self.cav_gain_value)

        # Derivation equations
        self.derivation_equation_height = 2.5

        self.electric_field_sum_0 = MathTex(
            r"{{ E_\mathrm{cav\,east} }} = E_0 + E_1 + E_2 + E_3 + \cdots"
        )
        self.electric_field_sum_0[0].set_color(PURPLE)
        self.electric_field_sum_0.scale(0.7)
        self.electric_field_sum_0.next_to(self.derive_tex_group, DOWN)

        self.electric_field_sum_1 = MathTex(
            r"{{ E_\mathrm{cav\,east} }} = \tau_i {{ E_\mathrm{in} }} + E_1 + E_2 + E_3 + \cdots",
            substrings_to_isolate=(r"E_\mathrm{cav\,east}", r"E_\mathrm{in}", r"\tau_i")
        )
        self.electric_field_sum_1.set_color_by_tex(r"E_\mathrm{cav\,east}", PURPLE)
        self.electric_field_sum_1.set_color_by_tex(r"E_\mathrm{in}", RED)
        self.electric_field_sum_1.set_color_by_tex(r"\tau_i", BLUE)
        self.electric_field_sum_1.scale(0.7)
        self.electric_field_sum_1.move_to(self.derive_tex_group)

        self.electric_field_sum_2 = MathTex(
            r"{{ E_\mathrm{cav\,east} }} = \tau_i {{ E_\mathrm{in} }} + \tau_i r_i r_e e^{ i k (2 L) } {{ E_\mathrm{in} }} + E_2 + E_3 + \cdots",
            substrings_to_isolate=(r"E_\mathrm{cav\,east}", r"E_\mathrm{in}", r"\tau_i", r"r_i", r"r_e")
        )
        self.electric_field_sum_2.set_color_by_tex(r"E_\mathrm{cav\,east}", PURPLE)
        self.electric_field_sum_2.set_color_by_tex(r"E_\mathrm{in}", RED)
        self.electric_field_sum_2.set_color_by_tex(r"\tau_i", BLUE)
        self.electric_field_sum_2.set_color_by_tex(r"r_i", BLUE)
        self.electric_field_sum_2.set_color_by_tex(r"r_e", BLUE)
        self.electric_field_sum_2.scale(0.7)
        self.electric_field_sum_2.move_to(self.derive_tex_group)

        self.electric_field_sum_3 = MathTex(
            r"{{ E_\mathrm{cav\,east} }} = \tau_i {{ E_\mathrm{in} }} + \tau_i r_i r_e e^{ i k (2 L) } {{ E_\mathrm{in} }} + \tau_i (r_i r_e e^{ i k (2 L) })^2 {{ E_\mathrm{in} }} + E_3 + \cdots",
            substrings_to_isolate=(r"E_\mathrm{cav\,east}", r"E_\mathrm{in}", r"\tau_i", r"r_i", r"r_e")
        )
        self.electric_field_sum_3.set_color_by_tex(r"E_\mathrm{cav\,east}", PURPLE)
        self.electric_field_sum_3.set_color_by_tex(r"E_\mathrm{in}", RED)
        self.electric_field_sum_3.set_color_by_tex(r"\tau_i", BLUE)
        self.electric_field_sum_3.set_color_by_tex(r"r_i", BLUE)
        self.electric_field_sum_3.set_color_by_tex(r"r_e", BLUE)
        self.electric_field_sum_3.scale(0.7)
        self.electric_field_sum_3.move_to(self.derive_tex_group)

        self.electric_field_sum_4 = MathTex(
            r"{{ E_\mathrm{cav\,east} }} = \tau_i {{ E_\mathrm{in} }} + \tau_i r_i r_e e^{ i k (2 L) } {{ E_\mathrm{in} }} + \tau_i (r_i r_e e^{ i k (2 L) })^2 {{ E_\mathrm{in} }} + \tau_i (r_i r_e e^{ i k (2 L) })^3 {{ E_\mathrm{in} }} + \cdots",
            substrings_to_isolate=(r"E_\mathrm{cav\,east}", r"E_\mathrm{in}", r"\tau_i", r"r_i", r"r_e")
        )
        self.electric_field_sum_4.set_color_by_tex(r"E_\mathrm{cav\,east}", PURPLE)
        self.electric_field_sum_4.set_color_by_tex(r"E_\mathrm{in}", RED)
        self.electric_field_sum_4.set_color_by_tex(r"\tau_i", BLUE)
        self.electric_field_sum_4.set_color_by_tex(r"r_i", BLUE)
        self.electric_field_sum_4.set_color_by_tex(r"r_e", BLUE)
        self.electric_field_sum_4.scale(0.7)
        self.electric_field_sum_4.move_to(self.derive_tex_group)

        self.final_equation_height = 2 * UP
        self.electric_field_sum_5 = MathTex(
            r"{{ E_\mathrm{cav\,east} }} = \tau_i {{ E_\mathrm{in} }} (1 + r_i r_e e^{ i k (2 L) } + (r_i r_e e^{ i k (2 L) })^2 + (r_i r_e e^{ i k (2 L) })^3 + \cdots )",
            substrings_to_isolate=(r"E_\mathrm{cav\,east}", r"E_\mathrm{in}", r"\tau_i", r"r_i", r"r_e")
        )
        self.electric_field_sum_5.set_color_by_tex(r"E_\mathrm{cav\,east}", PURPLE)
        self.electric_field_sum_5.set_color_by_tex(r"E_\mathrm{in}", RED)
        self.electric_field_sum_5.set_color_by_tex(r"\tau_i", BLUE)
        self.electric_field_sum_5.set_color_by_tex(r"r_i", BLUE)
        self.electric_field_sum_5.set_color_by_tex(r"r_e", BLUE)
        self.electric_field_sum_5.scale(0.7)
        self.electric_field_sum_5.move_to(self.final_equation_height)

        self.electric_field_sum_6 = MathTex(
            r"{{ E_\mathrm{cav\,east} }} = { \tau_i \over 1 - r_i r_e e^{ i k (2 L) } } {{ E_\mathrm{in} }}",
            substrings_to_isolate=(r"E_\mathrm{cav\,east}", r"E_\mathrm{in}", r"\tau_i", r"r_i", r"r_e")
        )
        self.electric_field_sum_6.set_color_by_tex(r"E_\mathrm{cav\,east}", PURPLE)
        self.electric_field_sum_6.set_color_by_tex(r"E_\mathrm{in}", RED)
        self.electric_field_sum_6.set_color_by_tex(r"\tau_i", BLUE)
        self.electric_field_sum_6.set_color_by_tex(r"r_i", BLUE)
        self.electric_field_sum_6.set_color_by_tex(r"r_e", BLUE)
        self.electric_field_sum_6.scale(0.7)
        self.electric_field_sum_6.move_to(self.final_equation_height)

        # Geometric Series
        self.geometric_series_name = Tex(r"Geometric series: ").scale(0.7)
        self.geometric_series_name.move_to(ORIGIN)
        self.geometric_series = MathTex(
            r"\sum_{n=0}^{\infty} a x^{n} = { a \over 1 - x } \qquad \mathrm{if}~|x| < 1"
        ).next_to(self.geometric_series_name, DOWN).scale(0.7)

        self.geometric_series_a = MathTex(
            r"a = {{ \tau_i }}{{ E_\mathrm{in} }}"
        ).scale(0.7)
        self.geometric_series_a[1].set_color(BLUE)
        self.geometric_series_a[2].set_color(RED)

        self.geometric_series_x = MathTex(
            r"x = {{ r_i }}{{ r_e }} e^{ i k (2 L) }"
        ).next_to(self.geometric_series_a, DOWN).scale(0.7)
        self.geometric_series_x[1].set_color(BLUE)
        self.geometric_series_x[2].set_color(BLUE)

        self.geometric_series_group = VGroup(self.geometric_series_name, self.geometric_series)
        self.geometric_series_consts_group = VGroup(self.geometric_series_a, self.geometric_series_x)

        # self.geometric_series_group.next_to(self.electric_field_sum_6, DOWN)
        self.geometric_series_consts_group.next_to(self.geometric_series_group, DOWN)

        ## Updaters
        def time_updater(mob, dt):
            self.timer += dt * self.rate
            return

        def wave_updater(mob):
            temp_mob = self.make_electric_field(
                mob.amp, mob.k, mob.freq, self.timer, mob.phase, mob.start, mob.end, mob.number_of_lines, mob.color, 
                mob.amp_scaler, mob.k_scaler, mob.freq_scaler, mob.phase_offset, mob.x_start, mob.x_end
            )
            mob.become(temp_mob)
            return 

        def standing_updater(mob):
            fmob = mob.forward_beam
            bmob = mob.backward_beam
            temp_fmob = self.make_electric_field(
                fmob.amp, fmob.k, fmob.freq, self.timer, fmob.phase, fmob.start, fmob.end, fmob.number_of_lines, fmob.color, 
                fmob.amp_scaler, fmob.k_scaler, fmob.freq_scaler, fmob.phase_offset, fmob.x_start, fmob.x_end
            )
            temp_bmob = self.make_electric_field(
                bmob.amp, bmob.k, bmob.freq, self.timer, bmob.phase, bmob.start, bmob.end, bmob.number_of_lines, bmob.color, 
                bmob.amp_scaler, bmob.k_scaler, bmob.freq_scaler, bmob.phase_offset, bmob.x_start, bmob.x_end
            )
            temp_mob = self.sum_electric_fields(temp_fmob, temp_bmob, color=mob.color)
            mob.become(temp_mob)
            return 

        # Add updaters to reflection ValueTrackers which need to be updated based on the cavity length
        # Remember to add these ValueTrackers to the Scene itself with self.add()
        self.add(k_input)
        self.add(self.cavity_length)
        self.add(self.input_mirror_refl)
        self.add(self.end_mirror_refl)
        self.add(self.cav_gain)

        # Set the scene and play it
        # self.show_axis()

        self.play(
            GrowFromCenter(self.input_mirror),
            GrowFromCenter(self.end_mirror)
        )
        self.wait(0.5)
        self.play(
            Write(self.input_mirror_tex), 
            Write(self.end_mirror_tex),
            Write(self.cavity_length_tex)
        )
        self.wait(0.5)

        self.play(
            Create(self.input_beam),
            Create(self.refl_beam),
            Create(self.cav_east_beam),
            Create(self.cav_west_beam),
            Create(self.trans_beam)
        )
        self.wait(0.5)
        self.play(
            Write(self.input_tex),
            Write(self.refl_tex),
            Write(self.cav_east_tex),
            Write(self.cav_west_tex),
            Write(self.trans_tex)
        )
        self.wait(0.5)

        self.play(
            Transform(self.input_beam, self.input_arrow),
            Transform(self.refl_beam, self.refl_arrow),
            Transform(self.cav_east_beam, self.cav_east_arrow),
            Transform(self.cav_west_beam, self.cav_west_arrow),
            Transform(self.trans_beam, self.trans_arrow),
        )
        self.wait(0.5)

        # Start derivation
        self.play(Write(self.derive_tex_group))
        self.wait(1.0)

        self.play(
            Uncreate(self.input_tex),
            Uncreate(self.refl_tex),
            Uncreate(self.cav_east_tex),
            Uncreate(self.cav_west_tex),
            Uncreate(self.trans_tex)
        )
        self.wait(0.5)

        # Put the initial infinite sum on screen
        self.play(Write(self.electric_field_sum_0))
        self.wait(0.5)
        self.play(Unwrite(self.derive_tex_group))
        self.wait(0.5)

        self.play(self.electric_field_sum_0.animate.shift(UP))
        self.wait(0.5)

        # Remove some extraneous info
        self.play(
            Uncreate(self.input_beam),
            Uncreate(self.refl_beam),
            Uncreate(self.cav_east_beam),
            Uncreate(self.cav_west_beam),
            Uncreate(self.trans_beam),
            Transform(self.input_mirror, self.input_mirror_small),
            Transform(self.end_mirror, self.end_mirror_small),
            self.input_mirror_tex.animate.next_to(self.input_mirror_small, DOWN),
            self.end_mirror_tex.animate.next_to(self.end_mirror_small, DOWN),
            self.cavity_length_tex.animate.move_to( (self.input_mirror_small.get_center() + self.end_mirror_small.get_center()) / 2 ).shift(DOWN)
        )
        self.wait(1.0)

        # Show the zeroth term
        self.play(Write(self.e0_tex))
        self.wait(0.5)

        self.play(
            Create(self.input_arrow_small),
            run_time=1.0
        )
        self.wait(0.5)
        self.play(
            Create(self.cav_east_arrow_small),
            run_time=1.0
        )
        self.wait(0.5)

        self.play(Write(self.e0_rhs_tex))
        self.wait(1.0)

        # Write the zeroth term
        # self.play(TransformMatchingTex(self.electric_field_sum_0, self.electric_field_sum_1))
        # self.wait(0.5)

        # Show the first term derivation
        self.input_arrow_small_copy = self.input_arrow_small.copy()
        self.cav_east_arrow_small_copy = self.cav_east_arrow_small.copy()
        self.play(
            Transform(self.input_mirror_small, self.input_mirror_small_1),
            Transform(self.end_mirror_small, self.end_mirror_small_1),
            Transform(self.input_arrow_small_copy, self.input_arrow_small_1),
            Transform(self.cav_east_arrow_small_copy, self.cav_east_arrow_small_1_1),
            self.input_mirror_tex.animate.next_to(self.input_mirror_small_1, DOWN),
            self.end_mirror_tex.animate.next_to(self.end_mirror_small_1, DOWN),
            self.cavity_length_tex.animate.move_to( (self.input_mirror_small_1.get_center() + self.end_mirror_small_1.get_center()) / 2 ).shift(DOWN),
            run_time=3
        )
        self.wait(0.5)

        self.play(Write(self.e1_tex))
        self.wait(1.0)

        self.play(FadeIn(self.cav_west_arrow_small_1))
        self.wait(0.5)
        self.play(FadeIn(self.cav_east_arrow_small_1_2))
        self.wait(0.5)

        self.play(Write(self.e1_rhs_tex))
        self.wait(1.0)

        # Write the first term
        # self.play(TransformMatchingTex(self.electric_field_sum_1, self.electric_field_sum_2))
        # self.wait(0.5)

        # Show the second term derivation
        self.input_mirror_small_1_copy = self.input_mirror_small_1.copy()
        self.end_mirror_small_1_copy = self.end_mirror_small_1.copy()
        self.input_arrow_small_1_copy = self.input_arrow_small_1.copy()
        self.cav_east_arrow_small_1_1_copy = self.cav_east_arrow_small_1_1.copy()
        self.play(
            Transform(self.input_mirror_small_1_copy, self.input_mirror_small_2),
            Transform(self.end_mirror_small_1_copy, self.end_mirror_small_2),
            Transform(self.input_arrow_small_1_copy, self.input_arrow_small_2),
            Transform(self.cav_east_arrow_small_1_1_copy, self.cav_east_arrow_small_2_1),
            self.input_mirror_tex.animate.next_to(self.input_mirror_small_2, DOWN),
            self.end_mirror_tex.animate.next_to(self.end_mirror_small_2, DOWN),
            self.cavity_length_tex.animate.move_to( (self.input_mirror_small_2.get_center() + self.end_mirror_small_2.get_center()) / 2 ).shift(DOWN),
            run_time=3
        )
        self.wait(0.5)

        self.play(Write(self.e2_tex))
        self.wait(1.0)

        # round trips
        self.play(
            FadeIn(self.cav_west_arrow_small_2_1),
            run_time=0.5
        )
        self.play(
            FadeIn(self.cav_east_arrow_small_2_2),
            run_time=0.5
        )
        self.play(
            FadeIn(self.cav_west_arrow_small_2_2),
            run_time=0.5
        )
        self.play(
            FadeIn(self.cav_east_arrow_small_2_3),
            run_time=0.5
        )

        self.play(Write(self.e2_rhs_tex))
        self.wait(1.0)

        # Write the second term
        # self.play(TransformMatchingTex(self.electric_field_sum_2, self.electric_field_sum_3))
        # self.wait(0.5)

        # Show the third term derivation
        self.input_mirror_small_2_copy = self.input_mirror_small_2.copy()
        self.end_mirror_small_2_copy = self.end_mirror_small_2.copy()
        self.input_arrow_small_2_copy = self.input_arrow_small_2.copy()
        self.cav_east_arrow_small_2_1_copy = self.cav_east_arrow_small_2_1.copy()
        self.play(
            Transform(self.input_mirror_small_2_copy, self.input_mirror_small_3),
            Transform(self.end_mirror_small_2_copy, self.end_mirror_small_3),
            Transform(self.input_arrow_small_2_copy, self.input_arrow_small_3),
            Transform(self.cav_east_arrow_small_2_1_copy, self.cav_east_arrow_small_3_1),
            self.input_mirror_tex.animate.next_to(self.input_mirror_small_3, DOWN),
            self.end_mirror_tex.animate.next_to(self.end_mirror_small_3, DOWN),
            self.cavity_length_tex.animate.move_to( (self.input_mirror_small_3.get_center() + self.end_mirror_small_3.get_center()) / 2 ).shift(DOWN),
            run_time=3
        )
        self.wait(0.5)

        self.play(Write(self.e3_tex))
        self.wait(1.0)

        # round trips
        self.play(
            FadeIn(self.cav_west_arrow_small_3_1),
            FadeIn(self.cav_east_arrow_small_3_2),
            FadeIn(self.cav_west_arrow_small_3_2),
            FadeIn(self.cav_east_arrow_small_3_3),
            FadeIn(self.cav_west_arrow_small_3_3),
            FadeIn(self.cav_east_arrow_small_3_4)
        )

        self.play(Write(self.e3_rhs_tex))
        self.wait(1.0)

        # Write the third term
        self.play(
            TransformMatchingTex(self.electric_field_sum_0, self.electric_field_sum_1),
            TransformMatchingTex(self.e0_rhs_tex, self.electric_field_sum_1)
        )
        self.wait(0.2)
        self.play(
            TransformMatchingTex(self.electric_field_sum_1, self.electric_field_sum_2),
            TransformMatchingTex(self.e1_rhs_tex, self.electric_field_sum_2)
        )
        self.wait(0.2)
        self.play(
            TransformMatchingTex(self.electric_field_sum_2, self.electric_field_sum_3),
            TransformMatchingTex(self.e2_rhs_tex, self.electric_field_sum_3)
        )
        self.wait(0.2)
        self.play(
            TransformMatchingTex(self.electric_field_sum_3, self.electric_field_sum_4),
            TransformMatchingTex(self.e3_rhs_tex, self.electric_field_sum_4)
        )
        self.wait(0.2)
        self.wait(2)

        # Recall the geometric series, (will have to move some stuff)
        self.play(
            FadeOut(self.input_mirror),
            FadeOut(self.end_mirror),
            FadeOut(self.small_cav_0_group),
            FadeOut(self.e0_tex),
            FadeOut(self.input_arrow_small_copy),
            FadeOut(self.cav_east_arrow_small_copy),
            FadeOut(self.small_cav_1_group),
            FadeOut(self.e1_tex),
            FadeOut(self.input_mirror_small_1_copy),
            FadeOut(self.end_mirror_small_1_copy),
            FadeOut(self.input_arrow_small_1_copy),
            FadeOut(self.cav_east_arrow_small_1_1_copy),
            FadeOut(self.small_cav_2_group),
            FadeOut(self.e2_tex),
            FadeOut(self.input_mirror_small_2_copy),
            FadeOut(self.end_mirror_small_2_copy),
            FadeOut(self.input_arrow_small_2_copy),
            FadeOut(self.cav_east_arrow_small_2_1_copy),
            FadeOut(self.small_cav_3_group),
            FadeOut(self.e3_tex),
            FadeOut(self.input_mirror_tex),
            FadeOut(self.end_mirror_tex),
            FadeOut(self.cavity_length_tex)
        )

        self.play(self.electric_field_sum_4.animate.move_to(self.final_equation_height))
        self.wait(3.0)

        self.play(Write(self.geometric_series_group))
        self.wait(1.0)

        self.play(Write(self.geometric_series_consts_group))
        self.wait(1.0)

        # Rearrange
        self.play(TransformMatchingTex(self.electric_field_sum_4, self.electric_field_sum_5))
        self.wait(1)

        # Solve
        self.play(TransformMatchingTex(self.electric_field_sum_5, self.electric_field_sum_6))
        self.wait(1)

        # Remove Geometric Series stuff
        self.play(
            Unwrite(self.geometric_series_group),
            Unwrite(self.geometric_series_consts_group)
        )
        self.wait(1.0)

        # Embiggen the final E_cav/E_in equation
        self.play(
            self.electric_field_sum_6.animate.scale(1.0/0.7)
        )
        self.wait(1)



        # So now we have the equation for the cavity gain g,
        # so what?
        # Let's go back to our two-mirror cavity and move the length around
        # x_end_mirror.set_value(2.5)
        # self.play(
        #     GrowFromCenter(self.input_mirror),
        #     GrowFromCenter(self.end_mirror),
        #     Write(self.input_mirror_tex), 
        #     Write(self.end_mirror_tex),
        #     Write(self.cavity_length_tex),
        #     Create(self.input_beam),
        #     Create(self.refl_beam),
        #     Create(self.cav_east_beam),
        #     Create(self.cav_west_beam),
        #     Create(self.trans_beam),
        # )
        # self.play(
        #     Write(self.cav_gain_value_group)
        # )
        # self.wait(1)

        # self.play(
        #     x_end_mirror.animate.set_value(4),
        #     run_time=2
        # )

        # self.play(Create(self.cav_gain_value_tex_init))
        # self.wait(1.0)

        # self.play(
        #     Transform(self.cav_gain_value_tex_init, self.cav_gain_value_tex),
        #     Create(self.cav_gain_value)
        # )
        # self.wait(1.0)

        # # self.input_beam.add_updater(time_updater)
        # self.input_beam.add_updater(wave_updater)
        # self.refl_beam.add_updater(wave_updater)
        # self.cav_east_beam.add_updater(wave_updater)
        # self.cav_west_beam.add_updater(wave_updater)
        # self.trans_beam.add_updater(wave_updater)

        # self.standing_beam_refl.add_updater(standing_updater)
        # self.standing_beam_intra.add_updater(standing_updater)
        
        # self.play(
        #     self.input_mirror_refl.animate.set_value(0.9),
        #     self.end_mirror_refl.animate.set_value(0.9), 
        #     run_time=10
        # )
        # self.wait(2)

        # self.play(x_end_mirror.animate.set_value(3.0), run_time=2*PI)
        # self.wait(2)
