"""beam_matrices.py

Manimation of beam propagation and beam reflection from both sides of a thin mirror.
Assumptions: Plane waves and thin optics.

Craig Cahillane
August 10, 2021
"""

from manim import *

def get_mirror_trans(refl):
    """Get amplitude transmission through mirror from amplitude reflectivity refl"""
    return np.sqrt(1 - refl**2)

class BeamMatricies(Scene):
    """Illustrate beam propagation and lossless thin dielectric surface"""
    def get_phase(self, k=1, x=0, freq=1, time=0, phase=0):
        return k * x + 2 * PI * freq * time + phase

    def get_sine_wave(self, amp=1, k=1, x=0, freq=1, time=0, phase=0):
        return  amp * np.sin(k * x + 2 * PI * freq * time + phase)

    def rotate(self, x1, y1, theta, origin=np.array([0,0,0])):
        """Return x2 and y2 being rotated about the origin by theta.
        """
        x0 = origin[0]
        y0 = origin[1]
        x2 = (x1 - x0) * np.cos(theta) - (y1 - y0) * np.sin(theta)
        y2 = (x1 - x0) * np.sin(theta) + (y1 - y0) * np.cos(theta)
        return x2, y2

    def make_electric_field(
        self, 
        amp=ValueTracker(1), 
        k=ValueTracker(1), 
        freq=ValueTracker(1), 
        time=0, 
        phase=ValueTracker(0), 
        start=np.array([-5,0,0]), 
        end=np.array([5,0,0]), 
        number_of_lines=50, 
        color=RED,
        amp_scaler=ValueTracker(1),
        k_scaler=1,
        freq_scaler=1,
        phase_offset=ValueTracker(0.0),
        x_start=None,
        x_end=None,
        y_start=None,
        y_end=None,
    ):
        """Makes an electric field VGroup of Lines.
        Electric field is defined by amp, k, freq, time, and phase ValueTrackers.
        start and end are 3D vectors defining the beginning and end of the wave.
        number_of_lines defines how many Line segments the wave is broken up into.
        x_start and x_end should either be None if we don't want to move the sine wave
        starting points, or a ValueTracker if we do.
        Returns the VGroup.
        """
        # Define the VGroup of Lines which make up the E-field, and store some ValueTrackers
        beam = VGroup()
        beam.amp = amp
        beam.k = k
        beam.freq = freq
        beam.time = time
        beam.phase = phase

        beam.x_start = x_start
        beam.x_end = x_end
        beam.y_start = y_start
        beam.y_end = y_end

        beam.amp_scaler = amp_scaler
        beam.k_scaler = k_scaler
        beam.freq_scaler = freq_scaler
        beam.phase_offset = phase_offset

        amp_float = amp_scaler.get_value() * amp.get_value()
        k_float = k_scaler * k.get_value()
        freq_float = freq_scaler * freq.get_value()
        phase_float = phase_offset.get_value() + phase.get_value()

        # Check if ValueTrackers exist for start and end pos
        if x_start:
            start[0] = x_start.get_value()
        if x_end:
            end[0] = x_end.get_value()

        if y_start:
            start[1] = y_start.get_value()
        if y_end:
            end[1] = y_end.get_value()

        # Get the start and end vector info.
        # This function will first calculate the wave along the x axis,
        # then rotate it appropriately.
        span = end - start
        origin = (start + end) / 2
        total_length = np.sqrt(np.sum(span**2))
        norm = span / total_length
        theta = np.arctan2(norm[1], norm[0])

        x_diff = total_length / number_of_lines

        phase_output = self.get_phase(k=k_float, x=total_length, freq=freq_float, time=time, phase=0)

        # Store some additional info
        beam.phase_output = phase_output
        beam.start = start
        beam.end = end
        beam.origin = origin
        beam.theta = theta
        beam.number_of_lines = number_of_lines
        beam.color = color

        for ii in range(number_of_lines):
            # Calculate wave in 1D
            temp_x_start = ii * x_diff
            temp_x_end = (ii + 1) * x_diff

            temp_y_start = self.get_sine_wave(amp_float, k_float, temp_x_start, freq_float, time, phase_float)
            temp_y_end = self.get_sine_wave(amp_float, k_float, temp_x_end, freq_float, time, phase_float)

            # Translate to origin = (0, 0)
            temp_x_start -= total_length/2
            temp_x_end -= total_length/2
            # temp_y_start += origin[1]
            # temp_y_end += origin[1]

            # Rotate about origin
            temp_x_start, temp_y_start = self.rotate(temp_x_start, temp_y_start, theta)
            temp_x_end, temp_y_end = self.rotate(temp_x_end, temp_y_end, theta)

            # Translate to sine wave position beam.origin
            temp_x_start += origin[0]
            temp_x_end += origin[0]
            temp_y_start += origin[1]
            temp_y_end += origin[1]

            # Define the Line
            temp_start = np.array([temp_x_start, temp_y_start, 0])
            temp_end = np.array([temp_x_end, temp_y_end, 0])

            temp_line = Line(temp_start, temp_end, color=color)
            beam.add(temp_line)

        return beam

    def sum_electric_fields(self, field1, field2, color=YELLOW, reverse=False, y_axis=None):
        """Add together two VGroups of electric fields.
        y_axis should be a ValueTracker defining the height of the summed wave
        """
        if field1.number_of_lines != field2.number_of_lines:
            print()
            print("Cannot sum two fields of different lengths")
            return
        
        beam = VGroup()
        beam.field1 = field1
        beam.field2 = field2
        beam.color = color
        beam.reverse = reverse
        beam.y_axis = y_axis

        if field1.y_start:
            nominal_y1 = field1.y_start.get_value()
        else:
            nominal_y1 = 0

        if field2.y_start:
            nominal_y2 = field2.y_start.get_value()
        else:
            nominal_y2 = 0

        if y_axis:
            y_axis_value = y_axis.get_value()

        for ii in range(field1.number_of_lines):
            if reverse:
                jj = (field1.number_of_lines - 1) - ii
            else:
                jj = ii
        
            line1 = field1[ii]
            line2 = field2[jj]

            if reverse:
                new_line_start = line1.start
                new_line_end = line1.end
                new_line_start[1] = (line1.start[1] - nominal_y1) + (line2.end[1] - nominal_y2) + y_axis_value
                new_line_end[1] = (line1.end[1] - nominal_y1) + (line2.start[1] - nominal_y2) + y_axis_value
            else:
                new_line_start = line1.start
                new_line_end = line1.end
                new_line_start[1] = (line1.start[1] - nominal_y1) + (line2.start[1]  - nominal_y2) + y_axis_value
                new_line_end[1] = (line1.end[1] - nominal_y1) + (line2.end[1] - nominal_y2) + y_axis_value

            new_line = Line(new_line_start, new_line_end, color=color)
            beam.add(new_line)

        return beam

    def construct(self):
        
        # Color map
        self.color_map = {
            r"a_1" : RED,
            r"b_1" : ORANGE,
            r"a_2" : PINK,
            r"b_2" : YELLOW,
            r"mirror" : BLUE,
        }

        # Set up a timer variable
        self.timer = 0.0
        self.rate = 0.20
        number_of_lines = 150

        # Position ValueTrackers
        left_start = ValueTracker(-7.0)
        left_end = ValueTracker(7.0)
        right_end = ValueTracker(2.0)
        right_start = ValueTracker(7.0)

        mirror_position = ValueTracker(8.0)

        up_start = ValueTracker(0.5)
        down_start = ValueTracker(-2.5)

        # Reflection ValueTrackers
        end_mirror_refl = ValueTracker(1.0)
        end_mirror_trans = ValueTracker(1.0)

        # Mirror
        mirror_width = 0.5

        self.end_mirror = Rectangle(
            height=5.4, 
            width=mirror_width, 
            fill_color=BLUE, 
            fill_opacity=end_mirror_refl.get_value(), 
            color=BLUE
        ).shift((mirror_width/2) * RIGHT + DOWN)
        self.end_mirror.add_updater(
            lambda mob: mob.move_to((mirror_position.get_value() + mirror_width/2) * RIGHT + DOWN)
        )
        self.end_mirror.add_updater(
            lambda mob: mob.set_fill(BLUE, opacity=end_mirror_refl.get_value()**2)
        )

        ## Electric fields
        # Input Beam a_1
        amp_input = ValueTracker(0.75)
        k_input = ValueTracker(PI / 2)
        freq_input = ValueTracker(1)
        time_input = 0.0
        phase_input = ValueTracker(0.0)
        
        start_input = np.array([left_start.get_value(), up_start.get_value(), 0.0])
        end_input = np.array([left_end.get_value(), up_start.get_value(), 0.0])

        self.input_beam = self.make_electric_field(
            amp_input, 
            k_input, 
            freq_input, 
            time_input, 
            phase_input, 
            start_input, 
            end_input, 
            number_of_lines,
            color=self.color_map[r"a_1"],
            x_start=left_start,
            x_end=left_end,
            y_start=up_start,
            y_end=up_start,
        )

        # Second Input Beam a_2
        amp_input_2 = ValueTracker(0.75)
        k_input_2 = ValueTracker(PI / 2)
        freq_input_2 = ValueTracker(1)
        time_input_2 = 0.0
        phase_input_2 = ValueTracker(0.0)
        
        start_input_2 = np.array([right_start.get_value(), down_start.get_value(), 0.0])
        end_input_2 = np.array([right_end.get_value(), down_start.get_value(), 0.0])

        self.input_2_beam = self.make_electric_field(
            amp_input_2, 
            k_input_2, 
            freq_input_2, 
            time_input_2, 
            phase_input_2, 
            start_input_2, 
            end_input_2, 
            number_of_lines,
            color=self.color_map[r"a_2"],
            x_start=right_start,
            x_end=right_end,
            y_start=down_start,
            y_end=down_start,
        )

        # Refl Beam b_1
        amp_refl = amp_input_2
        k_refl = k_input_2
        freq_refl = freq_input_2
        time_refl = 0.0
        phase_refl = phase_input_2

        start_refl = np.array([left_end.get_value(), down_start.get_value(), 0.0])
        end_refl = np.array([left_start.get_value(), down_start.get_value(), 0.0])

        amp_refl_scaler = end_mirror_trans
        k_refl_scaler = 1
        freq_refl_scaler = 1
        phase_refl_offset = ValueTracker(0.0)
        phase_refl_offset.add_updater(
            lambda mob: mob.set_value( 
                self.get_phase(
                    k_input_2.get_value(), 
                    right_start.get_value() - left_end.get_value(), 
                    freq_input_2.get_value(), 
                    time=0, # this is already account for in wave_updater
                    phase=0 # this is already accounted for in phase_refl
                ) 
            )
        )

        self.refl_beam = self.make_electric_field(
            amp_refl, 
            k_refl, 
            freq_refl, 
            time_refl, 
            phase_refl, 
            start_refl, 
            end_refl, 
            number_of_lines,
            color=self.color_map[r"b_1"],
            amp_scaler=amp_refl_scaler,
            k_scaler=k_refl_scaler,
            freq_scaler=freq_refl_scaler,
            phase_offset=phase_refl_offset,
            x_start=left_end,
            x_end=left_start,
            y_start=down_start,
            y_end=down_start,
        )

        # Transmitted Beam b_2
        amp_trans = amp_input
        k_trans = k_input
        freq_trans = freq_input
        time_trans = 0.0
        phase_trans = phase_input

        start_trans = np.array([right_end.get_value(), up_start.get_value(), 0.0])
        end_trans = np.array([right_start.get_value(), up_start.get_value(), 0.0])

        amp_trans_scaler = end_mirror_trans
        k_trans_scaler = 1
        freq_trans_scaler = 1
        phase_trans_offset = ValueTracker(0.0)
        phase_trans_offset.add_updater(
            lambda mob: mob.set_value( 
                self.get_phase(
                    k_input.get_value(), 
                    right_end.get_value() - left_start.get_value(), 
                    freq_input.get_value(), 
                    time=0, # this is already account for in wave_updater
                    phase=0 # this is already accounted for in phase_refl
                ) 
            )
        )

        self.trans_beam = self.make_electric_field(
            amp_trans, 
            k_trans, 
            freq_trans, 
            time_trans, 
            phase_trans, 
            start_trans, 
            end_trans, 
            number_of_lines,
            color=self.color_map[r"b_2"],
            amp_scaler=amp_trans_scaler,
            k_scaler=k_trans_scaler,
            freq_scaler=freq_trans_scaler,
            phase_offset=phase_trans_offset,
            x_start=right_end,
            x_end=right_start,
            y_start=up_start,
            y_end=up_start,
        )

        # Propagation beams to illustrate the matrix
        # Prop beam east
        amp_prop_beam_east = amp_input
        k_prop_beam_east = k_input
        freq_prop_beam_east = freq_input
        time_prop_beam_east = 0.0
        phase_prop_beam_east = phase_input
        
        start_prop_beam_east = np.array([left_end.get_value(), up_start.get_value(), 0.0])
        end_prop_beam_east = np.array([right_end.get_value(), up_start.get_value(), 0.0])

        amp_prop_beam_east_scaler = ValueTracker(1.0)
        k_prop_beam_east_scaler = 1
        freq_prop_beam_east_scaler = 1
        phase_prop_beam_east_offset = ValueTracker(0.0)
        phase_prop_beam_east_offset.add_updater(
            lambda mob: mob.set_value( 
                self.get_phase(
                    k_input.get_value(), 
                    left_end.get_value() - left_start.get_value(), 
                    freq_input.get_value(), 
                    time=0, # this is already account for in wave_updater
                    phase=0 # this is already accounted for in phase_refl
                ) 
            )
        )

        self.prop_beam_east_beam = self.make_electric_field(
            amp_prop_beam_east, 
            k_prop_beam_east, 
            freq_prop_beam_east, 
            time_prop_beam_east, 
            phase_prop_beam_east, 
            start_prop_beam_east, 
            end_prop_beam_east, 
            number_of_lines,
            color=WHITE,
            amp_scaler=amp_prop_beam_east_scaler,
            k_scaler=k_prop_beam_east_scaler,
            freq_scaler=freq_prop_beam_east_scaler,
            phase_offset=phase_prop_beam_east_offset,
            x_start=left_end,
            x_end=right_end,
            y_start=up_start,
            y_end=up_start,
        )

        # Prop beam west
        amp_prop_beam_west = amp_input
        k_prop_beam_west = k_input
        freq_prop_beam_west = freq_input
        time_prop_beam_west = 0.0
        phase_prop_beam_west = phase_input
        
        start_prop_beam_west = np.array([right_end.get_value(), up_start.get_value(), 0.0])
        end_prop_beam_west = np.array([left_end.get_value(), up_start.get_value(), 0.0])

        amp_prop_beam_west_scaler = ValueTracker(1.0)
        k_prop_beam_west_scaler = 1
        freq_prop_beam_west_scaler = 1
        phase_prop_beam_west_offset = ValueTracker(0.0)
        phase_prop_beam_west_offset.add_updater(
            lambda mob: mob.set_value( 
                self.get_phase(
                    k_input.get_value(), 
                    right_start.get_value() - right_end.get_value(), 
                    freq_input.get_value(), 
                    time=0, # this is already account for in wave_updater
                    phase=0 # this is already accounted for in phase_refl
                ) 
            )
        )

        self.prop_beam_west_beam = self.make_electric_field(
            amp_prop_beam_west, 
            k_prop_beam_west, 
            freq_prop_beam_west, 
            time_prop_beam_west, 
            phase_prop_beam_west, 
            start_prop_beam_west, 
            end_prop_beam_west, 
            number_of_lines,
            color=GREY,
            amp_scaler=amp_prop_beam_west_scaler,
            k_scaler=k_prop_beam_west_scaler,
            freq_scaler=freq_prop_beam_west_scaler,
            phase_offset=phase_prop_beam_west_offset,
            x_start=right_end,
            x_end=left_end,
            y_start=down_start,
            y_end=down_start,
        )

        ## Second beams to add to refl and trans
        # Second Transmitted Beam b_2 (a_2 beam refl off mirror)
        amp_trans_2 = amp_input_2
        k_trans_2 = k_input_2
        freq_trans_2 = freq_input_2
        time_trans_2 = 0.0
        phase_trans_2 = phase_input_2
        x_start_trans_2 = ValueTracker(0.0)
        x_end_trans_2 = ValueTracker(7.0)

        start_trans_2 = np.array([x_start_trans_2.get_value(), up_start.get_value(), 0.0])
        end_trans_2 = np.array([x_end_trans_2.get_value(), up_start.get_value(), 0.0])

        amp_trans_2_scaler = end_mirror_refl
        k_trans_2_scaler = 1
        freq_trans_2_scaler = 1
        phase_trans_2_offset = ValueTracker(PI) # phase flip on refl from back of mirror ( we want +r here)
        phase_trans_2_offset.add_updater(
            lambda mob: mob.set_value( 
                self.get_phase(
                    k_input_2.get_value(), 
                    x_end_trans_2.get_value() - x_start_trans_2.get_value(), 
                    freq_input_2.get_value(), 
                    time=0, # this is already account for in wave_updater
                    phase=PI # this is already accounted for in phase_refl
                ) 
            )
        )

        self.trans_2_beam = self.make_electric_field(
            amp_trans_2, 
            k_trans_2, 
            freq_trans_2, 
            time_trans_2, 
            phase_trans_2, 
            start_trans_2, 
            end_trans_2, 
            number_of_lines,
            color=WHITE,
            # color=self.color_map[r"b_2"],
            amp_scaler=amp_trans_2_scaler,
            k_scaler=k_trans_2_scaler,
            freq_scaler=freq_trans_2_scaler,
            phase_offset=phase_trans_2_offset,
            x_start=x_start_trans_2,
            x_end=x_end_trans_2,
            y_start=up_start,
            y_end=up_start,
        )

        # Second Refl Beam b_1 (a_1 beam refl off mirror)
        amp_refl_2 = amp_input
        k_refl_2 = k_input
        freq_refl_2 = freq_input
        time_refl_2 = 0.0
        phase_refl_2 = phase_input
        x_start_refl_2 = ValueTracker(0.0)
        x_end_refl_2 = ValueTracker(-7.0)

        start_refl_2 = np.array([x_start_refl_2.get_value(), down_start.get_value(), 0.0])
        end_refl_2 = np.array([x_end_refl_2.get_value(), down_start.get_value(), 0.0])

        amp_refl_2_scaler = end_mirror_refl
        k_refl_2_scaler = 1
        freq_refl_2_scaler = 1
        phase_refl_2_offset = ValueTracker(0.0)
        phase_refl_2_offset.add_updater(
            lambda mob: mob.set_value( 
                self.get_phase(
                    k_input.get_value(), 
                    x_start_refl_2.get_value() - x_end_refl_2.get_value(), # phase from input beam 1
                    freq_input.get_value(), 
                    time=0, # this is already account for in wave_updater
                    phase=0 # this is already accounted for in phase_refl
                ) 
            )
        )

        self.refl_2_beam = self.make_electric_field(
            amp_refl_2, 
            k_refl_2, 
            freq_refl_2, 
            time_refl_2, 
            phase_refl_2, 
            start_refl_2, 
            end_refl_2, 
            number_of_lines,
            color=WHITE,
            # color=self.color_map[r"b_1"],
            amp_scaler=amp_refl_2_scaler,
            k_scaler=k_refl_2_scaler,
            freq_scaler=freq_refl_2_scaler,
            phase_offset=phase_refl_2_offset,
            x_start=x_start_refl_2,
            x_end=x_end_refl_2,
            y_start=down_start,
            y_end=down_start,
        )

        # Wave sums
        self.b_1 = self.sum_electric_fields(self.refl_beam, self.refl_2_beam, color=self.color_map[r"b_1"], reverse=False, y_axis=down_start)
        self.b_2 = self.sum_electric_fields(self.trans_beam, self.trans_2_beam, color=self.color_map[r"b_2"], reverse=False, y_axis=up_start)

        ## Arrows
        arrow_length = 4
        arrow_height = 2.5
        self.input_beam_1_arrow = always_redraw(
            lambda: Arrow(
                start=np.array([-arrow_length/2, arrow_height, 0.0]),
                end=np.array([arrow_length/2, arrow_height, 0.0]),
                color=self.color_map[r"a_1"]
            ).next_to(self.input_beam, DOWN)
        )
        self.output_beam_1_arrow = always_redraw(
            lambda: Arrow(
                start=np.array([arrow_length/2, -arrow_height, 0.0]),
                end=np.array([-arrow_length/2, -arrow_height, 0.0]),
                color=self.color_map[r"b_1"]
            ).next_to(self.refl_beam, UP)
        )


        ## Dashed Lines
        dashed_line_height = 5.0
        dashed_line_height_offset = -1.0
        self.left_dashed_line = always_redraw(
            lambda: DashedLine(
                start=np.array([left_end.get_value(), -dashed_line_height/2 + dashed_line_height_offset, 0.0]),
                end=np.array([left_end.get_value(), dashed_line_height/2 + dashed_line_height_offset, 0.0]),
            )
        )
        self.right_dashed_line = always_redraw(
            lambda: DashedLine(
                start=np.array([right_end.get_value(), -dashed_line_height/2 + dashed_line_height_offset, 0.0]),
                end=np.array([right_end.get_value(), dashed_line_height/2 + dashed_line_height_offset, 0.0]),
            )
        )

        # Phase graph
        self.cavity_length = ValueTracker(right_end.get_value() - left_end.get_value())
        self.phase = ValueTracker(k_input.get_value() * self.cavity_length.get_value() % (2 * PI))

        graph_origin = np.array([-4.0, 2.5, 0.0])
        graph_length = 1
        self.x_axis = Line(
            start=(graph_origin[0] - graph_length)*RIGHT + graph_origin[1]*UP,
            end=(graph_origin[0] + graph_length)*RIGHT + graph_origin[1]*UP
        )
        self.y_axis = Line(
            start=graph_origin[0]*RIGHT + (graph_origin[1] - graph_length)*UP,
            end=graph_origin[0]*RIGHT + (graph_origin[1] + graph_length)*UP
        )
        self.phase_arrow_moving = Arrow(
            start=graph_origin,
            end=graph_origin + RIGHT,
            color=GREEN,
            buff=0.0
        )
        self.phase_arrow_ref = self.phase_arrow_moving.copy()
        self.phase_arrow_moving.add_updater(
            lambda mob: mob.become(self.phase_arrow_ref.copy()).rotate(self.phase.get_value(), about_point=graph_origin)
        )

        self.phase_graph_group = VGroup(
            self.x_axis, 
            self.y_axis, 
            self.phase_arrow_moving
        )

        ### Tex
        # General electric field
        self.general_electric_field = MathTex(
            r"{{ E_\mathrm{tot} }} = \Re( {{ a e^{i (\omega t + k x)} }} + {{ b e^{i (\omega t - k x)} }}  )"
        ).shift(3.5 * UP)
        self.general_electric_field[2].set_color(self.color_map[r"a_1"])
        self.general_electric_field[4].set_color(self.color_map[r"b_1"])

        self.amplitude_tex = Tex(r"$a, b$: Amplitude").next_to(self.general_electric_field, DOWN)
        self.frequency_tex = Tex(r"$\omega$: Angular frequency ($= 2 \pi f$ where $f$ is frequency)").next_to(self.amplitude_tex, DOWN)
        self.time_tex = Tex(r"$t$: Time").next_to(self.frequency_tex, DOWN)
        self.wavenumber_tex = Tex(r"$k$: Wavenumber ($={ 2 \pi \over \lambda}$ where $\lambda$ is wavelength)").next_to(self.time_tex, DOWN)
        self.space_tex = Tex(r"$x$: Space").next_to(self.wavenumber_tex, DOWN)

        self.wavenumber_east = MathTex(r"+k", color=self.color_map[r"a_1"]).next_to(self.input_beam_1_arrow, RIGHT)
        self.wavenumber_west = MathTex(r"-k", color=self.color_map[r"b_1"]).next_to(self.output_beam_1_arrow, LEFT)

        # Equations
        self.input_beam_1_tex = always_redraw(
            lambda: MathTex(
                r"a_1", 
                color=self.color_map[r"a_1"]
            ).move_to(self.input_beam).shift(UP)
        )

        self.output_beam_1_tex = MathTex(
            r"b_1", 
            color=self.color_map[r"b_1"]
        ).move_to(self.refl_beam).shift(DOWN)
        self.output_beam_1_tex.add_updater(
            lambda mob: mob.move_to(self.refl_beam).shift(DOWN)
        )

        self.input_beam_2_tex = always_redraw(
            lambda: MathTex(
                r"a_2", 
                color=self.color_map[r"a_2"]
            ).move_to(self.input_2_beam).shift(DOWN)
        )

        self.output_beam_2_tex = MathTex(
            r"b_2", 
            color=self.color_map[r"b_2"]
        ).move_to(self.trans_beam).shift(UP)
        self.output_beam_2_tex.add_updater(
            lambda mob: mob.move_to(self.trans_beam).shift(UP)
        )

        self.input_vector = Matrix(
            [
                [r"a_1"],
                [r"a_2"]
            ]
        ).set_row_colors(self.color_map[r"a_1"], self.color_map[r"a_2"])

        self.output_vector = Matrix(
            [
                [r"b_1"],
                [r"b_2"]
            ]
        ).set_row_colors(self.color_map[r"b_1"], self.color_map[r"b_2"])

        # Propagation matrix
        self.propagation_matrix_tex = MathTex(
            r"\begin{bmatrix} "+\
                r"e^{i k L} & 0 \\"+\
                r"0         & e^{-i k L}"+\
            r"\end{bmatrix}"
        ).shift(3*UP)
        self.propagation_matrix_name = Tex(
            r"Propagation Matrix:"
        ).next_to(self.propagation_matrix_tex, LEFT)

        # Reflection matrix
        self.reflection_matrix_tex = MathTex(
            r"\begin{bmatrix} "+\
                r"-r    & \tau \\"+\
                r" \tau & r"+\
            r"\end{bmatrix}"
        ).shift(3*UP)
        self.reflection_matrix_name = Tex(
            r"Reflection Matrix:"
        ).next_to(self.reflection_matrix_tex, LEFT)

        ## Equations
        self.equals_one = MathTex(r"=").next_to(self.propagation_matrix_tex, LEFT)
        self.input_vector.next_to(self.propagation_matrix_tex, RIGHT)
        self.output_vector.next_to(self.equals_one, LEFT)

        self.propagation_equation = VGroup(
            self.propagation_matrix_tex,
            self.equals_one,
            self.input_vector,
            self.output_vector
        )

        self.prop_equation_1 = MathTex(
            r"{{ b_1 }} = e^{i k L} {{ a_1 }}", 
        ).move_to(self.propagation_matrix_tex).shift(0.2*UP)
        self.prop_equation_1[0].set_color(self.color_map[r"b_1"])
        self.prop_equation_1[2].set_color(self.color_map[r"a_1"])

        self.prop_equation_2 = MathTex(
            r"{{ b_2 }} = e^{-i k L} {{ a_2 }}", 
        ).next_to(self.prop_equation_1, DOWN)
        self.prop_equation_2[0].set_color(self.color_map[r"b_2"])
        self.prop_equation_2[2].set_color(self.color_map[r"a_2"])

        self.refl_equation_1 = MathTex(
            r"{{ b_1 }} = - {{ r }}{{ a_1 }} + {{ \tau }}{{ a_2 }}", 
        ).move_to(self.reflection_matrix_tex).shift(0.2*UP)
        self.refl_equation_1[0].set_color(self.color_map[r"b_1"])
        self.refl_equation_1[2].set_color(self.color_map[r"mirror"])
        self.refl_equation_1[3].set_color(self.color_map[r"a_1"])
        self.refl_equation_1[5].set_color(self.color_map[r"mirror"])
        self.refl_equation_1[6].set_color(self.color_map[r"a_2"])

        self.refl_equation_2 = MathTex(
            r"{{ b_2 }} = {{ \tau }}{{ a_1 }} + {{ r }}{{ a_2 }}", 
        ).next_to(self.refl_equation_1, DOWN)
        self.refl_equation_2[0].set_color(self.color_map[r"b_2"])
        self.refl_equation_2[2].set_color(self.color_map[r"mirror"])
        self.refl_equation_2[3].set_color(self.color_map[r"a_1"])
        self.refl_equation_2[5].set_color(self.color_map[r"mirror"])
        self.refl_equation_2[6].set_color(self.color_map[r"a_2"])

        self.refl_equation_1_no_refl = MathTex(
            r"{{ b_1 }} = {{ \tau }}{{ a_2 }}", 
        ).move_to(self.reflection_matrix_tex).shift(0.2*UP)
        self.refl_equation_1_no_refl[0].set_color(self.color_map[r"b_1"])
        self.refl_equation_1_no_refl[2].set_color(self.color_map[r"mirror"])
        self.refl_equation_1_no_refl[3].set_color(self.color_map[r"a_2"])

        self.refl_equation_2_no_refl = MathTex(
            r"{{ b_2 }} = {{ \tau }}{{ a_1 }}", 
        ).next_to(self.refl_equation_1_no_refl, DOWN)
        self.refl_equation_2_no_refl[0].set_color(self.color_map[r"b_2"])
        self.refl_equation_2_no_refl[2].set_color(self.color_map[r"mirror"])
        self.refl_equation_2_no_refl[3].set_color(self.color_map[r"a_1"])

        self.refl_equation_1_no_trans = MathTex(
            r"{{ b_1 }} = - {{ r }}{{ a_1 }}", 
        ).move_to(self.reflection_matrix_tex).shift(0.2*UP)
        self.refl_equation_1_no_trans[0].set_color(self.color_map[r"b_1"])
        self.refl_equation_1_no_trans[2].set_color(self.color_map[r"mirror"])
        self.refl_equation_1_no_trans[3].set_color(self.color_map[r"a_1"])

        self.refl_equation_2_no_trans = MathTex(
            r"{{ b_2 }} = {{ r }}{{ a_2 }}", 
        ).next_to(self.refl_equation_1_no_trans, DOWN)
        self.refl_equation_2_no_trans[0].set_color(self.color_map[r"b_2"])
        self.refl_equation_2_no_trans[2].set_color(self.color_map[r"mirror"])
        self.refl_equation_2_no_trans[3].set_color(self.color_map[r"a_2"])

        self.refl_equation_1_no_b_1 = MathTex(
            r"{{ b_1 }} = 0", 
        ).move_to(self.reflection_matrix_tex).shift(0.2*UP)
        self.refl_equation_1_no_b_1[0].set_color(self.color_map[r"b_1"])

        self.refl_equation_2_no_b_1 = MathTex(
            r"{{ b_2 }} = {{ \tau }}{{ a_1 }} + {{ r }}{{ a_2 }} = { 2 \over \sqrt{2} }{{ a_1 }}", 
        ).next_to(self.refl_equation_1, DOWN)
        self.refl_equation_2_no_b_1[0].set_color(self.color_map[r"b_2"])
        self.refl_equation_2_no_b_1[2].set_color(self.color_map[r"mirror"])
        self.refl_equation_2_no_b_1[3].set_color(self.color_map[r"a_1"])
        self.refl_equation_2_no_b_1[5].set_color(self.color_map[r"mirror"])
        self.refl_equation_2_no_b_1[6].set_color(self.color_map[r"a_2"])
        self.refl_equation_2_no_b_1[8].set_color(self.color_map[r"a_1"])

        self.refl_equation_1_no_b_2 = MathTex(
            r"{{ b_1 }} = - {{ r }}{{ a_1 }} + {{ \tau }}{{ a_2 }} = - { 2 \over \sqrt{2} }{{ a_1 }}", 
        ).move_to(self.reflection_matrix_tex).shift(0.2*UP)
        self.refl_equation_1_no_b_2[0].set_color(self.color_map[r"b_1"])
        self.refl_equation_1_no_b_2[2].set_color(self.color_map[r"mirror"])
        self.refl_equation_1_no_b_2[3].set_color(self.color_map[r"a_1"])
        self.refl_equation_1_no_b_2[5].set_color(self.color_map[r"mirror"])
        self.refl_equation_1_no_b_2[6].set_color(self.color_map[r"a_2"])
        self.refl_equation_1_no_b_2[8].set_color(self.color_map[r"a_1"])

        self.refl_equation_2_no_b_2 = MathTex(
            r"{{ b_2 }} = 0", 
        ).next_to(self.refl_equation_1_no_b_2, DOWN)
        self.refl_equation_2_no_b_2[0].set_color(self.color_map[r"b_2"])

        self.refl_equation_1_no_a_2 = MathTex(
            r"{{ b_1 }} = - {{ r }}{{ a_1 }}", 
        ).move_to(self.reflection_matrix_tex).shift(0.2*UP)
        self.refl_equation_1_no_a_2[0].set_color(self.color_map[r"b_1"])
        self.refl_equation_1_no_a_2[2].set_color(self.color_map[r"mirror"])
        self.refl_equation_1_no_a_2[3].set_color(self.color_map[r"a_1"])

        self.refl_equation_2_no_a_2 = MathTex(
            r"{{ b_2 }} = {{ \tau }}{{ a_1 }}", 
        ).next_to(self.refl_equation_1_no_a_2, DOWN)
        self.refl_equation_2_no_a_2[0].set_color(self.color_map[r"b_2"])
        self.refl_equation_2_no_a_2[2].set_color(self.color_map[r"mirror"])
        self.refl_equation_2_no_a_2[3].set_color(self.color_map[r"a_1"])

        # Refl tex
        self.end_mirror_refl_value_tex = MathTex(
            r"|r|^2 = ", 
            substrings_to_isolate=(r"r")
        )
        self.end_mirror_refl_value_tex.add_updater(
            lambda mob: mob.next_to(self.end_mirror, RIGHT, buff=0.3)
        )

        self.end_mirror_refl_value = DecimalNumber(num_decimal_places=2, color=BLUE)
        self.end_mirror_refl_value.add_updater(
            lambda mob: mob.set_value(end_mirror_refl.get_value()**2)
        )
        self.end_mirror_refl_value.add_updater(
            lambda mob: mob.next_to(self.end_mirror_refl_value_tex, RIGHT, buff=0.1)
        )


        self.reflectivity_tex = Tex(
            r"Reflectivity",
            color=BLUE
        ).next_to(self.end_mirror, RIGHT, buff=0.3).shift(0.5*UP)

        # Trans tex
        self.end_mirror_trans_value_tex = MathTex(
            r"| {{ \tau }} |^2 = "
        )
        self.end_mirror_trans_value = always_redraw(
            lambda: DecimalNumber(num_decimal_places=2, color=BLUE)
            .set_value(end_mirror_trans.get_value()**2)
            .next_to(self.end_mirror_trans_value_tex, RIGHT, buff=0.1)
        )

        # Lossless eq
        self.lossless_mirror_tex = MathTex(
            r"|r|^2 + | {{ \tau }} |^2 = 1"
        )

        # Interference
        self.interference_tex = Tex(
            "Inteference",
            color=ORANGE
        )
        self.interference_tex.add_updater(
            lambda mob: mob.next_to(self.end_mirror, LEFT, buff=0.3)
        )

        # Length as Decimal Number
        self.length_value_tex = Tex(r"$L = $")
        self.length_value = always_redraw(
            lambda: DecimalNumber(num_decimal_places=2)
            .set_value(self.cavity_length.get_value())
            .next_to(self.length_value_tex, RIGHT, buff=0.1)
        )
        self.length_value_group = always_redraw(
            lambda: VGroup(self.length_value_tex, self.length_value)
            .move_to( np.array([(left_end.get_value() + right_end.get_value())/2.0, -dashed_line_height/2.0 + dashed_line_height_offset, 0.0]) )
        )

        # Time as Decimal Number
        self.timer_value_tex = MathTex(
            r"t = ",
            substrings_to_isolate=r"t"
        ).shift(3.5*UP + 5.5*RIGHT)
        self.timer_value_tex.set_color_by_tex(r"t", YELLOW)
        self.timer_value = always_redraw(
            lambda: DecimalNumber(num_decimal_places=1)
            .set_value(self.timer)
            .next_to(self.timer_value_tex, RIGHT, buff=0.15)
        )
        self.timer_value_group = VGroup(self.timer_value_tex, self.timer_value)

        # Phase as Decimal Number
        self.phase_value_tex = MathTex(
            r"{{ \phi }} = "
        ).next_to(self.x_axis, LEFT).shift(0.75*LEFT)
        self.phase_value_tex[0].set_color(GREEN)
        self.phase_value = DecimalNumber(num_decimal_places=1)
        self.phase_value.add_updater(
            lambda mob: mob.set_value(self.phase.get_value())
        )
        self.phase_value.add_updater(
            lambda mob: mob.next_to(self.phase_value_tex, RIGHT, buff=0.15)
        )
        self.phase_value_group = VGroup(self.phase_value_tex, self.phase_value)


        ## Updaters
        def time_updater(mob, dt):
            self.timer += dt * self.rate
            return

        def wave_updater(mob):
            temp_mob = self.make_electric_field(
                mob.amp, mob.k, mob.freq, self.timer, mob.phase, mob.start, mob.end, mob.number_of_lines, mob.color, 
                mob.amp_scaler, mob.k_scaler, mob.freq_scaler, mob.phase_offset, mob.x_start, mob.x_end, mob.y_start, mob.y_end
            )
            mob.become(temp_mob)
            return 

        def standing_updater(mob):
            fmob = mob.field1
            bmob = mob.field2
            temp_fmob = self.make_electric_field(
                fmob.amp, fmob.k, fmob.freq, self.timer, fmob.phase, fmob.start, fmob.end, fmob.number_of_lines, fmob.color, 
                fmob.amp_scaler, fmob.k_scaler, fmob.freq_scaler, fmob.phase_offset, fmob.x_start, fmob.x_end, fmob.y_start, fmob.y_end
            )
            temp_bmob = self.make_electric_field(
                bmob.amp, bmob.k, bmob.freq, self.timer, bmob.phase, bmob.start, bmob.end, bmob.number_of_lines, bmob.color, 
                bmob.amp_scaler, bmob.k_scaler, bmob.freq_scaler, bmob.phase_offset, bmob.x_start, bmob.x_end, bmob.y_start, bmob.y_end
            )
            temp_mob = self.sum_electric_fields(temp_fmob, temp_bmob, color=mob.color, reverse=mob.reverse, y_axis=mob.y_axis)
            mob.become(temp_mob)
            return 

        self.input_beam.add_updater(wave_updater)
        self.input_2_beam.add_updater(wave_updater)
        self.refl_beam.add_updater(wave_updater)
        self.trans_beam.add_updater(wave_updater)
        self.prop_beam_east_beam.add_updater(wave_updater)
        self.prop_beam_west_beam.add_updater(wave_updater)

        self.refl_2_beam.add_updater(wave_updater)
        self.trans_2_beam.add_updater(wave_updater)

        self.b_1.add_updater(standing_updater)
        self.b_2.add_updater(standing_updater)

        ### Set the scene
        # Add some ValueTrackers
        self.add(left_start)
        self.add(left_end)
        self.add(right_end)
        self.add(right_start)
        self.add(phase_refl_offset)
        self.add(phase_trans_offset)
        self.add(phase_prop_beam_east_offset)
        self.add(phase_prop_beam_west_offset)
        # self.add(self.cavity_length)
        # self.add(self.phase)
        self.add(self.cavity_length)
        self.add(k_input)
        self.add(self.phase)
        self.add(mirror_position)

        self.add(end_mirror_refl)
        self.add(end_mirror_trans)

        self.add(phase_trans_2_offset)
        self.add(phase_refl_2_offset)

        self.cavity_length.add_updater(
            lambda z: z.set_value( right_end.get_value() - left_end.get_value() )
        )
        self.phase.add_updater(
            lambda z: z.set_value( k_input.get_value() * self.cavity_length.get_value() % (2 * PI) )
        )

        ## Two beams introduction
        # Make the input beam and refl beam
        self.play(Create(self.input_beam))
        self.play(Write(self.input_beam_1_tex))
        self.wait(1.0)

        self.play(Create(self.refl_beam))
        self.play(Write(self.output_beam_1_tex))
        self.wait(1.0)

        # General electric field equation
        self.play(Write(self.general_electric_field))
        self.wait(1.0)

        # move electric fields out of the way for a second
        self.play(
            up_start.animate.set_value(-2.0),
            down_start.animate.set_value(-2.8),
            run_time=0.5
        )

        # define the values in general electric field equation
        self.play(Write(self.amplitude_tex))
        self.wait(1.0)
        self.play(Write(self.frequency_tex))
        self.wait(1.0)
        self.play(Write(self.time_tex))
        self.wait(1.0)
        self.play(Write(self.wavenumber_tex))
        self.wait(1.0)
        self.play(Write(self.space_tex))
        self.wait(1.0)

        self.wait(3.0)
        self.play(
            Unwrite(self.amplitude_tex),
            Unwrite(self.frequency_tex),
            Unwrite(self.time_tex),
            Unwrite(self.wavenumber_tex),
            Unwrite(self.space_tex),
        )

        # move electric fields back
        self.play(
            up_start.animate.set_value(0.5),
            down_start.animate.set_value(-2.5),
            run_time=0.5
        )

        # Show difference in direction of propagation
        self.play(Create(self.input_beam_1_arrow))
        self.play(Write(self.wavenumber_east))
        self.play(Create(self.output_beam_1_arrow))
        self.play(Write(self.wavenumber_west))

        # Evolve with time
        self.play(Create(self.timer_value_group))
        self.wait(1)

        self.input_beam.add_updater(time_updater)
        self.wait(5.0)

        # Stop evolution
        self.input_beam.remove_updater(time_updater)
        self.wait(1.0)

        self.play(
            FadeOut(self.general_electric_field),
            FadeOut(self.input_beam_1_arrow),
            FadeOut(self.output_beam_1_arrow),
            FadeOut(self.wavenumber_east),
            FadeOut(self.wavenumber_west)
        )
        self.wait(1.0)

        ## Demonstrate propagation matrix
        # Move left_end, right_start, and right_end off the right side of the screen
        left_end.set_value(7.0)
        right_start.set_value(9.0)
        right_end.set_value(10.0)

        # Add everything we want to add
        self.add(
            self.left_dashed_line,
            self.right_dashed_line,
            self.input_2_beam,
            self.trans_beam,
            self.prop_beam_east_beam,
            self.prop_beam_west_beam,
            self.length_value_group
        )

        self.play(
            left_end.animate.set_value(-2),
            right_end.animate.set_value(2),
            right_start.animate.set_value(7),
        )
        self.wait(2)

        self.play(
            Write(self.input_beam_2_tex),
            Write(self.output_beam_2_tex),
        )

        # Form the propagation matrix multiplication
        self.input_beam_1_tex_copy = self.input_beam_1_tex.copy()
        self.input_beam_2_tex_copy = self.input_beam_2_tex.copy()
        self.output_beam_1_tex_copy = self.output_beam_1_tex.copy()
        self.output_beam_2_tex_copy = self.output_beam_2_tex.copy()

        self.play(
            Write(self.propagation_matrix_tex),
            Write(self.propagation_matrix_name)
        )
        self.wait(1)

        self.play(
            Unwrite(self.propagation_matrix_name)
        )
        self.wait(1)

        self.play(
            Write(self.equals_one),
            TransformMatchingTex(self.input_beam_1_tex_copy, self.input_vector[0][0]),
            TransformMatchingTex(self.input_beam_2_tex_copy, self.input_vector[0][1]),
            TransformMatchingTex(self.output_beam_1_tex_copy, self.output_vector[0][0]),
            TransformMatchingTex(self.output_beam_2_tex_copy, self.output_vector[0][1]),
            Create(self.input_vector),
            Create(self.output_vector),
        )
        self.wait(1)

        self.play(
            FadeOut(self.input_vector),
            FadeOut(self.output_vector),
            FadeOut(self.equals_one),
            TransformMatchingTex(self.input_beam_1_tex_copy, self.prop_equation_1),
            TransformMatchingTex(self.input_beam_2_tex_copy, self.prop_equation_2),
            TransformMatchingTex(self.output_beam_1_tex_copy, self.prop_equation_1),
            TransformMatchingTex(self.output_beam_2_tex_copy, self.prop_equation_2),
            TransformMatchingTex(self.propagation_matrix_tex, self.prop_equation_1),
            TransformMatchingTex(self.propagation_matrix_tex, self.prop_equation_2),
        )
        self.wait(2)

        self.play(
            Write(self.phase_value_group)
        )
        self.wait(1)
        self.play(
            Create(self.phase_graph_group)
        )
        self.wait(2)

        self.play(
            right_end.animate.set_value(5),
            run_time=3
        )
        self.wait(1)

        self.play(
            left_end.animate.set_value(0.5),
            run_time=2
        )
        self.wait(1)

        # self.input_beam.add_updater(time_updater)
        self.wait(1.0)

        self.play(
            right_end.animate.set_value(3),
            run_time=1
        )
        self.wait(1)

        self.play(
            left_end.animate.set_value(-3),
            run_time=1
        )
        self.wait(1)

        # self.input_beam.remove_updater(time_updater)


        # Clear the phase stuff and dashed lines
        self.play(
            Uncreate(self.phase_graph_group),
            Unwrite(self.phase_value_group),
            Unwrite(self.prop_equation_1),
            Unwrite(self.prop_equation_2),
        )
        self.play(
            right_end.animate.set_value(0.0),
            left_end.animate.set_value(0.0),
        )
        self.play(
            FadeOut(self.left_dashed_line),
            FadeOut(self.right_dashed_line),
            Uncreate(self.length_value),
            Unwrite(self.length_value_tex),
            Uncreate(self.prop_beam_east_beam),
            Uncreate(self.prop_beam_west_beam)
        )

        # Spawn a mirror at the intersection, along with beams
        # Adjust phases at the same time
        self.add(self.end_mirror)

        self.remove(self.input_2_beam)
        self.add(self.input_2_beam) # re-add to put back on top of mirror
        self.remove(self.trans_beam)
        self.add(self.trans_beam) # re-add to put back on top of mirror

        self.play(
            end_mirror_refl.animate.set_value(np.sqrt(0.0)),
            end_mirror_trans.animate.set_value(np.sqrt(1.0))
        )
        end_mirror_trans.add_updater(
            lambda mob: mob.set_value( get_mirror_trans(end_mirror_refl.get_value()) )
        )

        self.play(
            mirror_position.animate.set_value(0),
        )
        self.wait(1)
        self.play(
            Write(self.end_mirror_refl_value_tex)
        )
        self.play(
            Write(self.end_mirror_refl_value)
        )
        self.wait(1)

        self.play(
            self.end_mirror_refl_value_tex.animate.set_color_by_tex(r"r", BLUE),
            Write(self.reflectivity_tex)
        )
        self.wait(1)
        self.play(
            Unwrite(self.reflectivity_tex)
        )
        self.wait(1)
        


        # Add some ValueTrackers
        self.add(x_start_trans_2)
        self.add(x_end_trans_2)
        self.add(x_start_refl_2)
        self.add(x_end_refl_2)
        
        # Add some updaters so left_end, right_end, x_start_trans_2, and x_start_refl_2 are always equal to mirror_position
        left_end.add_updater(
            lambda mob: mob.set_value(mirror_position.get_value())
        )
        right_end.add_updater(
            lambda mob: mob.set_value(mirror_position.get_value())
        )
        x_start_trans_2.add_updater(
            lambda mob: mob.set_value(mirror_position.get_value())
        )
        x_start_refl_2.add_updater(
            lambda mob: mob.set_value(mirror_position.get_value())
        )

        # Create reflected beams b_1 and b_2, as well as b_1 and b_2 themselves
        self.output_beam_1_tex.clear_updaters()
        self.output_beam_1_tex.add_updater(
            lambda mob: mob.move_to(self.b_1).shift(DOWN)
        )
        self.output_beam_2_tex.clear_updaters()
        self.output_beam_2_tex.add_updater(
            lambda mob: mob.move_to(self.b_2).shift(UP)
        )
        self.play(
            FadeOut(self.refl_beam),
            FadeOut(self.trans_beam),
            # Create(self.refl_2_beam),
            # Create(self.trans_2_beam),
            Create(self.b_1),
            Create(self.b_2)
        )
        self.wait(1)

        # # Start evolution
        # self.input_beam.add_updater(time_updater)
        # self.wait(5.0)

        # # Stop evolution
        # self.input_beam.remove_updater(time_updater)
        # self.wait(1.0)

        # Write the reflectivity matrix
        self.play(
            Write(self.reflection_matrix_tex),
            Write(self.reflection_matrix_name)
        )
        self.wait(1)

        self.play(
            Unwrite(self.reflection_matrix_name)
        )
        self.wait(1)

        self.input_beam_1_tex_copy = self.input_beam_1_tex.copy()
        self.input_beam_2_tex_copy = self.input_beam_2_tex.copy()
        self.output_beam_1_tex_copy = self.output_beam_1_tex.copy()
        self.output_beam_2_tex_copy = self.output_beam_2_tex.copy()

        self.equals_one = MathTex(r"=").next_to(self.reflection_matrix_tex, LEFT)
        self.input_vector.next_to(self.reflection_matrix_tex, RIGHT)
        self.output_vector.next_to(self.equals_one, LEFT)
        self.play(
            TransformMatchingTex(self.input_beam_1_tex_copy, self.input_vector[0][0]),
            TransformMatchingTex(self.input_beam_2_tex_copy, self.input_vector[0][1]),
            Create(self.input_vector),
        )
        self.wait(2)

        self.play(
            TransformMatchingTex(self.output_beam_1_tex_copy, self.output_vector[0][0]),
            TransformMatchingTex(self.output_beam_2_tex_copy, self.output_vector[0][1]),
            Create(self.output_vector),
        )
        self.wait(2)

        self.play(
            Write(self.equals_one),
        )
        self.wait(2)

        # Write the reflectivity systems of equations
        self.play(
            FadeOut(self.input_vector),
            FadeOut(self.output_vector),
            FadeOut(self.equals_one),
            TransformMatchingTex(self.input_beam_1_tex_copy, self.refl_equation_1),
            TransformMatchingTex(self.input_beam_2_tex_copy, self.refl_equation_2),
            TransformMatchingTex(self.output_beam_1_tex_copy, self.refl_equation_1),
            TransformMatchingTex(self.output_beam_2_tex_copy, self.refl_equation_2),
            TransformMatchingTex(self.reflection_matrix_tex, self.refl_equation_1),
            TransformMatchingTex(self.reflection_matrix_tex, self.refl_equation_2),
        )
        self.wait(4)

        self.play(
            TransformMatchingTex(self.refl_equation_1, self.refl_equation_1_no_refl),
            TransformMatchingTex(self.refl_equation_2, self.refl_equation_2_no_refl),
        )
        self.input_beam.add_updater(time_updater)
        self.wait(5)

        self.input_beam.remove_updater(time_updater)
        self.wait(1)

        # Move to constructive/destructive interference point
        # self.play(
        #     left_end.animate.set_value(0.0),
        #     right_end.animate.set_value(0.0),
        #     mirror_position.animate.set_value(0.0),
        # )
      

        # Change mirror reflectivity to perfectly reflective
        self.play(
            end_mirror_refl.animate.set_value(1.0),
        )
        self.wait(1)
        self.play(
            TransformMatchingTex(self.refl_equation_1_no_refl, self.refl_equation_1_no_trans),
            TransformMatchingTex(self.refl_equation_2_no_refl, self.refl_equation_2_no_trans),
        )
        self.input_beam.add_updater(time_updater)
        self.wait(5)

        self.input_beam.remove_updater(time_updater)
        self.wait(1)

        # Change mirror reflectivity to half
        self.play(
            end_mirror_refl.animate.set_value(np.sqrt(0.5)),
        )
        self.wait(1)
        self.play(
            TransformMatchingTex(self.refl_equation_1_no_trans, self.refl_equation_1_no_b_2),
            TransformMatchingTex(self.refl_equation_2_no_trans, self.refl_equation_2_no_b_2),
        )
        self.input_beam.add_updater(time_updater)
        self.wait(5)

        self.input_beam.remove_updater(time_updater)
        self.wait(1)

        # Switch from b_1 on to b_2 on
        self.play(
            mirror_position.animate.set_value(1.0),
        )
        self.wait(1)
        self.play(
            TransformMatchingTex(self.refl_equation_1_no_b_2, self.refl_equation_1_no_b_1),
            TransformMatchingTex(self.refl_equation_2_no_b_2, self.refl_equation_2_no_b_1),
        )
        self.input_beam.add_updater(time_updater)
        self.wait(5)

        self.input_beam.remove_updater(time_updater)
        self.wait(1)

        # Switch from b_2 on to b_1 on
        self.play(
            mirror_position.animate.set_value(2.0),
        )
        self.wait(1)

        self.play(
            TransformMatchingTex(self.refl_equation_1_no_b_1, self.refl_equation_1_no_b_2),
            TransformMatchingTex(self.refl_equation_2_no_b_1, self.refl_equation_2_no_b_2),
        )
        self.wait(1)
        self.play(
            Write(self.interference_tex)
        )
        self.wait(4)

        self.input_beam.add_updater(time_updater)
        self.wait(5)

        self.input_beam.remove_updater(time_updater)
        self.wait(1)

        self.play(
            Unwrite(self.interference_tex)
        )
        self.wait(1)

        # Turn off a_2 input
        self.play(
            amp_input_2.animate.set_value(0.0),
            run_time=3
        )
        self.play(
            TransformMatchingTex(self.refl_equation_1_no_b_2, self.refl_equation_1_no_a_2),
            TransformMatchingTex(self.refl_equation_2_no_b_2, self.refl_equation_2_no_a_2),
        )
        self.input_beam.add_updater(time_updater)
        self.wait(5)

        self.input_beam.remove_updater(time_updater)
        self.wait(1)